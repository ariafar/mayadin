<?php

if (!extension_loaded('gearman')) {
    die('Error! The PECL::gearman extension is required.');
}

set_time_limit(0);

define('DS', DIRECTORY_SEPARATOR);
define('PS', PATH_SEPARATOR);

define('TEA_ROOT_DIR', dirname(__FILE__) . DS .  '..' . DS . '..');
define('TEA_APP_DIR',  TEA_ROOT_DIR . DS . 'application');
define('TEA_LIB_DIR',  TEA_ROOT_DIR . DS . 'library');
define('TEA_TEMP_DIR', TEA_ROOT_DIR . DS . 'temp');

// Define path to application directory
defined('APPLICATION_PATH')
    || define(
        'APPLICATION_PATH',
        realpath(dirname(__FILE__) . '/../application')
    );

// Define application environment
defined('APPLICATION_ENV')
    || define(
        'APPLICATION_ENV',
        (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'development')
    );

// Ensure library/ is on include_path
set_include_path(
    implode(
        PATH_SEPARATOR,
        array(
            realpath(APPLICATION_PATH . '/../library'),
            get_include_path(),
        )
    )
);

/** Zend_Application */
require_once 'Zend/Application.php';

// Create application, bootstrap, and run
$application = new Zend_Application(
    APPLICATION_ENV,
    APPLICATION_PATH . '/configs/application.ini'
);

$application = $application->bootstrap();

error_reporting(E_ALL);
ini_set('display_errors', true);

$configs = Zend_Registry::get('config');
$gWorker  = new GearmanWorker();
if (isset($configs->gearman) && isset($configs->gearman->servers)) {
    $gWorker->addServers($configs->gearman->servers);
} else {
    $gWorker->addServer();
}

$workers = array (
    'sites::site_snapshot',
    'feeds::publish_feed',
    'feeds::update_stats',
    'feeds::delete_activities',
    'feeds::send_notification',
    'resources::create_thumbnails'
);
$prefix = '';
if (isset($configs->gearman->prefix_workers)) {
    $prefix = $configs->gearman->prefix_workers;
}

foreach ($workers as $worker) {
    $workerName = $prefix . $worker;
    $gWorker->addFunction($workerName, 'Gearman_dowork', $worker);
}

while (1) {
    $ret = $gWorker->work();
    if ($gWorker->returnCode() != GEARMAN_SUCCESS) {
        break;
    }
}

function Gearman_dowork($job, $workerName)
{
    global $application;

    list($moduleName, $workerName) = explode('::', $workerName);

    $modulePath  = strtolower($moduleName);
    $moduleName  = str_replace('_', ' ', strtolower($moduleName));
    $moduleName  = str_replace(' ', '', ucwords($moduleName));

    $workerName  = str_replace('_', ' ', strtolower($workerName));
    $workerName  = str_replace(' ', '', ucwords($workerName));
    $workerFile  = APPLICATION_PATH . '/modules/'.$modulePath.'/workers/' . $workerName . '.php';
    $workerClass = ucfirst(strtolower($moduleName)) . '_Worker_' . $workerName;

    if (!file_exists($workerFile)) {
        throw new Exception('The worker file does not exist: ' . $workerFile);
    }
    include_once $workerFile;

    if (!class_exists($workerClass)) {
        throw new Exception(
            'The worker class: ' . $workerClass.
            ' does not exist in file: ' . $workerFile
        );
    }
    $worker = new $workerClass($application->getBootstrap());
    return $worker->work(unserialize($job->workload()));
}

