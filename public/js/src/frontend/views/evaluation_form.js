
define([
    'jquery',
    'underscore',
    'backbone',
    'base/views/item_view',
    'frontend/models/item',
    'frontend/collections/questions',
    'text!frontend/templates/evaluation_form.html',
    'text!frontend/templates/question_form.html',
    'text!frontend/templates/question.html',
    'i18n!nls/labels',
    'bootstrap_selectpicker',
    ], function ($, _, Backbone, BaseItemView, ItemModel, QuestionsCollection, FormTpl, qFormTpl, QuestionTpl, Labels) {

        var ItemView = BaseItemView.extend({

            events : {
                'change .question_type'     : 'changeQuestionType',
                'click .add_question'       : 'addQuestion',
                'click .delete_question'    : 'deleteQuestion',
                'click .edit_question'      : 'editQuestion',
                'click .save_evaluation'    : 'save',
                'click .cancel'             : 'cancelEditQ',
                'click .update_question'    : 'updateQuestion'
            },

            initialize : function(options) {
                var $this = this;
                this.questionsCollection = new QuestionsCollection([]);
                
                this.evaluationType = options.type;
                

                this.questionsCollection.bind("add", this.renderQuestion, this);
                
                if(this.model.get("id")){
                    this.model.fetch({
                        async : true,
                        success : function(model){
                            $this.render()
                            if($this.model.get("questions") && $this.model.get("questions").length){
                                $this.questionsCollection.add($this.model.get("questions"));
                            }
                        },
                        error : function(){
                        }
                    })
                }else{
                    this.is_New = true;
                    this.model.set("type", options.type);
                    this.render();
                }
              
            },

            render : function(fields) {
                var tpl = _.template(FormTpl);
                this.$el.html(tpl(_.extend(this.model.toJSON(), {
                    labels  : Labels
                })));

                $("#main-container").html(this.$el);
                
                this.renderForm();
                
                return this;
            },

            renderForm : function(opt){
                var qTpl = _.template(qFormTpl);

                var obj = opt || _.extend(this.model.toJSON(), {
                    labels  : Labels,
                    mode    : "new"
                });

                $(".form-wrapper", this.$el).html(qTpl(obj));
                    
                
                $(".selectpicker").selectpicker();
            },

            changeQuestionType : function(e){
                if($(e.target).val() == 1){
                    $(".options input[type=radio]").hide();
                }else{
                    $(".options input[type=radio]").show();
                }
            },
            
            addQuestion : function(){
                var question = $(".question").val();
                var items = [];
                _.each( $(".options input[type=text]"), function(input, index){
                    if($(input).val() != ""){
                        items.push({
                            text    : $(input).val(),
                            value   : index++
                        });
                    }
                });

                if( question == "" || items.length < 2 ){
                    return;
                }
                else{
                    var action = (this.model.get("id")) ? "create" : "add" ;
                    var questionType =  (this.evaluationType != "poll") ? 2 :  $(".question_type").val();
                    this.questionsCollection[action]({
                        evaluationId    : (this.model.get("id")) ? this.model.get("id") : null,
                        question        : question,
                        type            : $(".question_type").val(),
                        items           : items,
                        answer         : (($(".title").val()) != 1) ?  $("[name=answer]:checked").val() : null
                    });
                }

                this.renderForm();
            },

            updateQuestion : function(e){
                var cid = $(e.target).attr("id");
                var questionModel = this.questionsCollection.get(cid);
                
                var question = $(".question").val();
                var items = [];
                _.each( $(".options input[type=text]"), function(input, index){
                    if($(input).val() != ""){
                        items.push({
                            text    : $(input).val(),
                            value   : index++
                        });
                    }
                });

                if( question == "" || items.length < 2 ){
                    return;
                }
                else{
                    var action = (questionModel.get("id")) ? "save" : "set" ;
                    var questionType =  (this.evaluationType != "poll") ? 2 :  $(".question_type").val();
                    questionModel[action]({
                        question        : question,
                        type            : $(".question_type").val(),
                        items           : items,
                        answer         : (($(".title").val()) != 1) ?  $("[name=answer]:checked").val() : null
                    });
                }

                this.renderForm();
            },

            clearQuestionForm : function(){
            //                $(".options input[type=text]").val("");
            //                $($("[name=answer]")[0]).attr("checked", true)
            //                $(".question").val("");

            },

            renderQuestion : function(q_model){
                var q_tpl = _.template(QuestionTpl);
                $(".questions-list", this.$el).append(q_tpl(_.extend(q_model.toJSON(), {
                    labels  : Labels,
                    index   : this.questionsCollection.indexOf(q_model)
                })));
            },

            deleteQuestion : function(e){
                var $this = this,
                el = $(e.target).parents(".item-wrapper"),
                index = el.attr("id") - 1,
                question = this.questionsCollection.at(index);
                
                question.save({
                    deleted : 1
                },{
                    success : function(){
                        el.remove();
                        $this.questionsCollection.remove(question);
                    }
                });
                
               
            },

            editQuestion : function(e){
                var index = $(e.target).parents(".row").attr("id");
                var question = this.questionsCollection.at(index - 1);
                this.renderForm(_.extend(question.toJSON(),{
                    labels  : Labels,
                    mode    : "edit",
                    index   : index,
                    cid     : question.cid
                }));
            },

            cancelEditQ : function(){
                this.renderForm();
            },

            save : function(){
                var title = $(".title").val(),
                type = this.model.get("type");
                
                if(title == "" || !this.questionsCollection.length)
                    return;
                
                switch(this.evaluationType){
                    case "poll" :
                        type = 1;
                        break;
                    case "contest" :
                        type = 2;
                        break;
                    case "exam" :
                        type = 3;
                        break;
                }
            
                this.model.save({
                    title       : title,
                    type        : type,
                    questions   : this.questionsCollection.toJSON()
                },{
                    success : function(){
                        $(".loading-image").hide();
                        $(".success-msg").show();
                        setTimeout(function(){
                            $(".success-msg").hide();
                            window.history.back();
                        }, 2000);

                    //                        Backbone.history.navigate()
                    }
                })
            }

        });

        return ItemView;
    });
