define([
    'application',
    'jquery',
    'underscore',
    'backbone',
    'frontend/views/items_list',
    'text!frontend/templates/news_list.html',
    'frontend/views/item',
    'i18n!nls/fa-ir/labels',
    ], function (Application, $, _, Backbone, BaseListView,  ListTpl, ItemView, Labels) {
       
        var NewsListView = BaseListView.extend({

            hasMoreItems : true,

            events : {
                'click .next-page'              : 'nextPage',
                'click .second-navbar span'     : 'getNewsByType'
            },

            render : function(fields){
                var $this = this;

                this.template = _.template(ListTpl);
                this.$el.html(this.template( {
                    labels  : Labels,
                    target    : this.options.target,
                    type    : this.options.type || 'news'
                }));

                return this;
            },
            
            afterRender : function(){
                this.initTable();
                this.createFilter();
            },
            
            afterRenderItems : function(){
                var $this = this;

                if(this.collection.length == 0)
                    return;
            },

            getNewsByType : function(e){
                var el = $(e.target),
                type = el.data("type");

                if(!el.hasClass("selected")){
                    $(".second-navbar .selected", this.$el).removeClass("selected");
                    el.addClass("selected");
                    this.collection.filters.set("type", type);
                }
                
            }


        });


        return NewsListView
    });
