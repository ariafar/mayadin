define([
    'application',
    'jquery',
    'underscore',
    'backbone',
    'base/views/list_view',
    'text!frontend/templates/questions_answers_list.html',
    'frontend/views/question_answer_item',
    'i18n!nls/fa-ir/labels',
    ], function (Application, $, _, Backbone, BaseListView,  ListTpl, ItemView, Labels) {
       
        var NewsListView = BaseListView.extend({

            hasMoreItems : true,

            events : {
                'click .next-page'              : 'nextPage',
                'click .second-navbar span'     : 'getNewsByType'
            },

            render : function(fields){
                var $this = this;

                this.template = _.template(ListTpl);
                this.$el.html(this.template( {
                    labels  : Labels,
                    type    : this.collection.filters.get("type"),
                    target  : this.options.target
                }));

                return this;
            },
            
            afterRender : function(){
                this.initTable();
                this.createFilter();
            },
            
            afterRenderItems : function(){
                var $this = this;

                if(this.collection.length == 0)
                    return;
            },

            renderItem: function(model, index) {
                
                model.set({
                    target : this.options.target
                });

                var no_class = ["humans", "galleries", "stations"];

                var item_view = new ItemView({
                    model       : model,
                    index       : index,
                    fields      : this.selectedFields,
                    className   : ( no_class.indexOf(model.get('target')) == -1 ) ? "item-wrapper row" : ""
                });
                this.items.push(item_view);
                $(".items-wrapper", this.$el).append(item_view.render().$el);
                item_view.afterRender();
            },

            nextPage : function(){

                if (!this.hasMoreItems)
                    return;
                
                this.paging = true;

                var $this = this;
                this.collection.nextPage().fetch({
                    add: true,
                    reset   : false,
                    update: true,
                    //                    silent : true,
                    //                    remove: true,
                    success: function(collection, response) {
                        if (response.length <= 0) {
                            $this.hasMoreItems = false;
                        }
                    }
                });
                return this;
            },

            getNewsByType : function(e){
                var el = $(e.target),
                answer = el.data("answer");

                if(!el.hasClass("selected")){
                    $(".second-navbar .selected", this.$el).removeClass("selected");
                    el.addClass("selected");
                    this.collection.filters.set("has_answer", answer);
                }
                
            }


        });


        return NewsListView
    });
