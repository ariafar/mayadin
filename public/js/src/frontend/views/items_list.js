define([
    'application',
    'jquery',
    'underscore',
    'backbone',
    'base/views/list_view',
    'text!frontend/templates/items_list.html',
    'frontend/views/item',
    'i18n!nls/fa-ir/labels',
    ], function (Application, $, _, Backbone, BaseView,  ListTpl, ItemView, Labels) {
       
        var ItemsListView = BaseView.extend({

            className : "row",
            events : {
                'click .next-page'      : 'nextPage'
            },
            initialize : function(options){
                
                this.template = _.template(ListTpl);
                this.options = options;
                var $this = this;
                
                if(this.options.filter)
                    this.collection.filters.set(this.options.filter, {
                        silent : true
                    });


                this.collection.fetch({
                    add     : true,
                    reset : true,
                    update  : true
                });

                this.collection.on('add' , this.renderItem, this);
                this.collection.on('reset' , this.reset, this);
                Events.on("scroll:end", this.nextPage, this);

                this.collection.on('fetchSuccess', function(resp, collection) {
                    if(this.filters.get("start") + this.filters.get("limit") > resp.count ){
                        $(".no-more-items", this.$el).show();
                        $this.hasMoreItems = false;
                        $('.load-more').hide();
                        $(".no-items", this.$el).hide();
                    }
                    else{
                        $this.hasMoreItems = true;
                        $('.load-more').show();
                        $(".no-items", this.$el).hide();
                        $('.no-more-items').hide();
                    }

                    if(resp.count){
                        $('.count').html(_.translate(resp.count.toString(), true));
                    }
                   
                    if(this.filters.get('start') != 0)
                        $('.previous-page').removeClass('disabled');
                    
                    if((this.filters.get('limit') + this.filters.get('start')) < resp.count ){
                        $('.load-more').show();
                    }
                });

                this.items = [];
            },


            reset : function(){
                _.each(this.items, function(itemView){
                    itemView.dispose();
                })
            },
            
            render : function(fields){
                var $this = this;
                this.$el.html(this.template( {
                    labels  : Labels,
                    target    : this.options.target,
                    type    : this.options.type || false
                }));

                return this;
            },
            
            afterRender : function(){
                this.initTable();
                this.createFilter();
            },
            
            afterRenderItems : function(){
                var $this = this;

                if(this.collection.length == 0)
                    return;
            },

            renderItem: function(model, index) {
                var item_view = new ItemView({
                    model       : model,
                    index       : index,
                    fields      : this.selectedFields,
                    className   : "item-wrapper row"
                });
                this.items.push(item_view);
                $(".items-wrapper", this.$el).append(item_view.render().$el);
                item_view.$el.attr("id" , model.get("id"));
                item_view.afterRender();
            },

            nextPage : function(){
                if (!this.hasMoreItems)
                    return;
                
                this.paging = true;

                this.collection.nextPage().fetch({
                    add     : true,
                    reset : true,
                    update  : true
                });
            }


        });


        return ItemsListView
    });
