define([
    'application',
    'jquery',
    'underscore',
    'backbone',
    'frontend/views/items_list',
    'text!frontend/templates/activity_item.html',
    'i18n!nls/fa-ir/labels',
    ], function (Application, $, _, Backbone, ListView, ItemTpl, Labels) {
       
        var ActivitiesListView = ListView.extend({

            hasMoreItems : true,
            tagName : "ul",
            
            render : function(){
                return this;
            },
            
            renderItem: function(model, index) {
                var tpl = _.template(ItemTpl);
                this.$el.append(tpl(_.extend(model.toJSON(),{
                    labels : Labels
                })));
            },

        });


        return ActivitiesListView
    });
