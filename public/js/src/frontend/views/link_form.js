define([
    'jquery',
    'backbone',
    'underscore',
    'app',
    'frontend/views/item_form',
    'frontend/models/item',
    'text!frontend/templates/link_form.html',
    'i18n!nls/fa-ir/labels',
    ], function($, Backbone, _, App, FormView, ItemModel, FormTpl, Labels) {
       
        var LinkFormView = FormView.extend({

            initialize: function(options) {

                this.events =_.extend({
                   
                    }, this.events);

                LinkFormView.__super__.initialize.call(this, options);

            },

            setSectionsPhotos : function(links){
                var $this = this;
                 
                _.each(links, function(link){
                    for(var index = 0; index < $this.parts.length -1 ; index++){
                        var section = $this.parts[index];
                        if( link.location.indexOf(section) != -1 ){
                            if(!$this.images){
                                $this.images = [];
                            }
                            $this.images.push(link);
                            index = $this.parts.length -1;
                        }
                    }
                })
            },

            render : function(){
                var tpl = _.template(FormTpl);
                this.$el.html(tpl(_.extend(this.model.toJSON(), {
                    labels          : Labels,
                    target          : this.model.get("target")
                })));
                return this;
            },

            afterRender : function(){
                this.initUploader();
            },

            changePart : function(){
                var content =  $("#content", this.$el).val();
                this.model.set(this.part, content);
                $("#content", this.$el).val("");
                $(".images-list", this.$el).empty();
                this.part = $(".boursane-parts", this.$el).val();
                if(!this.images){
                    this.images = [];
                }

                this.loadData();
                
            },

            loadData : function(){
                var $this = this;
                if(this.model.get(this.part) && this.model.get(this.part) != ""){
                    $("#content", this.$el).val(this.model.get(this.part));
                }

                var photos = this.images;
                if(photos && photos.length > 0){
                    _.each(photos, function(photo){
                        $this.addPhoto(photo);
                    })
                }
                 
            },
            
            initUploader : function()
            {
                if(this.model.get("target") == "links"){
                    var filters = [{
                        title      : "PDF files",
                        extensions : "pdf"
                    }];
                }else{
                    var filters = [{
                        title      : "Image files",
                        extensions : "jpg,gif,png"
                    }];
                }

                var $this = this;
                if(!$("#image_upload_btn").length)
                    return;
                
                var uploaderBTn = new plupload.Uploader({
                    runtimes            : 'html5,html4',
                    browse_button       : 'image_upload_btn',
                    container           : 'btn_container',
                    multi_selection     : true,
                    max_file_size       : '10mb',
                    url                 :'/api/resources/',
                    resize              : {
                        width           : 320,
                        height          : 240,
                        quality         : 90
                    },
                    filters             : filters,
                    
                    init                : {
                        FilesAdded: function(up, files) {
                            $("span.plupload_upload_status", $this.$el).html("");
                            $("div.progress-bar", $this.$el).width(0);
                            $(".upload-image-progress", $this.el).show();
                            up.start();
                        },
                        beforeUpload : function(uploader ,file){
                            var target = ( $this.model.get("target") == "photoCoverage")? "news" : $this.model.get("target");
                            uploader.settings.url = '/api/resources?parentType='+ target + (($this.model.get("id")) ? '&parentId=' + $this.model.get("id") : "" );
                        },
                        UploadProgress : function(up, file){
                            $("div.progress-bar", this.$el).html(up.total.percent + "%");
                            $("div.progress-bar", $this.$el).css("width", up.total.percent + "%");
                            $("span.plupload_upload_status", $this.$el).html("Uploaded "+(up.total.uploaded + 1) + "/" + up.files.length+" files");
                        },
                        FileUploaded : function(up, file, resp){
                            if(up.total.uploaded == up.files.length){
                                $('.upload-image-progress', this.$el).fadeOut(5000);
                            }
                            var file = _.extend($.parseJSON(resp.response));

                            if($this.model.get("target") == "links"){
                                $("[name=path]").val(file.name);
                                $this.uploadedFile = file;
                            }else{
                                $this.addPhoto(file);
                                $this.images.push(file);
                            }
                        }
                    }
                });
                uploaderBTn.init();

            },
            addPhoto : function(photo){
                var tpl = _.template(ImgTpl);
                $(".images-list", this.$el).append(tpl(photo));
            },

            deletePhoto : function(e){
                var $this = this;
                var el = $(e.target).parent(),
                photoId = el.attr("id");

                var self = this;
                $.ajax({
                    url     : '/api/resources/'+ photoId,
                    type    :  'PUT',
                    data    : {
                        deleted : 1
                    },
                    success : function(){
                        var photos = $.grep( $this.images, function(opt){
                            return !(opt.id == photoId);
                        });

                        $this.images = photos;
                        el.remove();
                    }
                }, this);
               
            },

            validate : function(){
                var validate =  true;
                
                $("[required]").each(function(index, el){
                    if($(el).val() == ""){
                        validate =  false;
                        $(el).parent().addClass("error");
                    }
                     
                });
                return validate;
            },

            save : function(){
                
                if(!this.validate())
                    return;

                $(".error").removeClass("error");
                
                var $this = this;
                if(this.model.get("target") == "links"){
                    var data = $("form", this.$el).serializeJSON();
                    if(this.uploadedFile && data.path == this.uploadedFile.name){
                        data.fileId = this.uploadedFile.id;
                    }
                }else{
                    var data = _.extend( $("form", this.$el).serializeJSON(), {
                        images : $this.images
                    });
                }


                this.model.save(data, {
                    success : function(){
                        $(".loading-image").hide();
                        $(".success-msg").show();
                        setTimeout(function(){
                            $(".success-msg").hide();
                            window.history.back();
                        }, 2000);
                    }
                })
              
            }

        })

        return LinkFormView;
    
   
    });




