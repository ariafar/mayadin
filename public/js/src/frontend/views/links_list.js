define([
    'application',
    'jquery',
    'underscore',
    'backbone',
    'frontend/views/items_list',
    'frontend/views/link_form',
    'text!frontend/templates/links_list.html',
    'text!frontend/templates/link_form.html',
    'text!frontend/templates/education_form.html',
    'frontend/views/item',
    'frontend/models/item',
    'i18n!nls/fa-ir/labels',
    ], function (Application, $, _, Backbone, ListView, LinkFormView, ListTpl, LinkFormTpl, EducationFormTpl, ItemView, ItemModel, Labels) {
       
        var LinksListView = ListView.extend({

            hasMoreItems : true,

            events : {
                'click .second-navbar span'     : 'getNewsByType',
                'click .add-item'               : 'popNewItemCreation',
                'click .create-item'            : 'createNewItem',
                'click .items-wrapper:not(.icons)' : 'showFolderItems',
                'click .back'                   : 'back',
                'click .edit-link'              : 'editItem',
                'click .update-item'            : 'updateItem'
            },

            render : function(){
                this.template = _.template(ListTpl);
                this.$el.html(this.template( {
                    labels  : Labels,
                    type    : this.options.type,
                    target  : this.options.target
                }));

                return this;
            },

            getNewsByType : function(e){
                var el = $(e.target),
                type = el.data("category"),
                target = el.data("type");

                this.collection.target = target;
                
                if(!el.hasClass("selected")){
                    $(".second-navbar .selected", this.$el).removeClass("selected");
                    el.addClass("selected");
                    this.collection.filters.set({
                        "category"  : type,
                        "parentId"  : 0
                    });

                    $(".back", this.$el).hide();
                }
            },

            popNewItemCreation :  function(){
                this.new_item = new ItemModel({
                    target : this.collection.target
                });
                
                var template = (this.collection.target == "links") ? LinkFormTpl : EducationFormTpl,
                tpl = _.template(template);
                
                $("#new-item-modal", this.$el).html(tpl(_.extend(this.new_item.toJSON(), {
                    labels          : Labels,
                    category        : this.collection.filters.get("category"),
                    showfolderTab   : (this.collection.filters.get("type") == 2 || this.collection.filters.get("parentId")) ? false : true
                })));
                
                $("#new-item-modal", this.$el).modal();
                this.initUploader(this.new_item);
            },

            initUploader : function(model)
            {
                this.images = [];

                if(model.get("target") == "links"){
                    var filters = [{
                        title      : "PDF files",
                        extensions : "pdf"
                    }];
                }else{
                    var filters = [{
                        title      : "Image files",
                        extensions : "jpg,gif,png"
                    }];
                }

                var $this = this;
                if(!$("#file_upload_btn").length)
                    return;

                var uploaderBTn = new plupload.Uploader({
                    runtimes            : 'html5,html4',
                    browse_button       : 'file_upload_btn',
                    container           : 'upload_btn_container',
                    multi_selection     : true,
                    max_file_size       : '10mb',
                    url                 :'/api/resources/',
                    resize              : {
                        width           : 320,
                        height          : 240,
                        quality         : 90
                    },
                    filters             : filters,

                    init                : {
                        FilesAdded: function(up, files) {
                            $("span.plupload_upload_status", $this.$el).html("");
                            $("div.progress-bar", $this.$el).width(0);
                            $(".upload-image-progress", $this.el).show();
                            up.start();
                        },
                        beforeUpload : function(uploader ,file){
                            uploader.settings.url = '/api/resources?parentType='+ model.get("target") + ((model.get("id")) ? '&parentId=' + model.get("id") : "" );
                        },
                        UploadProgress : function(up, file){
                            $("div.progress-bar", this.$el).html(up.total.percent + "%");
                            $("div.progress-bar", $this.$el).css("width", up.total.percent + "%");
                            $("span.plupload_upload_status", $this.$el).html("Uploaded "+(up.total.uploaded + 1) + "/" + up.files.length+" files");
                        },
                        FileUploaded : function(up, file, resp){
                            if(up.total.uploaded == up.files.length){
                                $('.upload-image-progress', this.$el).fadeOut(5000);
                            }
                            var file = _.extend($.parseJSON(resp.response));

                            if(model.get("target") == "links"){
                                model.set("fileId", file.id);
                                $("[name=path]").val(file.name);
                                $this.uploadedFile = file;
                            }else{
                                $(".image-preview").attr("src", "/r/" + file.id);
                                $this.images.push(file);
                            }
                        }
                    }
                });

                uploaderBTn.init();

            },

            createNewItem : function(){
                 
                var data =  $(".active form", this.$el).serializeJSON(),
                $this = this;
                
                if(data.isFile == 1 && !this.new_item.get("fileId"))
                    return;

                if(data.title == "" && data.description == "")
                    return;
                
                data.images = this.images;
                
                this.new_item.save(_.extend({
                    type        : this.collection.filters.get("type"),
                    category    : this.collection.filters.get("category"),
                    parentId    : this.collection.filters.get("parentId")
                },data), {
                    success : function(model){
                        $this.collection.add(model);
                        $("#new-item-modal", this.$el).modal('hide');
                    }
                });
            },

            updateItem : function(e){
                var item_id = $(e.target).attr("id"),
                model = this.collection.get(item_id),
                data =  $("form", this.$el).serializeJSON();

                if(data.title == "" && data.description == "")
                    return;

                model.save(data, {
                    success : function(model){
                        $("#new-item-modal", this.$el).modal('hide');
                    }
                });
            },
            
            showFolderItems : function(e){
                var item_id = $(e.target).parents(".item-wrapper").attr("id"),
                item = this.collection.get(item_id);
                if(item.get("isFile") != "0")
                    return;
                
                this.collection.filters.set("parentId", item_id);
                $(".back", this.$el).show();
            },

            back : function(e){
                this.collection.filters.set("parentId", 0);
                $(".back", this.$el).hide();
            },

            editItem : function(e){
                var item_id = $(e.target).parents(".item-wrapper").attr("id"),
                model = this.collection.get(item_id);
                
                var template = (this.collection.target == "links") ? LinkFormTpl : EducationFormTpl,
                tpl = _.template(template);
                $("#new-item-modal", this.$el).html(tpl(_.extend(model.toJSON(), {
                    labels : Labels
                })));
                $("#new-item-modal", this.$el).modal();
                
                model.get("isFile") == 1  && this.initUploader(model);
            }

        });


        return LinksListView
    });
