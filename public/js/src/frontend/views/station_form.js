define([
    'jquery',
    'backbone',
    'underscore',
    'app',
    'base/views/base_view',
    'frontend/models/item',
    'text!frontend/templates/station_form.html',
    'i18n!nls/labels',
    "jquery-helper",
    'bootstrap_selectpicker',
    //    'http://map.tehran.ir/app/pub/index.php/application/api/key/2523b935ca80dde2613bcc9f8968cbd3?v=4.0&t=Vector'

    ], function($, Backbone, _, App, BaseView, ItemModel,FormTpl, Labels) {
        var BoorsaneFormView = BaseView.extend({

            className : 'station-form-wrapper',

            events : {
                'click .save'               : 'save'
            },
                
            initialize: function(opt) {
                var $this = this;
                
                if(this.model.get("id")){
                    this.model.fetch({
                        async : true,
                        success : function(model){
                            $this.render()
                        },
                        error : function(){
                        }
                    })
                }else{
                    this.is_New = true;
                    this.render();
                }
            },

            render : function(){
                var tpl = _.template(FormTpl);
                this.$el.html(tpl(_.extend(this.model.toJSON(), {
                    labels          : Labels,
                    target          : this.model.get("target")
                })));
                $("#main-container").html(this.$el);

                this.afterRender();
            },

            afterRender : function(){
                $("select", this.$el).selectpicker({
                    width : 300
                });
                if(this.model.get("id")){
                    $("select", this.$el).selectpicker("val", this.model.get("area"));
                }

            //                var id  = "map-tehran-" + (new Date()).getTime();
            //                $('.map-container', this.$el).attr('id', id);
            //                this.map = new MPS.Map(id);
            //                this.map.setCenter(new MPS.LonLat(this.model.get("longitude"), this.model.get("latitude")), 30);

            },

            changePart : function(){
                var content =  $("#content", this.$el).val();
                this.model.set(this.part, content);
                $("#content", this.$el).val("");
                $(".images-list", this.$el).empty();
                this.part = $(".boursane-parts", this.$el).val();
                if(!this.images){
                    this.images = [];
                }

                this.loadData();
                
            },
            
            save : function(){
                var $this = this;
                var data = $("form", this.$el).serializeJSON();

                this.model.save(_.extend(data, {
                    images : $this.images
                }), {
                    success : function(){
                        $(".loading-image").hide();
                        $(".success-msg").show();
                        setTimeout(function(){
                            $(".success-msg").hide();
                            window.history.back();
                        }, 2000);
                    //                        if($this.is_New) {
                    //                            var target = $this.model.get("target");
                    //                            var type = $this.model.get("type");
                    //                            $this.model = new ItemModel({
                    //                                target : target,
                    //                                type    : type
                    //                            });
                    //                            window.history.back();
                    //                        //                            $this.render();
                    //                        //                            $this.delegateEvents();
                    //                        }
                      
                    }
                })
              
            }

        })

        return BoorsaneFormView;
    
   
    });




