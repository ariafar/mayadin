define([
    'application',
    'jquery',
    'underscore',
    'backbone',
    'frontend/views/items_list',
    'text!frontend/templates/markets_list.html',
    'frontend/views/market_form',
    'frontend/views/item',
    'frontend/models/market',
    'i18n!nls/fa-ir/labels',
    ], function (Application, $, _, Backbone, ItemsListView,  ListTpl, FormView, ItemView, ItemModel, Labels) {
       
        var NewsListView = ItemsListView.extend({


            events : {
                'click .second-navbar span'     : 'getNewsByType',
                'click .add-item'               : 'popNewItemCreation',
                'click .list-group-item-heading': 'showFolderItems',
                'click .back'                   : 'back',
                'click .edit-market'            : 'popEditItem'
            },

            render : function(fields){
                var $this = this;

                this.template = _.template(ListTpl);
                this.$el.html(this.template( {
                    labels  : Labels,
                    target    : this.options.target
                }));

                return this;
            },

            popNewItemCreation : function(model){
                var form_view = new FormView({
                    collection  : this.collection,
                    model       : (model && model instanceof Backbone.Model) ? model :  new ItemModel(),
                    className   : "modal-dialog market-form"
                });

                $("#new-item-modal", this.$el).html(form_view.$el);
                $("#new-item-modal", this.$el).modal();
                form_view.afterRender();
            },

            popEditItem : function(e){
                var item_id = $(e.target).parents(".item-wrapper").attr("id"),
                model = this.collection.get(item_id);
                this.popNewItemCreation(model);
            },
            
            afterRenderItems : function(){
                var $this = this;

                if(this.collection.length == 0)
                    return;
            },

            getNewsByType : function(e){
                var el = $(e.target),
                type = el.data("type");

                if(!el.hasClass("selected")){
                    $(".second-navbar .selected", this.$el).removeClass("selected");
                    el.addClass("selected");
                    this.collection.filters.set("type", type);
                }
            },

            showFolderItems : function(e){
                if($(e.target).data("parent") != -1 )
                    return;
                
                var item_id = $(e.target).parents(".item-wrapper").attr("id"),
                parent = this.collection.get(item_id);
                
                $(".title", this.$el).html(Labels.square_bazaar_list + parent.get("mrktName"))
                this.collection.filters.set("mrktParent", item_id);
                $(".back", this.$el).show();
            },

            back : function(e){
                this.collection.filters.set("mrktParent", -1);
                $(".back", this.$el).hide();
                $(".title", this.$el).html(Labels.squares_list);
            }


        });


        return NewsListView
    });
