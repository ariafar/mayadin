define([
    'application',
    'jquery',
    'underscore',
    'backbone',
    'base/views/base_view',
    'text!frontend/templates/images_list.html',
    'frontend/models/item',
    'frontend/views/item',
    'i18n!nls/fa-ir/labels',
    'libs/jqueryPlugins/plupload.full',
    'libs/jqueryPlugins/jquery.plupload.queue',
    'libs/jqueryPlugins/jquery.lightbox-0.5',
    ], function (Application, $, _, Backbone, BaseView, ListTpl,ItemModel, ItemView, Labels) {
       
        var ItemsListView = BaseView.extend({

            hasMoreItems : true,

            className : "row",
            events : {
                'click .save'           : "save",
                'click .next-page'      : 'nextPage'
            },

            reset : function(){},
            
            render : function(fields){
                var $this = this;
                this.$el.html(this.template( {
                    labels  : Labels,
                    target    : this.options.target,
                    type    : this.options.type || false
                }));
                
                return this;
            },
            registerEvents : function()
            {
                var $this = this;
                var uploaderBTn = new plupload.Uploader({
                    runtimes            : 'html5,html4',
                    browse_button       : 'image_upload_btn',
                    container           : 'btn_container',
                    multi_selection     : true,
                    max_file_size       : '10mb',
                    url                 :'/api/resources/?parentType=gallery',
                    filters             : [{
                        title      : "Image files",
                        extensions : "jpg,gif,png"
                    }],
                    resize              : {
                        width           : 320,
                        height          : 240,
                        quality         : 90
                    },
                    init                : {
                        FilesAdded: function(up, files) {
                            $this.newPhotoModel = new ItemModel();
                            $("span.plupload_upload_status", $this.$el).html("");
                            $("div.progress-bar", $this.$el).width(0);
                            $(".upload-image-progress", $this.el).show();
                            up.start();
                        },
                        UploadProgress : function(up, file){
                            $("div.progress-bar", this.$el).html(up.total.percent + "%");
                            $("div.progress-bar", $this.$el).css("width", up.total.percent + "%");
                            $("span.plupload_upload_status", $this.$el).html("Uploaded "+(up.total.uploaded + 1) + "/" + up.files.length+" files");
                        },
                        FileUploaded : function(up, file, resp){
                            var photo = _.extend($.parseJSON(resp.response));
                            $this.newPhotoModel.set(photo);
                            $(".new_img", $this.$el).attr("src", "/r/"+ photo.id);
                        }
                    }
                });
                uploaderBTn.init();

            },
            
            addPhoto : function(photo){
                var tpl = _.template(ImgTpl);
                $(".images-list", this.$el).append(tpl(photo));
            },

            afterRender : function(){
                this.initTable();
                this.createFilter();
            },
            
            afterRenderItems : function(){
                var $this = this;

                if(this.collection.length == 0)
                    return;
            },

            renderItem: function(model, index) {
                model.set({
                    target : this.options.target
                })
                var item_view = new ItemView({
                    model       : model,
                    index       : index,
                    fields      : this.selectedFields,
                    className   : "image-wrapper"
                });
                this.items.push(item_view);
                $(".items-wrapper", this.$el).append(item_view.render().$el)
            },

            nextPage : function(){

                if (!this.hasMoreItems)
                    return;
                
                this.paging = true;

                var $this = this;
                this.collection.nextPage().fetch({
                    add: true,
                    reset   : false,
                    update: true,
                    //                    silent : true,
                    //                    remove: true,
                    success: function(collection, response) {
                        if (response.length <= 0) {
                            $this.hasMoreItems = false;
                        }
                    }
                });
                return this;
            },

            save : function(){
                var $this = this;
                
                this.newPhotoModel.save({
                    target  : "resources",
                    parentType : "gallery",
                    parentId : $this.options.gallery_id,
                    title   : $(".title", this.$el).val(),
                    description : $(".description", this.$el).val()
                },{
                    success : function(model){
                        $this.collection.add(model);
                        $this.newPhotoModel = new ItemModel();

                        $(".new_img", $this.$el).attr("src", "");
                        $(".title", $this.$el).val(""),
                        $(".description", $this.$el).val("")
                    }
                })
            }


        });


        return ItemsListView
    });
