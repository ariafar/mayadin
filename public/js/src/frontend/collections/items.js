define([
    'jquery',
    'underscore',
    'backbone',
    'base/collection/Collection',
    'frontend/models/item'
    ], function ($, _, Backbone, BaseCollections, ItemModel) {

        var ItemsCollection = BaseCollections.extend({
            
            initialize: function(models, opt){
                
                this.opt = opt;
                
                ItemsCollection.__super__.initialize.apply(this, [this.model, opt]);
                this.target = opt.target;
                if(opt){
                    switch(opt.target){
                        case "photoCoverage":
                            this.filters.set("type", 3,{
                                silent : true
                            })
                            break;

                        default :
                            break;
                    }
                }
            },
	
            url : function(){
                return "/api/" +  ((this.target != "photoCoverage") ? this.target : "news");
            },

            parse: function( resp ){
                var target = this.target,
                list = resp.list;

                _.each(list, function(item){
                    item.target = target;
                    if(item.photos && item.photos.length > 0){
                        item.photos = $.grep(item.photos, function(item){
                            return (item.deleted != 1)
                        })
                    }
                });
                return list;
            },

            nextPage : function(count) {
                var start = this.filters.get('start') + (count ? count : this.filters.get('limit'));
                this.filters.set({
                    start : start
                }, {
                    silent : true
                });
                return this;
            },

            previousPage : function(start) {
                this.filters.set({
                    start : start
                }, {
                    silent : true
                });
                return this;
            }

        });

        return ItemsCollection;
    });
