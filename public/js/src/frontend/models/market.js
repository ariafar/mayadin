define([
    'jquery',
    'backbone',
    'underscore',
    'frontend/models/item'
    ], function ($, Backbone, _, ItemModel) {

        var MarketModel = ItemModel.extend({

            defaults : {
                'id' : null ,
                'mrktName' : null ,
                'mrktParent' : null ,
                'mrktStatus' : null ,
                'mrktAddress' : null ,
                'mrktRegionNo' : null ,
                'mrktAreaNo' : null ,
                'telNumber' : null ,
                'fruitBooth' : null ,
                'vegetableBooth' : null ,
                'summerCropsBooth' : null ,
                'chickenFishBooth' : null ,
                'meatBooth' : null ,
                'diaryBooth' : null ,
                'electricBooth' : null ,
                'nutsBooth' : null ,
                'laundryBooth' : null ,
                'dressmakingBooth' : null ,
                'recycleBooth' : null ,
                'flowerBooth' : null ,
                'plasticBooth' : null ,
                'sausageBooth' : null ,
                'clothingBooth' : null ,
                'riceBooth' : null ,
                'breadBooth' : null ,
                'compoteBooth' : null ,
                'courierBooth' : null ,
                'agencyBooth' : null ,
                'detergentBooth' : null ,
                'stationeryBooth' : null ,
                'otherBooth' : null ,
                'latitude' : 35.744 ,
                'longitude' : 51.3750 ,
                'parking'   : null,
                'creationDate' : null ,
                'updateDate' : null ,
                'deleted' : null,
                'photos' : [],
                'target'    : "markets",
                'establishYear' : null
            },
            url : function() {

                if(this.get("id")){
                    return '/api/'+ this.get("target") +'/' + this.get('id') + "/?deletedImage=null&deleted=null";
                }
                else
                    return '/api/'+ this.get("target");
            }
        });

        return MarketModel;
    });
