define([
    'jquery',
    'backbone',
    'underscore'
    ], function ($, Backbone, _) {

        var ItemModel = Backbone.Model.extend({
             
            defaults : {
                id              : null,
                photoId         : null,
                title           : null,
                description     : null,
                shortDesc       : null,
                longDesc        : null,
                name            : null,
                family          : null,
                locationService : null,
                birthday        : null,
                askerName      : null,
                askerFamily    : null,
                askerEmail     : null,
                question        : null,
                answer          : null,
                path            : null,
                siteUrl         : null,
                tell            : null,
                fax             : null,
                email           : null,
                area            : null,
                address         : null,
                firstTellNumber : null,
                secondTellNumber: null,
                latitude        : null,
                longitude       : null,
                question        : null,
                history         : null,
                viewMode        : "view",
                file            : null
            },
            url : function() {
                var target = (this.get("target") != "photoCoverage") ?  this.get('target')  : "news";

                if(this.get("id")){
                    return '/api/'+ target +'/' + this.get('id') + "/?deletedImage=0&deleted=0";
                }
                else
                    return '/api/'+ target;
            }
            
        });

        return ItemModel;
    });
