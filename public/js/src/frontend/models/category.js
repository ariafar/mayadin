define([
    'jquery',
    'backbone',
    'underscore',
    'frontend/models/item'
    ], function ($, Backbone, _, ItemModel) {

        var CategoryModel = ItemModel.extend({

            defaults : {
                'id' : null,
                'title' : null,
                'photo' : null
            },
            url : function() {
              return '/api/categories/'+ (this.get('id') || "");
            }
            
        });

        return CategoryModel;
    });
