define([
    'jquery',
    'backbone',
    'underscore'
    ], function ($, Backbone, _) {

        var ItemModel = Backbone.Model.extend({

            defaults : {
                user    : null,
                item    : null
            },
            
            parse : function(resp){
                resp.item = $.parseJSON(resp.item);
                return resp;
            }
        });

        return ItemModel;
    });

