define([
    'jquery',
    'backbone',
    'underscore',
    'frontend/models/item'
    ], function ($, Backbone, _, ItemModel) {

        var TableModel = ItemModel.extend({

            defaults : {
                'id' : null,
                'description' : null
            },
            
            url : function() {
              return '/api/categories/' + this.get('categoryId') + '/tables/' + (this.get('id') || "");
            },
            
            parse : function(resp){
                _.each(resp.products, function(product){
                    var values = $.parseJSON(product.values)
                    product.values = values;
                });

                return resp;
            }
            
        });

        return TableModel;
    });
