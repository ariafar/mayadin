define([
    'jquery',
    'backbone',
    'underscore'
    ], function ($, Backbone, _) {

        var HouseModel = Backbone.Model.extend({
            
            url : function() {
                return '/api/evaluations/'+ this.get('evaluationId') + "/responses";
            }
            
        });

        return HouseModel;
    });
