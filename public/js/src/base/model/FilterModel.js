define([
    'jquery',
    'underscore',
    'backbone',
    ], function ($, _, Backbone) {
        var FilterModel =  Backbone.Model.extend({
            defaults: {
                limit: 20,
                start: 0,
                deletedImage : 0,
                deleted      : 0
            }
        });
        return FilterModel;
    });


