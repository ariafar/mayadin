define([
    'jquery',
    'underscore',
    'backbone',
    'modules/users/models/user',
], function($, _, Backbone, UserModel) {
    var PopupView = Backbone.View.extend({
        initialize: function(opt) {            
            this.PopupTpl = opt.PopupTpl;
            this.render();
        },
//        el: $('.popup-holder'),
        events: {
            'click   .email-btn': 'emailFormShow',
            'click   .email-form': 'emailFormClick',
            'click   .like-site': 'likeSite',
            'click    #collection-select': 'clickCollection',
            'change   #collection-select': 'selectCollection',
            'click    .bead-site': 'beadSite',
            'click    .send-email': 'sendEmail'
        },
                
        sendEmail: function() {
            $.ajax({
                url: '/sites/suggestions/email',
                data: {
                    'subject': $('.email-form #recpSubject').val(),
                    'email': $('.email-form #recpEmail').val(),
                    'message': $('.email-form #recpMessage').val(),
                    'site_id': self.model.get('id') // y site_id?!
                },
                dataType: 'json',
                success: function() {
                },
                error: function() {
                }
            }, this);
            $('.email-form #recpEmail').val('');
            $('.email-form #recpMessage').val('');
            $('.email-form #recpSubject').val('');
        },
        
        followOwnerUser: function() {
            if ($('#also-follow-owner-site').is(':checked')) {
                this.ownerUser.toggleFollow();
            } else {
                if (this.ownerUser.is_followed) {
                    this.ownerUser.toggleFollow();
                }
            }
        },
                
        beadSite: function(e) {
            var $this = this;
            this.ownerUser = new UserModel({id: this.model.get('owner.id')});

            $('#collection-select').on('change', function() {
                $('#user-threads-container').removeClass('error');
            });

            this.ownerUser.fetch({
                success: function() {
                    $this.followOwnerUser();
                }
            });
            
            this.hide();
            this.model.toggleFollow();
        },
        
        emailFormShow: function() {
            $('.email-form').toggle();
            return false;
        },
                
        emailFormClick: function() {
            return false;
        },
                
        likeSite: function(e) {
            success = function() {
                if (self.model.get('is_liked')) {
                    $('.like-site').html('Unlike');
                } else {
                    $('.like-site').html('Like');
                }
            };
            this.model.toggleLike(success);
            e.stopPropagation();
            return false;
        },
                
        clickCollection: function(e) {
            e.stopPropagation();
            return false;
        },
                
        selectCollection: function(e) {
            this.model.set({
                collection_id: $(e.target).val()
            });
        },
                
        render: function() {
            this.$el.append(this.PopupTpl);
            this.show();
            this.registerEvent();
            $('.email-btn').popover('hide')
        },
                
        registerEvent: function() {

        },
                
        show: function(title, message, options) {
            var self = this;
            $('body').addClass('noscroll');
            $('body').removeClass('scroll');
            //                $('.popup-modal').addClass('visible');

            $('.popup-modal').modal();
            // $('.modal-backdrop').addClass('white');
            $('#zoomScroll').removeClass('hide');
            $('#zoomScroll').addClass('loaded show');

            $('#zoomScroll').click(function() {
                $('.popup-modal').modal('hide');
            });
            $('.popup-modal').on('hidden', self.hide);
        },
                
        hide: function() {
            $('.popup-holder').empty();
            $('body').addClass('scroll');
            $('body').removeClass('noscroll');
            $('.popup-modal').removeClass('visible');
            $('.modal-backdrop').remove();
            $('#zoomScroll').removeClass('loaded show');
            $('#zoomScroll').addClass('hide');

        }
    });

    return PopupView;
});
