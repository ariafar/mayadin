define([
    'jquery',
    'backbone',
    'underscore',
    'app',
    'base/views/item_view',
    'text!base/templates/header.html',
    'i18n!nls/labels',

    ], function ($, Backbone, _, App, BaseView, HeaderTmp, labels) {

        var HeaderView = BaseView.extend({

            className : "container",

            events : {
                'click #logout'             : 'logout',
                'click .guide-modal'        : 'showGuidModal',
                'click .about-modal'        : 'showAboutModal',
                'click .menu-item a'        : 'changeRout'
            },

            initialize : function() {
                this.template = _.template(HeaderTmp);
                this.render();
            },

            render : function() {
                this.$el.html(this.template(_.extend(window.currentUser.toJSON() , {
                    labels : labels
                })));

                $('header').html(this.$el);
               
            },

            logout : function(){
                $.ajax({
                    url     : '/users/auth/logout',
                    type: "POST",
                    success : function(resp){
                        window.currentUser.clear();
                        window.location = '/';
                    },
                    error : function(error){
                        console.log(error)
                    }
                }, this);
            },

            showGuidModal : function(){
                var guidTpl =  _.template(GuidPopupTpl);
                $("#guide-modal").html(guidTpl);
                $("#guide-modal").modal('show');
            },
            
            showAboutModal : function(){
                var aboutTpl =  _.template(AboutModalTpl);
                $("#about-modal").html(aboutTpl);
                $("#about-modal").modal('show');
            },
            changeRout : function(e){
                var route = $(e.target).data('page');
                if(route)
                    Backbone.history.navigate(route, true);
            }
        });

        return HeaderView;
    });
