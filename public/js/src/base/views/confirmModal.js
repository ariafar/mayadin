define([
    'jquery',
    'underscore',
    'backbone',
    'text!base/templates/confirm.html',    
    ], function ($, _, Backbone, confirmTpl) {
        var ConfirmModalView = Backbone.View.extend({                     
            id: 'confirm-modal',
            className: 'modal popup-modal hide',
            
            initialize : function(opt) {  
                this.message = 'Are you sure you want to delete this plugin?';
                this.title = 'Confirm plugin Delete';
                this.render();        
            },
            
            render: function(){
                this.template = _.template(confirmTpl);                 
                $(document.body).append(this.$el.append(this.template({
                    message : this.message,
                    title : this.title
                })));
            },
            
            registerEvent : function(opt){
                var self = this;
                $('.confirm-action').off('click').on('click', function(e){
                    opt.confirmFunction();
                    self.$el.modal('hide');
                    return false;
                });                                

                $('.btn-cancel').off('click').on('click', function(){                    
                    self.$el.modal('hide');  
                    $('.btn-cancel').off('click');
                    return false;
                });
                               
            },
            
            show: function(opt) { 

                if(opt && (opt.message || opt.title)){
                    this.$el.html(this.template({
                        message : opt.message || this.message,
                        title : opt.title || this.title
                    }));
                }
                this.$el.modal('show');
                this.registerEvent(opt);                
            },
        
            hide: function() {
               
            }
        });

        return new ConfirmModalView();
    });
