define(
    [ "libs/async!http://maps.google.com/maps/api/js?key=AIzaSyCpKW9yCfVH4umDWpoh5gqnlp8vipn1_p4&sensor=true!callback" ],
    function() {
        return {
            addMapToCanvas: function( mapCanvas , options) {
                var myOptions =  {
                    center                  : new google.maps.LatLng( options.center_lat, options.center_lng ),
                    zoom                    : options.zoom,
                    scrollwheel             : options.scrollwheel,
                    zoomControl             : options.zoomControl,
                    scaleControl            : options.scaleControl,
                    draggable               : options.draggable,
                    disableDoubleClickZoom  : options.disableDoubleClickZoom,
                    mapTypeId               : google.maps.MapTypeId.ROADMAP
                };
                var latlng = new google.maps.LatLng(options.center_lat, options.center_lng);
                map = new google.maps.Map( mapCanvas, myOptions);
                
                new google.maps.Marker({
                    position: latlng,
                    map: map
                });                
            },

            addResizeEvent : function(){
                if( google.maps && google.maps.event){
                    google.maps.event.trigger(map, 'resize');
                    map.setZoom( map.getZoom());
                }
            },
            
            findCenter : function(){
                var obj = {
                    'center' :  map.getCenter(),
                    'zoom'   :  map.getZoom()
                }
                return obj;              
            },
            
            searchLocation : function(location, el){
                var geocoder = new google.maps.Geocoder();  
                if (geocoder == null){
                    geocoder = new google.maps.Geocoder();
                }
                geocoder.geocode( {
                    'address': location
                }, function(results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        var searchLoc = results[0].geometry.location;
                        var lat = results[0].geometry.location.lat();
                        var lng = results[0].geometry.location.lng();
                        var latlng = new google.maps.LatLng(lat, lng);
                        var mapCanvas = jQuery( ".map_canvas", el ).get( 0 );
                        
                        var myOptions =  {
                            center: new google.maps.LatLng(lat, lng),
                            zoom: 8,
                            mapTypeId: google.maps.MapTypeId.ROADMAP
                        };
                        map = new google.maps.Map( mapCanvas, myOptions );	
                        new google.maps.Marker({
                            position: latlng,
                            map: map
                        });
                    }       
                });
            }
        }
    }
    );