define([
    'jquery',
    'backbone',
    'underscore'
    ], function ($, Backbone, _) {

        var userModel = Backbone.Model.extend({

            defaults : {
                service : null,
                regions : []
            },
            
            url : function() {
                if (this.get("id") == undefined) {
                    return '/api/users/';
                } else {
                    return '/api/users/' + this.get('id');
                }
            }
            
        });

        return userModel;
    });
