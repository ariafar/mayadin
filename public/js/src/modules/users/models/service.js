define([
    'jquery',
    'backbone',
    'underscore'
    ], function ($, Backbone, _) {

        var serviceModel = Backbone.Model.extend({

            defaults : {
                'id' : null,
                'userId' : null,
                'userRegions' : null,
                'acountDuration' : null,
                'serviceType' : null,
                'balanc' : null,
                'expireDate' : null,
                "fuzzy_expire_date" : null
            },
            
            url : function() {
                return '/api/users/' + this.get('userId') + '/service/' + (this.get('id') || "") ;
            }

        });

        return serviceModel;
    });
