define([
    'jquery',
    'backbone',
    'underscore'
    ], function ($, Backbone, _) {

        var websiteModel = Backbone.Model.extend({
            defaults : {
                id                      : null,
                collection              : null,
                collection_id           : null,
                creation_date           : null,
                follow_count            : null,
                fuzzy_creation_date     : null,
                is_liked                : null,
                like_count              : null,
                role                    : null,
                site_id                 : null,
                source                  : null,
                user_id                 : null,
                user                    : null,
                site                    : null
            }
        });

        return websiteModel;
    });
