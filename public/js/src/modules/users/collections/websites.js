define([
    'jquery',
    'underscore',
    'backbone',
    'base/collection/Collection',
    'modules/users/models/website'
    ], function ($,_, Backbone, BaseCollection, WebsiteModel) {
        var WebsitesCollection = BaseCollection.extend({
            
            model : WebsiteModel,
        
            initialize : function(models, opt){                
                WebsitesCollection.__super__.initialize.apply(this, [this.model, opt]);
            },
            url   : function(){
                return '/api/sites/user/'+ this.userId +'/follows/?role=OWNER';
            },
            
            parse  : function(resp){
                if(resp && this.filters){
                    this.filters.count = resp.count;
                } 
                return resp.list;
            }
        });

        return WebsitesCollection;
    });