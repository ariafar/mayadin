define([
    'jquery',
    'underscore',
    'backbone',
    'base/collection/Collection',
    'modules/users/models/user'
    ], function ($,_, Backbone, BaseCollection, UserModel) {
        var UsersCollection = BaseCollection.extend({
            
            model : UserModel,
        
            initialize : function(models, opt){
                this.url = ( opt && opt.url ) ? opt.url : '/api/users';
                
                UsersCollection.__super__.initialize.apply(this, [this.model, opt]);
            },
            url   : function(){
                return this.url;
            },
            
            parse  : function(resp){
                if(resp && this.filters){
                    this.filters.count = resp.count;
                } 
                return resp.list;
            }
        });

        return UsersCollection;
    });