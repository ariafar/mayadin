define([
    'jquery',
    'backbone',
    'underscore',
    'app',
    'libs/jquery-helper',
    'modules/users/models/preferences',
    'modules/users/models/user',
    'text!modules/users/templates/changePassword.html',
    'libs/jqueryPlugins/jquery.complexify',
    'i18n!modules/users/nls/edit-profile',
    
    ], function ($, Backbone, _, App, jqHelper,PreferencesModel, UserModel, changePasswordTpl, jqueryComplexify, labels) {
        //jquery ui must be loaded first

        var editProfile = App.BaseView.extend({
            
            className : 'edit-profile-container',
                   
            events : {
                'click  #save-detail'                  : 'saveDetail',
                'click  #save-password'                : 'changePassword'
            //                'blur   #new_password'                 : 'checkPassword'
            },
                    
            initialize : function(opt) {
                var self = this;
                this.preferencesModel = new PreferencesModel({
                    id : window.currentUser.get('id')
                });
                this.template = _.template(changePasswordTpl);
                this.render();
            },
            
            render : function() {
                this.$el.html(this.template({
                    labels : labels
                }));

                return this;
            },

            registerEvents :function(){
                 $(".change-password-suuccessfully").on('hidden.bs.modal', function (e) {
                    Backbone.history.navigate('houses', true);
                });
            },

            checkPassword : function(e){
                if($("#new_password", this.$el).val().length > 5){
                //                    $("#new_password", this.$el).next().html('<span style="color:green;" class="voo-checkmark-2"></span>');
                } else {
                    $(".extra-chars").show();
                }
            },
            
            passwordComplexify : function(){
                var $this = this;
                $("#new_password" , $this.$el).complexify({}, function (valid, complexity) {
                    if (!valid) {
                        $('#progress',$this.$el).css({
                            'width':complexity + '%'
                        }).removeClass('progressbarValid').addClass('progressbarInvalid');
                    } else {
                        $('#progress' ,$this.$el).css({
                            'width':complexity + '%'
                        }).removeClass('progressbarInvalid').addClass('progressbarValid');
                    }
                });
            },
            
            changePassword : function(e){
                var $this = this;
                var password = $('#change-password-form', this.$el).serializeJSON();
                var valid = true;
                $(".error").hide();
                
                if($("#old_password").val() == ""){
                    $(".einter-oldpass").show();
                    valid = false;
                    return;
                }else if(password.newPass.length < 5){
                    $(".extra-chars").show();
                    valid = false;
                    return;
                }else if(password.newPass != password.confirm){
                    $(".match-passes").show();
                    valid = false;
                    return;
                }

                if(!valid)
                    return;
                else{
                    $(".error").hide();
                    var userModel = new UserModel();
                    userModel.set({
                        id : window.currentUser.get('id')
                    });

                    userModel.save({
                        'password'  : password.newPass,
                        'old'       : password.old
                    },{
                        success : function(resp){
                            $this.render();
                            $(".change-password-suuccessfully").modal('show');
                        },
                        error : function(model ,error){
                            if(error.status == 401){
                                $(".wrong-oldpass").show();
                            }
                        }
                    })
                }
            },
            saveDetail : function(){
                var user = $('#edit-profile-save', this.$el).serializeJSON();
                
                if(!this.validateFormDetail()){
                    return false;
                }
                
                window.currentUser.save({
                    first_name  : user.first_name,
                    last_name   : user.last_name,
                    phone       : user.phone,
                    username    : user.username,
                    email       : user.email,
                    gender      : user.gender,
                    birth_date  : $('#birth_date_year', this.$el).val() + '-' + $('#birth_date_month', this.$el).val() + '-' + $('#birth_date_day', this.$el).val(),
                    locale      : user.locale,
                    phone       : user.phone,
                    keywords    : user.keywords,
                    about       : user.about,
                    country     : user.country,
                    photo_id    : user.photo_id
                }, {
                    success : function(){
                        $('.success-setting', this.$el).css('visibility', 'visible');
                        $('.error-setting', this.$el).css('visibility', 'hidden');
                    },
                    error : function(e, ee){
                        $('.success-setting', this.$el).css('visibility', 'hidden');
                        $('.error-setting', this.$el).css('visibility', 'visible');
                    }
                });
                
                return false;
            }
        });

        return editProfile;
    });