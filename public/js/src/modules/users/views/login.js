define([
    'jquery',
    'backbone',
    'underscore',
    'app',
    'base/views/base_view',
    'libs/jquery-helper',   
    'modules/users/models/user',
    'text!modules/users/templates/login.html',    
    'i18n!modules/users/nls/fa-ir/labels'
    ], function ($, Backbone, _, App, BaseView, jqHelper, UserModel, loginTmp, labels) {

        var loginView = BaseView.extend({
            initialize: function() {
                this.template = _.template(loginTmp, {
                    labels: labels
                });
                this.render();
            },
            className : 'login-wrapper',
            events : {
                //                'blur  #password '     : 'passwordChange',
                'blur  #username '     : 'usernameChange',
                'keyup  #password '    : 'passwordEnter',
                'keyup  #username '    : 'usernameEnter',
                'click .login'         : 'login',
                'keydown input'        : 'login'
            },

            render : function() {
                this.$el.html(this.template);
                this.registerEvents();
                return this;
            },
            
            registerEvents : function(){
                $('.loginWarning').hide();
            },
            
            validateEmail : function(email) {
                var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                return re.test(email);
            },
        
            usernameChange : function(e){
                var el = $(e.target);
                if (el.val().length > 1 && !this.validateEmail(el.val())){
                    el.parent().addClass('error');
                }
                if (this.validateEmail($(e.target).val())){
                    el.parent().removeClass('error');
                }
                
                if (e.which == 13 && !e.shiftKey && !e.ctrlKey) {
                    $('.login-memvoo').trigger('click');
                }
            },
            
            usernameEnter : function(e){
                if (e.which == 13 && !e.shiftKey && !e.ctrlKey) {
                    $('.login-memvoo').trigger('click');
                }
            },
            
            passwordEnter : function(e){
                if (e.which == 13 && !e.shiftKey && !e.ctrlKey) {
                    $('.login-memvoo').trigger('click');
                }
            },
            
            login : function(e){
                $('.error').removeClass('error');
                $(".error-message").hide();
                
                if(e.type == "keydown" && e.keyCode != 13)
                    return;
                
                if($('#username', this.$el).val() == "" &&  $('#password', this.$el).val() == ""){
                    $('#username', this.$el).parents('.control-group').addClass('error');
                    $('#password', this.$el).parents('.control-group').addClass('error');
                    $('.userpass-required').show();
                    return false;
                }
                else if($('#username', this.$el).val() == ""){
                    $('#username', this.$el).parents('.control-group').addClass('error');
                    $('.username-required').show();
                    return false;
                }
                else if( $('#password', this.$el).val() == ""){
                    $('#password', this.$el).parents('.control-group').addClass('error');
                    $('.password-required').show();
                    return false;
                }

                $('.loading').show();
               

                $.ajax({
                    url     : '/users/auth/login',
                    data    : {
                        'email'       : $('#login-box').find('#username').val(),
                        'password'    : $('#login-box').find('#password').val(),
                        'remember_me' : $('#remember_me').attr('checked') == 'checked'? '1' : '0'
                    },
                    type: "POST",
                    success : function(resp){
                        window.currentUser.set($.parseJSON(resp));
                        if(window.currentUser.get("type") == 1 ||  (window.currentUser.get("type") != 1 && window.currentUser.get("fuzzy_expire_date") > 0  && window.currentUser.get('service_type') != 0) )
                                Backbone.history.navigate('houses', true);
                            else
                                Backbone.history.navigate('chargeService', true);

                        $('.loading').hide();

                    },
                    error : function(resp){
                        $('.invali-user').show();
                        $('#password', this.$el).parents('.control-group').addClass('error');
                        $('#username', this.$el).parents('.control-group').addClass('error');
                        $('.loading').hide();
                    }
                }, this);
                return false;
               
            }
        });

        return loginView;
    });
