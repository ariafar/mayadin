define([
    'jquery',
    'underscore',
    'backbone',
    'app',
    'modules/users/collections/users',
    'text!modules/users/templates/users_list.html',
    'modules/users/views/users_list_item',
    'modules/users/models/user',
    'base/views/list_view',    
    ], function ($, _, Backbone,  App, UsersCollection, ListTpl, UserItemView, UserModel, BaseListView) {
        var UsersListView = BaseListView.extend({
            
            className : 'users-list',
            events : {
                'click .sites-pagination'   : 'loadMore',
                'click .search'             : 'search',
                'keyup #search_user'        : 'clearSearch'
            },

            initialize : function(){
                this.collection =  new UsersCollection([]);
                UsersListView.__super__.initialize.apply(this);
            },
            
            render : function(){
                this.tpl = ListTpl;
                this.$el.html(_.template(this.tpl));
                return this;
            },

            reset : function(){

                $('#users-list', this.$el).empty();
            },
            
            renderItem: function(model) {
                if(!model.get('email') )
                    return;
                var item = new UserItemView({
                    "model": model
                });
                
                $('#users-list', this.$el).append(item.render().$el);
            },
            
            afterRenderItems : function(){
            //                App.processScroll();
            },
            search : function(e){
                var $this = this;
                var id = $("#search_user").val();
                this.oldVal = id;

                if( (e && e.type == "keyup" && e.keyCode != 13) || id == "" )
                    return;

                var user = new UserModel({
                    id : this.getNumber(id)
                });
                
                user.fetch({
                    success : function(model){
//                        $('.loading').hide();
                        $this.collection.reset();
                        $this.collection.add(model)
                    }
                });
            },

            clearSearch : function(e){
                var code = e.keyCode,
                newVal = $('#search_user', this.$el).val();
                if( code == 46 || code == 8){
                    
                    if(newVal != this.oldVal && newVal == ""){
                        this.fetchCollection();
                    }
                    this.oldVal = newVal;
                }
                else if( code == 13 ){
                    this.search();
                }
            }
        });

        return UsersListView;
    });
