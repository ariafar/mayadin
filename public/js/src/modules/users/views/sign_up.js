define([
    'jquery',
    'backbone',
    'underscore',
    'app',
    'libs/jquery-helper',
    'libs/jqueryPlugins/bootstrap-datepicker',
    'libs/jqueryPlugins/jquery.complexify',
    'modules/users/models/user',
    'text!modules/users/templates/signup.html',
    'i18n!modules/users/nls/sign-up',
    'bootstrap_selectpicker',
    'libs/jqueryPlugins/plupload.full',
    'jkblayout'
    ], function($, Backbone, _, App, jqHelper, datepicker, jqueryComplexify, userModel, userItem, labels) {

        var sign_upView = App.BaseView.extend({

            className : 'signup-page-wrapper',

            initialize: function() {
                this.template = _.template(userItem);
                this.render();
            },
                
            events: {
                'click #signup_btn': 'signup',
                //                'blur input'        : 'validate',
                //                'blur [name=email]': 'checkEmail',
                //                'blur #first_name': 'checkFirstName',
                //                'blur #last_name': 'checkLastName',
                'blur #newPass': 'checkPassword',
                'blur #rePass': 'checkRePassword'
            },
                
            render: function() {
                this.$el.html(this.template({
                    labels: labels
                }));
               
                App.processScroll();
                var $this = this;

                return this;
            },
                
            registerEvents: function() {

                $('.selectpicker').selectpicker({
                    hideDisabled : true,
                    width : "290px"
                });
            },
                
            validateEmail : function(email) {
                var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                return re.test(email);
            },

            validateMobileNumber : function(mobileNumber) {
                // 091 - 093 

                var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                return re.test(mobileNumber);
            },

            validatForm : function(){
                var valid = true,
                $this = this;
                var filled_requirment = true;
                $('.error-message').hide();
                $(".fill-requirment").hide();
                $(".error").removeClass('error');
                $('.duplicate-email-msg').hide();
                
                _.each($("[required]", this.$el), function(el){
                    if($(el).val() == ""){
                        filled_requirment = false;
                        $(el).parents('.control-group').addClass('error');
                        $(".fill-requirment").show();
                    //                        return false;
                    }
                })
                
                if(!filled_requirment)
                    return;
                
                _.each($('#signup-page input'), function(el){
                    var pattern = $(el).attr('pattern');
                    var  value = $(el).val();

                    if($(el).attr('name') == 'email'){
                        var match = $this.validateEmail(value);
                    }else{
                        var match = new RegExp(pattern).test(value)
                    }
                  
                    if(!match){
                        $(el).parent().addClass('error');
                        $(el).next().show();
                    }
                    else{
                        $(el).parent().removeClass('error');
                    }
                   
                    valid = valid && match;
                });
                

                return valid;
            },

            validate : function(e){
                var el = $(e.target);
                if(el.is(":valid")){
                    el.parent().removeClass('error');
                    el.next().hide();
                }
                else{
                    el.next().show();
                }
            },
                
            signup: function() {
                
                var validate = this.validatForm();
                if(!validate)
                    return;

                var user = $('#signup').serializeJSON(),
                new_user = new userModel(),
                $this = this;


                new_user.save(user, {
                    success: function(model) {
                        window.currentUser = model;
                        Backbone.history.navigate('chargeService', true);
                    },
                    error: function(model, resp) {

                        if(resp.status == 401){
                            $('.duplicate-email-msg').show();
                            var emialInput = $('[name=email]');
                            emialInput.val("");
                            emialInput.parent().addClass("error");
                        }

                        if(resp.status == 400){
                            var errors = $.parseJSON(resp.responseText).fields;
                            $('.error-message', $this.$el).html('');
                            _.each(errors, function(item) {
                                var formEl = $('#' + item.field, $this.$el)
                                formEl.next().html(item.message);
                            });
                        }
                    }
                });

                return false;
            },
        
            checkBirth: function(e) {
                var el = $(e.currentTarget);
                if ($('#birth_date_day').val().length == 0 || $('#birth_date_month').val().length == 0 || $('#birth_date_year').val().length == 0) {
                    $('.birthdate', this.$el).next().html('<span class="voo-notice-2"></span>&nbsp;' + labels.requireField);
                    el.parent().parent().addClass('error');
                } else {
                    $('.birthdate', this.$el).next().html('');
                    el.parent().parent().removeClass('error');
                    el.next().html('<span style="color:green;" class="voo-checkmark-2"></span>');
                }
            },
                
            checkEmail: function(e) {
                $('.duplicate-email-msg').hide();
                var email = $(e.currentTarget);
                if ((!this.validateEmail(email.val())) || (email.val() == '')) {
                    email.next().html('<span class="voo-notice-2"></span>&nbsp; ' + labels.valiMail);
                    email.parent().addClass('error');
                } else {
                    email.parent().removeClass('error');
                    email.next().html('<span style="color:green;" class="voo-checkmark-2"></span>');
                }
            },
                
            checkFirstName: function(e) {
                var el = $(e.currentTarget);
                if (el.val().length == 0) {
                    el.next().html('<span class="voo-notice-2"></span>&nbsp;' + labels.requireField);
                    el.parent().parent().addClass('error');
                } else {
                    el.parent().parent().removeClass('error');
                    el.next().html('<span style="color:green;" class="voo-checkmark-2"></span>');
                }
            },
                
            checkLastName: function(e) {
                var el = $(e.currentTarget);
                if (el.val().length == 0) {
                    el.next().html('<span class="voo-notice-2"></span>&nbsp;' + labels.requireField);
                    el.parent().parent().addClass('error');
                } else {
                    el.parent().parent().removeClass('error');
                    el.next().html('<span style="color:green;" class="voo-checkmark-2"></span>');
                }
            },
                
            checkPassword: function(e) {
                if ($("#newPass", this.$el).val().length < 6) {
                    $("#newPass", this.$el).next().html('<span class="voo-notice-2"></span>&nbsp;' + labels.requirePass);
                } else {
                    $("#newPass", this.$el).next().html('<span style="color:green;" class="voo-checkmark-2"></span>');
                }
            },
                
            checkRePassword: function(e) {
                if ($("#rePass", this.$el).val() == $("#newPass", this.$el).val()) {
                    $("#rePass", this.$el).next().html('<span style="color:green;" class="voo-checkmark-2"></span>');
                } else {
                    $("#rePass", this.$el).next().html('<span class="voo-notice-2"></span>&nbsp; Password is not correct');
                }
            }
        });

        return sign_upView;
    });
