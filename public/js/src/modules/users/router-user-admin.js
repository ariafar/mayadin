
define([
    'jquery',
    'backbone',
    'modules/users/views/admin/users_admin_list'
    
], function( $ , Backbone , UsersAdminListView ){
    
        var userAdminRouter = Backbone.Router.extend({
            routes : {
                ''        :   'listUsers',
                'edit'    :   'editUsers',
                'block'   :   'blockUser',
                'unblock' :   'unblockUser'
            },
            
            listUsers :   function(){
                var users = new UsersAdminListView({
                    
                    el : $("#main-container")
                });
                users.render();
            }
        });
        
        return userAdminRouter;
    }
);

