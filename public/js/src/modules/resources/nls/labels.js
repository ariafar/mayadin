define({
    // root is mandatory.
    'root': {
        'title'         : 'Title',
        'edit'          : 'Edit',
        'site_add'      : 'Add new site',
        'site_title'    : 'Title',
        'site_desc'     : 'Description',
        'site_status'   : 'Status',
        'site_edit'     : 'Design',
        'site_preview'  : 'Preview',
        'creation_date' : 'Creation date %s',
        'published'     : 'Published',
        'notpublished'  : 'Notpublished',
        'unavailable'   : 'Unavailable',
        'page'          : 'Page',
        'page_title'    : 'Title',
        'new_page'      : 'New Page',
        'heading'       : 'Heading',
        'next_page'     : 'next page',
        'close'         : 'Close',
        'save'          : 'Save changes',
        'remove'        : 'Remove',
        'preview'       : 'Preview',
        'category'      : 'category',
        'sub_category'  : 'sub category',
        'sub_categories': 'sub categories',
		'site_members'  : 'Members'
    },
    "fa-ir": true
});
