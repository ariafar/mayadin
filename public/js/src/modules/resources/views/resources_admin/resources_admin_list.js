define([
    'jquery',
    'underscore',
    'backbone',
    'base/views/list_view',
    'modules/resources/views/resources_admin/resources_admin_list_item',
    'i18n!modules/resources/nls/labels',
    'libs/jqueryPlugins/plupload.full',
    'libs/jqueryPlugins/jquery.plupload.queue',
    'libs/jqueryPlugins/jquery.lightbox-0.5',
    ], function ($, _, Backbone, BaseListView ,ListItemView, labels,
        PulpLoad, PulpLoadUI   ) {
        var resourcesAdminListView = BaseListView.extend({
            tagName    : 'ul',
            className : 'list-resources container-boxes photos-list',     
            events : {
                'submit form'  : 'addPhoto'
            },

            render    : function() {
                this.$el.append(
                    '<li class="resources photo-list-item-thumb" style="height:250px;">'+
                    '<form>'+
                    '<div id="uploader">'+
                    '<p>You browser doesnt have Flash, Silverlight, Gears, BrowserPlus or HTML5 support.</p>'+
                    '</div>'+
                    '</form>'+
                    '</li>'
                    );
                return this;
            },
            registerEvents : function()
            {
                var $this = this;     
                $("#uploader").pluploadQueue({
                    // General settings
                    runtimes : 'html5, html4',
                    url : 'api/albums/' + $this.options.album_id+ '/resources',
                    max_file_size : '10mb',
                    chunk_size : '1mb',
                    unique_names : true,

                    // Resize images on clientside if we can
                    resize : {
                        width : 320, 
                        height : 240, 
                        quality : 90
                    },

                    // Specify what files to browse for
                    filters : [
                    {
                        title : "Image files", 
                        extensions : "jpg,gif,png"
                    }
                    ]             
                });
                var uploader = $('#uploader').pluploadQueue();
                uploader.bind('FileUploaded', function(res , photos , resp){
                    var resp = $.parseJSON(resp.response);
                    if(res.total.failed != 1){
                        $this.collection.add(resp);
                        $(window).unbind('resize');
                        $this.afterRenderItems();
                        
                        
                        $('.plupload_buttons').show();
                        $('.plupload_total_file_size').html('0 b');
                        $('#uploader_filelist').empty();
                    }
                });  
                
                
                
                    
            },
            addPhoto : function()
            {
                var uploader = $('#uploader').pluploadQueue();

                // Files in queue upload them first
                if (uploader.files.length > 0) {
                    // When all files are uploaded submit form
                    uploader.bind('StateChanged', function() {
                        if (uploader.files.length === (uploader.total.uploaded + uploader.total.failed)) {
                            $('form')[0].submit();
                        }
                    });
                
                    uploader.start();
                } else {
                    alert('You must queue at least one file.');
                }

                var uploader = $('#uploader').pluploadQueue();
              
                return false;
                
            },
            renderItem: function(model) {
                var item = new ListItemView({
                    "model": model
                });
                
                this.$el.append(item.render().$el);
            } ,
            afterRenderItems : function(){  
                var self = this;                  
                var options = {                  
                    container: self.$el, 
                    offset: 50,
                    itemWidth: 190,
                    autoResize: true
                };
                $('li.resources' , this.$el).wookmark(options);
                $('.list-resources a.resource-list-item').lightBox();
            }
        });

        return resourcesAdminListView;
    });
