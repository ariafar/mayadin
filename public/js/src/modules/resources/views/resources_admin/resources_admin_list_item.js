define([
    'jquery',
    'underscore',
    'backbone',
    'base/views/item_view',
    'text!modules/resources/templates/resources_list_item.html',
    'text!modules/resources/templates/modal.html',
    'i18n!modules/resources/nls/labels'
    ], function ($, _, Backbone,BaseItemView, resourcesAdminItemTpl, ModalTpl, labels) {
        var resourcesAdminListItemView = BaseItemView.extend({
            tagName   : 'li',
            className : 'resources photo-list-item-thumb',
            events    : {
                'click .photo-list-item-close '      : 'deleteResources',
                "blur  .resources-title"        : "doneRename",
                "keyup .resources-title"        : "doneRename",
//                'click .resource-list-item'     : 'showModal'
            },

            initialize : function() {
                this.template = _.template(resourcesAdminItemTpl);
            },

            render    : function() {
                this.model.set({
                    thumbHeight : Math.round((Math.round(this.model.get('meta_data').height) * 180) / (this.model.get('meta_data').width))
                });
                
                var obj = _.extend(this.model.toJSON(), {
                    labels : labels
                });                
                this.$el.html(this.template(obj));

                return this;
            },
        
            deleteResources : function() {
                this.model.destroy();
                this.$el.remove();
                var self = this;                  
                var options = {                  
                    container: $('.resources-list'), 
                    offset: 50,
                    autoResize: true
                };
                $('li.resources').wookmark(options);
            },
            
            doneRename: function(e) {
                var val = '';
                if((e.type == 'keyup') && (e.which == 13) && (!e.shiftKey) && (!e.ctrlKey)){
                    return false;                    
                } else if(e.type == 'focusout') {
                    val = this.$el.find('.resources-title').html().trim();
                    this.model.save({
                        name : val
                    });
                }
            },
            
            showModal : function(){
                var obj = {
                    labels : labels
                }
                var tpl = _.template(ModalTpl, this.model.toJSON());

                var obj = $(tpl);
                obj.on('show', function() {
                    $("body").css("overflow", "hidden");
                });
                obj.on('hide', function() {
                    $("body").css("overflow", "auto");
                });
                obj.modal({
                    backdrop : 'static',
                    show     : true
                });
                
            }
        });

        return resourcesAdminListItemView;
    });
