define([
    'jquery',
    'backbone',
    'underscore'
    ], function ($, Backbone, _) {

        var ImageModel = Backbone.Model.extend({
            defaults : {
                id              : null,
                title           : null,
                coverPhoto      : 'img/photo_album.png',
                description     : null,
                album_size      : 0,
                deleted         : null,
                created_by_id   : null,
                creation_date   : null,
                updated_by_id   : null,
                update_date     : null
            },

            url : function() {
                return '/api/resources/' + (this.get('id') || "");
            }            
        });

        return ImageModel;
    });
