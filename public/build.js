({
    baseUrl: "./js/src",
    mainConfigFile: './js/src/main.js',
    dir:'./js/release',
    logLevel:0,
    preserveLicenseComments:!1,
    optimize:"uglify2",
    removeCombined: true,
    findNestedDependencies: true,

    // 3rd party script alias names
    paths: {        
        jquery                      : 'vendor/jquery/jquery',
        underscore                  : 'vendor/underscore/underscore-min',
        'underscore.string'         : 'vendor/underscore.string/lib/underscore.string',
        backbone                    : 'vendor/backbone/backbone-min',
        'backbone.routefilter'      : 'vendor/backbone.routefilter/src/backbone.routefilter',
        'deep-model'                : 'vendor/backbone-deep-model/src/deep-model',
        bootstrap                   : 'vendor/bootstrap-3.0.2-dist/dist/js/bootstrap.min',
        text                        : 'vendor/requirejs-text/text',
        json                        : 'vendor/requirejs-plugins/src/json',
        i18n                        : 'vendor/requirejs-i18n/i18n',
        'contextmenu'               : 'vendor/sydcanem-bootstrap-contextmenu/bootstrap-contextmenu',
        'pwt-date'                  : 'vendor/pwt.datepicker-master/js/pwt-date',
        'pwt-datepicker.min'        : 'vendor/pwt.datepicker-master/js/pwt-datepicker',
        "bootstrap_selectpicker"    : 'vendor/bootstrap-plugins/bootstrap-select.min',
        "rtl-slider"                : 'vendor/jquery/jquery.ui.slider-rtl.min',
        "table-sorter"              : 'vendor/tablesorter-master/js/jquery.tablesorter.min',
        "table-sorter-widget"       : 'vendor/tablesorter-master/js/jquery.tablesorter.widgets',
        "jquery-ui"                 : 'vendor/jquery/jquery-ui-1.10.3.custom.min',
        "jquery-helper"             : 'vendor/jquery/jquery-helper',
        "pageslide"                 : 'vendor/jquery/jquery.pageslide',
        "sider"                     : 'vendor/jquery.sidr',
        'jalali'                    : 'vendor/JalaliJSCalendar-master/jalali',
        'calendar'                  : 'vendor/JalaliJSCalendar-master/calendar',
        'calendar-setup'            : 'vendor/JalaliJSCalendar-master/calendar-setup',
        'calendar-fa'               : 'vendor/JalaliJSCalendar-master/lang/calendar-fa',
        'smartWizard'               : 'vendor/jQuery-Smart-Wizard-master/js/jquery.smartWizard',
        'map'                       : 'vendor/map/gmap3',
        'backbone.googlemaps'       : 'vendor/backbone.googlemaps-master/lib/backbone.googlemaps',
        'PopoverView'               : 'vendor/PopoverView',
        'jkblayout'                 : 'vendor/jkblayout.min',
        'NumberToLetter'            : 'vendor/NumberToLetter',
        'icheck'                    : 'vendor/iCheck-master/icheck.min',
        'sly'                       : 'vendor/sly',
        'kwicks'                    : 'vendor/kwicks-master/jquery.kwicks'

    },

    // Sets the use.js configuration for your application
    use: {
        bootstrap : {
            deps : ['jquery']
        },

        'pwt-date' : {
            deps : ['jquery']
        },
        'pwt-datepicker.min' :{
            deps :['jquery', 'pwt-date']
        },
        
        'rtl-slider': {
            deps: ['jquery','jquery-ui']
        },
        'underscore.string' : {
            deps : ['underscore']
        },

        'backbone.routefilter' : {
            deps : ['backbone', 'underscore']
        },
        'kwicks' : {
            deps : ['jquery']
        },

        'table-sorter' : {
            deps : ['jquery']
        },

        'table-sorter-widget' :{
            deps : ['jquery','table-sorter']
        },

        'pageslide' : {
            deps : ['jquery']
        },

        'sly' : {
            deps : ['jquery']
        },
        'bootstrap_selectpicker' : {
            deps: ['jquery','bootstrap']
        },
        'underscore': {
            attach: "_" //attaches "_" to the window object
        },
        'backbone': {
            deps : ['underscore', 'jquery'],
            attach: "Backbone"  //attaches "Backbone" to the window object
        }
    }, 
   
    name: "main"

})
