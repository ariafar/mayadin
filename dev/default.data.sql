
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) COLLATE utf8_persian_ci DEFAULT NULL,
  `family` varchar(64) COLLATE utf8_persian_ci DEFAULT NULL,
  `username` varchar(128) COLLATE utf8_persian_ci DEFAULT NULL,
  `password` varchar(64) COLLATE utf8_persian_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;


DROP TABLE IF EXISTS `galleries`;
CREATE TABLE IF NOT EXISTS `galleries` (
    `id`                INT(20)      UNSIGNED NOT NULL AUTO_INCREMENT,
    `title`             VARCHAR(500)     NULL,
    `description`       TEXT   NULL,
    `creationDate`      DATETIME            NULL,
    `updateDate`        DATETIME            NULL,
    `imageCount`        INT(4)   NOT NULL DEFAULT 0,
    `deleted`           BOOLEAN NOT NULL DEFAULT FALSE,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci AUTO_INCREMENT=1 ;


DROP TABLE IF EXISTS `news`;
CREATE TABLE IF NOT EXISTS `news` (
    `id`                INT(20)      UNSIGNED NOT NULL AUTO_INCREMENT,
    `title`             VARCHAR(500)     NULL,
    `shortDesc`         TEXT   NULL,
    `longDesc`          TEXT   NULL,
    `creationDate`      DATETIME            NULL,
    `updateDate`        DATETIME            NULL,
    `notification`      BOOLEAN NOT NULL DEFAULT FALSE,
    `deleted`           BOOLEAN NOT NULL DEFAULT FALSE,
    `type`              TINYINT  NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci AUTO_INCREMENT=1 ;


DROP TABLE IF EXISTS `resources`;
CREATE TABLE IF NOT EXISTS `resources` (
    `id` INT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(64) NULL,
	`meta_data` VARCHAR(64) NULL,
	`parentId` INT(20) NULL,
	`title` VARCHAR(500) NULL,
	`location` VARCHAR(64) NULL,
	`description` TEXT NULL,
	`creationDate` DATETIME NULL,
    `updateDate` DATETIME NULL,
	`parentType` VARCHAR(64) NULL,
	`deleted`           BOOLEAN NOT NULL DEFAULT FALSE,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci AUTO_INCREMENT=1 ;


DROP TABLE IF EXISTS `educations`;
CREATE TABLE IF NOT EXISTS `educations` (
    `id`                INT(20)      UNSIGNED NOT NULL AUTO_INCREMENT,
    `title`             VARCHAR(500)     NULL,
    `description`       TEXT   NULL,
    `category`          TINYINT,
    `creationDate`      DATETIME            NULL,
    `updateDate`        DATETIME            NULL,
    `deleted`           BOOLEAN NOT NULL DEFAULT FALSE,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci AUTO_INCREMENT=1 ;


DROP TABLE IF EXISTS `memories`;
CREATE TABLE IF NOT EXISTS `memories` (
    `id`                INT(20)      UNSIGNED NOT NULL AUTO_INCREMENT,
    `title`             VARCHAR(500)     NULL,
    `description`       TEXT   NULL,
    `creationDate`      DATETIME            NULL,
    `updateDate`        DATETIME            NULL,
    `deleted`           BOOLEAN NOT NULL DEFAULT FALSE,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci AUTO_INCREMENT=1 ;


DROP TABLE IF EXISTS `humans`;
CREATE TABLE IF NOT EXISTS `humans` (
    `id`                INT(20)      UNSIGNED NOT NULL AUTO_INCREMENT,
    `photoId`           INT(20)         NULL,
    `name`              VARCHAR(64)     NULL,
    `type`              TINYINT         NULL,
    `family`            VARCHAR(64)     NULL,
    `birthday`          DATETIME        NULL,
    `testimonyAddress`  VARCHAR(200)    NULL,
    `martyrdom`         VARCHAR (15)    NULL,
    `history`           TEXT NULL,
    `locationService`    VARCHAR (50)   NULL,
    `degree`            VARCHAR (15)    NULL,
    `testimonyDate`     DATETIME        NULL,
    `updateDate`        DATETIME        NULL,
    `deleted`           BOOLEAN NOT NULL DEFAULT FALSE,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci AUTO_INCREMENT=1 ;


DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
    `id`                INT(20)      UNSIGNED NOT NULL AUTO_INCREMENT,
    `title`             VARCHAR(64)     NULL,
    `updateDate`        DATETIME            NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci AUTO_INCREMENT=1 ;


DROP TABLE IF EXISTS `evaluations`;
CREATE TABLE IF NOT EXISTS `evaluations` (
    `id`                INT(20)      UNSIGNED NOT NULL AUTO_INCREMENT,
    `title`             VARCHAR(64)     NULL,
    `status`            INT(1) NOT NULL DEFAULT 1,
    `winnerId`           VARCHAR(11),
    `winnerName`         VARCHAR(64)     NULL,
    `responseCount`     INT   NULL,
    `type`              VARCHAR(64)     NOT NULL,
    `creationDate`      DATETIME  NULL,
    `updateDate`        DATETIME            NULL,
    `deleted`           BOOLEAN NOT NULL DEFAULT FALSE,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci AUTO_INCREMENT=1 ;



DROP TABLE IF EXISTS `questions`;
CREATE TABLE IF NOT EXISTS `questions` (
    `id`                INT(20)      UNSIGNED NOT NULL AUTO_INCREMENT,
    `evaluationId`      INT(20),
    `question`          VARCHAR(64)     NULL,
    `type`              TINYINT,
    `answer`            TINYINT NULL,
    `responseCount`     TINYINT,
    `creationDate`      DATETIME            NULL,
    `updateDate`        DATETIME            NULL,
    `deleted`           BOOLEAN NOT NULL DEFAULT FALSE,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci AUTO_INCREMENT=1;


DROP TABLE IF EXISTS `options`;
CREATE TABLE IF NOT EXISTS `options` (
    `id`                INT(20)      UNSIGNED NOT NULL AUTO_INCREMENT,
    `evaluationId`      INT(20),
    `questionId`        INT(20),
    `text`              VARCHAR(200) ,
    `value`             TINYINT,
    `responseCount`     INT(4),
    `statistics`        TINYINT,
    `ordering`        TINYINT NULL,
    `deleted`           BOOLEAN NOT NULL DEFAULT FALSE,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci AUTO_INCREMENT=1 ;


DROP TABLE IF EXISTS `responses`;
CREATE TABLE IF NOT EXISTS `responses` (
    `id`                INT(20)      UNSIGNED NOT NULL AUTO_INCREMENT,
    `responderId`       VARCHAR(11),
    `responderName`     VARCHAR(200),
    `evaluationId`      INT(20),
    `questionId`        INT(20),
    `value`             TINYINT,
    `creationDate`        DATETIME,
    `updateDate`        DATETIME,
    `statistics`        DATETIME,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci AUTO_INCREMENT=1 ;


DROP TABLE IF EXISTS `responders`;
CREATE TABLE IF NOT EXISTS `responders` (
    `id`                INT(20)      UNSIGNED NOT NULL AUTO_INCREMENT,
    `responderId`       VARCHAR(11),
    `responderName`     VARCHAR(200),
    `evaluationId`      INT(20),
    `trueAnswersCount`  INT(5),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci AUTO_INCREMENT=1 ;


DROP TABLE IF EXISTS `devices`;
CREATE TABLE IF NOT EXISTS `devices` (
    `id`                INT(20)      UNSIGNED NOT NULL AUTO_INCREMENT,
    `type`              TINYINT,
    `deviceId`          VARCHAR(64)     NULL,
    `brand`             VARCHAR(64)     NULL,
    `model`             VARCHAR(64)     NULL,
    `manufacturer`      VARCHAR(64)     NULL,
    `version`           TINYINT,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci AUTO_INCREMENT=1 ;


DROP TABLE IF EXISTS `reports`;
CREATE TABLE IF NOT EXISTS `reports` (
    `id`                INT(20)      UNSIGNED NOT NULL AUTO_INCREMENT,
    `description`       TEXT     NULL,
    `latitude`          FLOAT,
    `longitude`         FLOAT,
    `tell`              VARCHAR(20)  NULL,
    `creationDate`      DATETIME            NULL,
    `updateDate`        DATETIME            NULL,
    `deleted`           BOOLEAN NOT NULL DEFAULT FALSE,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci AUTO_INCREMENT=1 ;



DROP TABLE IF EXISTS `links`;
CREATE TABLE IF NOT EXISTS `links` (
    `id`                INT(20)      UNSIGNED NOT NULL AUTO_INCREMENT,
    `parentId`          INT(20) NOT NULL DEFAULT 0,
    `fileId`            INT(20) NULL,
    `title`             VARCHAR(200)  NULL,
    `description`       TEXT NULL,
    `path`              VARCHAR(500) NULL,
    `type`              TINYINT,
    `category`          TINYINT,
    `isFile`            BOOLEAN NOT NULL DEFAULT true,
    `creationDate`      DATETIME            NULL,
    `updateDate`        DATETIME            NULL,
    `deleted`           BOOLEAN NOT NULL DEFAULT FALSE,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci AUTO_INCREMENT=1 ;


DROP TABLE IF EXISTS `offices`;
CREATE TABLE IF NOT EXISTS `offices` (
    `id`                INT(20)      UNSIGNED NOT NULL AUTO_INCREMENT,
    `title`             VARCHAR(200)  NULL,
    `description`       TEXT NULL,
    `siteUrl`           VARCHAR(200)  NULL,
    `email`             VARCHAR(200)  NULL,
    `ceo`               VARCHAR(200)  NULL,
    `tell`              VARCHAR(200)  NULL,
    `fax`               VARCHAR(200)  NULL,
    `economicCode`      VARCHAR(200)  NULL,
    `creationDate`      DATETIME            NULL,
    `updateDate`        DATETIME            NULL,
    `deleted`           BOOLEAN NOT NULL DEFAULT FALSE,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci AUTO_INCREMENT=1 ;


DROP TABLE IF EXISTS `questions_answers`;
CREATE TABLE IF NOT EXISTS `questions_answers` (
    `id`                INT(20)      UNSIGNED NOT NULL AUTO_INCREMENT,
    `type`              TINYINT,
    `askerName`         VARCHAR(200)  NULL,
    `askerFamily`       VARCHAR(200)  NULL,
    `askerEmail`        VARCHAR(200)  NULL,
    `title`             VARCHAR(200)  NULL,
    `question`          TEXT NULL,
    `answer`            TEXT NULL,
    `creationDate`      DATETIME NULL,
    `updateDate`        DATETIME NULL,
    `deleted`           BOOLEAN NOT NULL DEFAULT FALSE,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci AUTO_INCREMENT=1 ;



DROP TABLE IF EXISTS `markets`;
CREATE TABLE IF NOT EXISTS `markets` (
  `id` int(20) NOT NULL,
  `mrktName` varchar(60) COLLATE utf8_persian_ci NOT NULL,
  `mrktParent` int(20) NOT NULL,
  `mrktStatus` int(1) NOT NULL,
  `mrktAddress` varchar(500) COLLATE utf8_persian_ci NOT NULL,
  `mrktRegionNo` int(3)  NULL,
  `mrktAreaNo` int(3)  NULL,
  `creationDate` date DEFAULT NULL,
  `updateDate` date DEFAULT NULL,
  `telNumber` varchar(256) COLLATE utf8_persian_ci DEFAULT NULL,
  `fruitBooth` int(3)  NULL,
  `vegetableBooth` int(3)  NULL,
  `summerCropsBooth` int(3)  NULL,
  `chickenFishBooth` int(3)  NULL,
  `meatBooth` int(3)  NULL,
  `diaryBooth` int(3)  NULL,
  `electricBooth` int(3)  NULL,
  `nutsBooth` int(3)  NULL,
  `laundryBooth` int(3)  NULL,
  `dressmakingBooth` int(3)  NULL,
  `recycleBooth` int(3)  NULL,
  `flowerBooth` int(3)  NULL,
  `plasticBooth` int(3)  NULL,
  `sausageBooth` int(3)  NULL,
  `clothingBooth` int(3)  NULL,
  `riceBooth` int(3)  NULL,
  `breadBooth` int(3)  NULL,
  `compoteBooth` int(3)  NULL,
  `courierBooth` int(3)  NULL,
  `agencyBooth` int(3)  NULL,
  `detergentBooth` int(3)  NULL,
  `stationeryBooth` int(3)  NULL,
  `otherBooth` int(3)  NULL,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `deleted`           BOOLEAN NOT NULL DEFAULT FALSE,
  `parking`     int(3)  NULL,
  `establishYear` varchar(4),
    PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci AUTO_INCREMENT=1 ;


DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
    `id`                INT(20)      UNSIGNED NOT NULL AUTO_INCREMENT,
    `title`             VARCHAR(500)     NULL,
    `creationDate`      DATETIME            NULL,
    `updateDate`        DATETIME            NULL,
    `deleted`           BOOLEAN NOT NULL DEFAULT FALSE,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci AUTO_INCREMENT=1 ;


DROP TABLE IF EXISTS `tables`;
CREATE TABLE IF NOT EXISTS `tables` (
    `id`                INT(20)      UNSIGNED NOT NULL AUTO_INCREMENT,
    `categoryId`        INT(20) NULL,
    `description`       TEXT NULL,
    `creationDate`      DATETIME            NULL,
    `updateDate`        DATETIME            NULL,
    `deleted`           BOOLEAN NOT NULL DEFAULT FALSE,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci AUTO_INCREMENT=1 ;


DROP TABLE IF EXISTS `fields`;
CREATE TABLE IF NOT EXISTS `fields` (
    `id`                INT(20)      UNSIGNED NOT NULL AUTO_INCREMENT,
    `tableId`           INT(20) NULL,
    `name`              VARCHAR(500)     NULL,
    `creationDate`      DATETIME            NULL,
    `updateDate`        DATETIME            NULL,
    `deleted`           BOOLEAN NOT NULL DEFAULT FALSE,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci AUTO_INCREMENT=1 ;


DROP TABLE IF EXISTS `products`;
CREATE TABLE IF NOT EXISTS `products` (
    `id`                INT(20)      UNSIGNED NOT NULL AUTO_INCREMENT,
    `tableId`           INT(20) NULL,
    `categoryId`        INT(20) NULL,
    `values`            TEXT NOT NULL,
    `creationDate`      DATETIME            NULL,
    `updateDate`        DATETIME            NULL,
    `deleted`           BOOLEAN NOT NULL DEFAULT FALSE,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci AUTO_INCREMENT=1 ;




-- *********************************************************
-- *********************************************************
-- *********************************************************

DROP TABLE IF EXISTS `markets`;
CREATE TABLE IF NOT EXISTS `markets` (
  `marketCode` int(20) NOT NULL,
  `marketName` varchar(60) CHARACTER SET utf8 COLLATE utf8_persian_ci NOT NULL,
  `marketParent` varchar(3) CHARACTER SET utf8 COLLATE utf8_persian_ci NOT NULL,
  `marketStatus` varchar(1) CHARACTER SET utf8 COLLATE utf8_persian_ci NOT NULL,
  `boothCount` int(11) DEFAULT NULL,
  `kioskCount` int(11) DEFAULT NULL,
  `placeCount` int(11) DEFAULT NULL,
  `marketAddress` varchar(500) CHARACTER SET utf8 COLLATE utf8_persian_ci DEFAULT NULL,
  `marketRegionNumber` varchar(3) CHARACTER SET utf8 COLLATE utf8_persian_ci DEFAULT NULL,
  `marketAreaNumber` varchar(3) CHARACTER SET utf8 COLLATE utf8_persian_ci DEFAULT NULL,
  `marketIBCode` varchar(10) CHARACTER SET utf8 COLLATE utf8_persian_ci DEFAULT NULL,
  `marketPostalCode` varchar(20) CHARACTER SET utf8 COLLATE utf8_persian_ci DEFAULT NULL,
  `accessCenters` varchar(10) CHARACTER SET utf8 COLLATE utf8_persian_ci DEFAULT NULL,
  `latitude`          VARCHAR (15)    NULL,
  `longitude`          VARCHAR (15)    NULL,
  `creationDate`      DATETIME            NULL,
  `updateDate`        DATETIME            NULL,
  `deleted`           BOOLEAN NOT NULL DEFAULT FALSE,
 PRIMARY KEY (`marketCode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

DROP TABLE IF EXISTS `products`;
CREATE TABLE IF NOT EXISTS `products` (
  `productCode`  int(20) NOT NULL,
  `productTitle` varchar(150) CHARACTER SET utf8 COLLATE utf8_persian_ci NOT NULL,
  `unitCode` varchar(2) CHARACTER SET utf8 COLLATE utf8_persian_ci NOT NULL,
  `groupId` varchar(2) CHARACTER SET utf8 COLLATE utf8_persian_ci DEFAULT NULL,
  `departmentId` varchar(3) CHARACTER SET utf8 COLLATE utf8_persian_ci DEFAULT NULL,
  `typeId` varchar(2) CHARACTER SET utf8 COLLATE utf8_persian_ci DEFAULT NULL,
  `itemId` varchar(3) CHARACTER SET utf8 COLLATE utf8_persian_ci DEFAULT NULL,
  `productSource` varchar(1) CHARACTER SET utf8 COLLATE utf8_persian_ci DEFAULT NULL,
  `ptCode` varchar(2) CHARACTER SET utf8 COLLATE utf8_persian_ci DEFAULT NULL,
  `price` FLOAT NULL,
  `grade1Price` FLOAT NULL,
  `grade2Price` FLOAT  NULL,
  `premiumPrice` FLOAT NULL,
  `packingPrice` FLOAT NULL,
  `efficacyDate` DATETIME            NULL,
  `creationDate`      DATETIME            NULL,
  `updateDate`        DATETIME            NULL,
  `deleted`           BOOLEAN NOT NULL DEFAULT FALSE,
PRIMARY KEY (`productCode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;


DROP TABLE IF EXISTS `product_price_head`;
CREATE TABLE IF NOT EXISTS `product_price_head` (
  `productPriceNumber` varchar(6) CHARACTER SET utf8 COLLATE utf8_persian_ci NOT NULL,
  `efficacyDate` date NOT NULL,
  `description` varchar(200) CHARACTER SET utf8 COLLATE utf8_persian_ci DEFAULT NULL,
  `creationDate`      DATETIME            NULL,
  `updateDate`        DATETIME            NULL,
  `deleted`           BOOLEAN NOT NULL DEFAULT FALSE,
PRIMARY KEY (`productPriceNumber`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;



DROP TABLE IF EXISTS `product_price_detail`;
CREATE TABLE IF NOT EXISTS `product_price_detail` (
  `productPriceNumber` varchar(6)  NOT NULL,
  `productCode` varchar(4)  NOT NULL,
  `rowNumber` int(11) NOT NULL,
  `price` float NOT NULL,
  `grade1Price` float DEFAULT NULL,
  `grade2Price` float DEFAULT NULL,
  `premiumPrice` float DEFAULT NULL,
  `packingPrice` float DEFAULT NULL,
  `creationDate`      DATETIME            NULL,
  `updateDate`        DATETIME            NULL,
  `deleted`           BOOLEAN NOT NULL DEFAULT FALSE,
PRIMARY KEY (`productPriceNumber`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;




DROP TABLE IF EXISTS `activities`;
CREATE TABLE IF NOT EXISTS `activities` (
  `id` INT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `target` varchar(10) NOT NULL,
  `action` varchar(10) NOT NULL,
  `userId` INT(20)   NOT NULL,
  `itemid` INT(20)  NOT NULL,
  `item` TEXT NOT NULL,
  `creationDate`      DATETIME  NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci AUTO_INCREMENT=1 ;


