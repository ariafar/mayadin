<?php

require_once 'Zend/Filter/Interface.php';

class Tea_Filter_StringToArray implements Zend_Filter_Interface
{
    public function filter($value)
    {
        $array = explode(',', $value);
        foreach ($array as $key => &$item) {
            $item = trim($item);
            if (empty($item)) {
                unset($array[$key]);
            }
        }
        return array_values($array);
    }
}
