<?php

class Tea_Model_Actions_Abstract
{
    public function render($activity)
    {
        return null;
    }

    public function getNotificationMessage($activity)
    {
        return null;
    }

    public function getFollowers($activity)
    {
        $uids = array(
            $activity->getActorId()
        );

        return array_unique($uids);
    }

    public function getNotifReceivers($activity)
    {
        $uids = array();

        return $uids;
    }

    public function sendEmail($activity)
    {
    }

    public function isLikable()
    {
        return false;
    }

    public function isCommentable()
    {
        return false;
    }

    public function isSharable()
    {
        return false;
    }

    public function isFollowable()
    {
        return false;
    }

    public function isVotable()
    {
        return false;
    }

    protected function sendSocialStatus($activity)
    {
        return false;
    }

    protected function sendFacebookFeed($user, $params)
    {
        $data = array(
            'user'   => $user,
            'params' => $params
        );

        Tea_Service_Gearman::getInstance()->doBackground('users', 'send_facebook_feed', $data);
    }

    protected function sendFacebookAction($user, $action, $params)
    {
        $data = array(
            'user'   => $user,
            'action' => $action,
            'params' => $params
        );

        Tea_Service_Gearman::getInstance()->doBackground('users', 'send_facebook_action', $data);
    }

    protected function tweetInTwitter($user, $message, $link)
    {
        $data = array(
            'user'    => $user,
            'message' => $message,
            'link'    => $params
        );

        Tea_Service_Gearman::getInstance()->doBackground('users', 'tweet_in_twitter', $data);
    }
}
