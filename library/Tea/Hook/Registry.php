<?php

class Tea_Hook_Registry
{
    const HOOKS_REGISTRY_KEY = 'TEA_Hooks';

    public static function getHooks($eventName = null)
    {
        if (is_null($eventName)) {
            return self::_getHooks();
        } elseif (is_string($eventName)
            && array_key_exists(strtolower($eventName), self::_getHooks())
        ) {
            $hooks = self::_getHooks();
            return $hooks[strtolower($eventName)];
        }

        return null;
    }

    public static function addHook($eventName, $hookName)
    {
        if (is_string($eventName) && is_string($hookName)) {
            $eventName = strtolower($eventName);
            $hooks     = self::_getHooks();

            $hooks[$eventName][] = $hookName;
            self::_setHooks($hooks);
        }
    }

    public static function removeHook($eventName, $hookName)
    {
        if (is_string($eventName) && is_string($hookName)) {

            $eventName = strtolower($eventName);
            $hooks     = self::_getHooks();

            for ($i = 0; $i < count($hooks); $i++) {
                if ($hooks[$eventName][$i] === $hook) {
                    unset($hooks[$eventName][$i]);
                }
            }
            // save to registry
            self::_setHooks($hooks);
        }
    }

    public static function clearAllHooks()
    {
        self::_setHooks(array());
    }

    public static function dispatchEvent($eventName, $value = null)
    {
        $eventName = strtolower($eventName);
        $hooks     = self::_getHooks();
        if (array_key_exists($eventName, $hooks)) {

            foreach ($hooks[$eventName] as $hook) {
                if (class_exists($hook, true)) {
                    $obj   = new $hook();
                    $obj->execute($eventName, $value);
                }
            }
        }

    }

    protected static function _getHooks()
    {
        if (Zend_Registry::isRegistered(self::HOOKS_REGISTRY_KEY)) {
            return Zend_Registry::get(self::HOOKS_REGISTRY_KEY);
        } else {
            Zend_Registry::set(self::HOOKS_REGISTRY_KEY, array());
            return Zend_Registry::get(self::HOOKS_REGISTRY_KEY);
        }
    }

    protected static function _setHooks($value)
    {
        Zend_Registry::set(self::HOOKS_REGISTRY_KEY, $value);
    }

}
