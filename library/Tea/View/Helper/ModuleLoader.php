<?php

class Tea_View_Helper_ModuleLoader extends Zend_View_Helper_Abstract
{
    private $modules = array();
    /**
     * Registry key for placeholder
     * @var string
     */
    protected $_regKey = 'Tea_View_Helper_LoadBackboneModule';

    public function moduleLoader()
    {
        return $this;
    }

    public function appendModule($module)
    {
        if (!in_array($module, $this->modules)) {
            $this->modules[] = $module;
        }
        return $this;
    }

    public function getModules()
    {
        return $this->modules;
    }
}
