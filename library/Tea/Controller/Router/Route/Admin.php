<?php

require_once 'Zend/Controller/Router/Route/Module.php';

class Tea_Controller_Router_Route_Admin extends Zend_Controller_Router_Route_Module
{
    public function getVersion()
    {
        return 1;
    }

    public function __construct($front, $defaults = array())
    {
        $this->_defaults   = $defaults;
        $this->_dispatcher = $front->getDispatcher();
        $this->request     = $front->getRequest();
    }

    public function match($path, $partial = false)
    {
        $this->_setRequestKeys();

        $path = trim($path, self::URI_DELIMITER);
        $path = explode(self::URI_DELIMITER, $path);
        if (!isset($path[0]) || strtolower($path[0]) != 'admin') {
            return false;
        }
        if (isset($path[1])) {
            $path[0] = $path[1];
        } else {
            $path[0] = 'default';
        }

        $path[1] = 'admin';
        $path    = implode(self::URI_DELIMITER, $path);

        return parent::match($path, $partial);
    }
}
