<?php

class Tea_Controller_Rest_Action extends Zend_Rest_Controller
{

    protected $_params = null;
    protected $_rawBody = null;
    protected $_currentUser = null;

    public function init()
    {
        $this->_helper->getHelper('layout')->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $this->_params = $this->getRequest()->getParams();

        if ($this->_rawBody === null) {
            $this->_rawBody = $this->getRequest()->getRawBody();
            $_params = json_decode($this->_rawBody, true);
            if (is_null($_params)) {
                parse_str($this->_rawBody, $_params);
            }
            if (is_array($_params)) {
                $this->_params = array_merge($this->_params, $_params);
            }
        }

        $this->_currentUser = Users_Service_Auth::getInstance()->getCurrentUser();
    }

    public function indexAction()
    {
        $this->getResponse()
                ->setHttpResponseCode(501)
                ->appendBody("Not Implemented");
    }

    public function getAction()
    {
        $this->getResponse()
                ->setHttpResponseCode(501)
                ->appendBody("Not Implemented");
    }

    public function postAction()
    {
        $this->getResponse()
                ->setHttpResponseCode(501)
                ->appendBody("Not Implemented");
    }

    public function putAction()
    {
        $this->getResponse()
                ->setHttpResponseCode(501)
                ->appendBody("Not Implemented");
    }

    public function deleteAction()
    {
        $this->getResponse()
                ->setHttpResponseCode(501)
                ->appendBody("Not Implemented");
    }

    public function getParams()
    {
        return $this->_params;
    }

    public function getParam($key, $default = null)
    {
        $key = (string) $key;
        if (isset($this->_params[$key])) {
            return $this->_params[$key];
        }

        return $default;
    }

    public function isAccessible($context, $action, $item = "*", $returnResponse = true)
    {
        if ($this->_currentUser == null) {
            $this->getResponse()
                    ->setHttpResponseCode(401)
                    ->appendBody("Unauthorized");
            return false;
        }

        $acl = Zend_Registry::getInstance()->get('acl');
        if (!$acl->isAccessible($context, $action, $item)) {
            $this->getResponse()
                    ->setHttpResponseCode(403)
                    ->appendBody("Forbidden");
            return false;
        }

        return true;
    }

    protected function getFuzzyFormat($datetime)
    {
        include_once 'Date_HumanDiff/HumanDiff.php';
        $dh = new Date_HumanDiff();
        return $dh->get($datetime);
    }

    protected function checkParams($requiredParams)
    {
        $params = $this->getParams();

        foreach ($requiredParams as $reqParam) {
            if (!isset($params[$reqParam])) {
                $this->getResponse()
                        ->setHttpResponseCode(400)
                        ->appendBody("Not Found " . $reqParam);
                return false;
            }

            if (is_string($params[$reqParam])) {
                if (strlen(trim($params[$reqParam])) == 0) {
                    $this->getResponse()
                            ->setHttpResponseCode(400)
                            ->appendBody("Not Found " . $reqParam);
                    return false;
                }
            }
        }
        return true;
    }

    protected function pushActivity($item, $action)
    {
        $authService = Users_Service_Auth::getInstance();
        $user = $authService->getCurrentUser();

        $activity_service = Activities_Service_Activity::getInstance();
        $activity = new Activities_Model_Activity();
        $activity->fill(array(
            'userId' => $user->getId(),
            'action' => $action,
            'itemId' => $item['id'],
            'target'=> strtoupper($this->getParam('module')),
            'item' => json_encode($item)
        ));

        $activity_service->save($activity);
    }

    protected function getItemHistory($itemId)
    {
        
    }

}
