<?php
//namespace Everyman\Neo4j\Batch;
//
//use Everyman\Neo4j\Batch,
//	Everyman\Neo4j\Command\Batch as Command,
//	Everyman\Neo4j\Index,
//	Everyman\Neo4j\PropertyContainer;

/**
 * An add-to-index operation
 */
class Tea_Neo4j_Batch_AddTo extends Tea_Neo4j_Batch_Operation
{
	protected $command = null;
	protected $index = null;
	protected $key = null;
	protected $value = null;

	/**
	 * Build the operation
	 *
	 * @param Tea_Neo4j_Batch $batch
	 * @param Tea_Neo4j_Index $index
	 * @param Tea_Neo4j_PropertyContainer $entity
	 * @param string $key
	 * @param string $value
 * @param integer $opId
	 */
	public function __construct(Tea_Neo4j_Batch $batch, Tea_Neo4j_Index $index, Tea_Neo4j_PropertyContainer $entity, $key, $value, $opId)
	{
		parent::__construct($batch, 'addto', $entity, $opId);
		$this->index = $index;
		$this->key = $key;
		$this->value = $value;
	}

	/**
	 * Get the command that represents this operation
	 *
	 * @return Tea_Neo4j_Command_Batch_Command
	 */
	public function getCommand()
	{
		if (!$this->command) {
			$this->command = new Tea_Neo4j_Command_Batch_AddToIndex($this->batch->getClient(),
				$this->index, $this->entity, $this->key, $this->value, $this->opId, $this->batch);
		}
		return $this->command;
	}
	
	/**
	 * Get the index
	 *
	 * @return Index
	 */
	public function getIndex()
	{
		return $this->index;
	}
	
	/**
	 * Get the key being indexed
	 *
	 * @return string
	 */
	public function getKey()
	{
		return $this->key;
	}
	
	/**
	 * Get the value being indexed
	 *
	 * @return mixed
	 */
	public function getValue()
	{
		return $this->value;
	}

	/**
	 * Based on this operations parameters, generate a consistent id
	 *
	 * @return mixed
	 */
	public function matchId()
	{
		return parent::matchId() . spl_object_hash($this->index) . $this->key . $this->value;
	}
}
