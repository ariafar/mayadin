<?php
//namespace Everyman\Neo4j\Batch;
//
//use Everyman\Neo4j\Batch,
//	Everyman\Neo4j\Command\Batch as Command,
//	Everyman\Neo4j\Index,
//	Everyman\Neo4j\PropertyContainer;

/**
 * A remove-from-index operation
 */
class Tea_Neo4j_Batch_RemoveFrom extends Tea_Neo4j_Batch_Operation
{
	protected $command = null;
	protected $index = null;
	protected $key = null;
	protected $value = null;

	/**
	 * Build the operation
	 *
	 * @param Tea_Neo4j_Batch $batch
	 * @param Tea_Neo4j_Index $index
	 * @param Tea_Neo4j_PropertyContainer $entity
	 * @param string $key
	 * @param string $value
 * @param integer $opId
	 */
	public function __construct(Tea_Neo4j_Batch $batch, Tea_Neo4j_Index $index, Tea_Neo4j_PropertyContainer $entity, $key=null, $value=null, $opId)
	{
		parent::__construct($batch, 'removefrom', $entity, $opId);
		$this->index = $index;
		$this->key = $key;
		$this->value = $value;
	}

	/**
	 * Get the command that represents this operation
	 *
	 * @return Batch\Command
	 */
	public function getCommand()
	{
		if (!$this->command) {
			$this->command = new Tea_Neo4j_Command_Batch_RemoveFromIndex($this->batch->getClient(),
				$this->index, $this->entity, $this->key, $this->value, $this->opId);
		}
		return $this->command;
	}
	
	/**
	 * Based on this operations parameters, generate a consistent id
	 *
	 * @return mixed
	 */
	public function matchId()
	{
		return parent::matchId() . spl_object_hash($this->index) . $this->key . $this->value;
	}
}
