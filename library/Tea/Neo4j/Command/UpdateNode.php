<?php
//namespace Everyman\Neo4j\Command;
//use Everyman\Neo4j\Command,
//	Everyman\Neo4j\Client,
//	Everyman\Neo4j\Exception,
//	Everyman\Neo4j\Node;

/**
 * Update a node
 */
class Tea_Neo4j_Command_UpdateNode extends Tea_Neo4j_Command
{
	protected $node = null;

	/**
	 * Set the node to drive the command
	 *
	 * @param Tea_Neo4j_Client $client
	 * @param Tea_Neo4j_Node $node
	 */
	public function __construct(Tea_Neo4j_Client $client, Tea_Neo4j_Node $node)
	{
		parent::__construct($client);
		$this->node = $node;
	}

	/**
	 * Return the data to pass
	 *
	 * @return mixed
	 */
	protected function getData()
	{
		return $this->node->getProperties();
	}

	/**
	 * Return the transport method to call
	 *
	 * @return string
	 */
	protected function getMethod()
	{
		return 'put';
	}

	/**
	 * Return the path to use
	 *
	 * @return string
	 */
	protected function getPath()
	{
		if (!$this->node->hasId()) {
			throw new Tea_Neo4j_Exception('No node id specified');
		}
		return '/node/'.$this->node->getId().'/properties';
	}

	/**
	 * Use the results
	 *
	 * @param integer $code
	 * @param array   $headers
	 * @param array   $data
	 * @return boolean true on success
	 * @throws Tea_Neo4j_Exception on failure
	 */
	protected function handleResult($code, $headers, $data)
	{
		if ((int)($code / 100) == 2) {
			$this->getEntityCache()->setCachedEntity($this->node);
			return true;
		} else {
			$this->throwException('Unable to update node', $code, $headers, $data);
		}
	}
}

