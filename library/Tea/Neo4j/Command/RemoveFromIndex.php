<?php
//namespace Everyman\Neo4j\Command;
//use Everyman\Neo4j\Command,
//	Everyman\Neo4j\Client,
//	Everyman\Neo4j\Exception,
//	Everyman\Neo4j\PropertyContainer,
//	Everyman\Neo4j\Relationship,
//	Everyman\Neo4j\Node,
//	Everyman\Neo4j\Index;

/**
 * Removes an entity from an index
 */
class Tea_Neo4j_Command_RemoveFromIndex extends Tea_Neo4j_Command
{
	protected $index = null;
	protected $entity = null;
	protected $key = null;
	protected $value = null;

	/**
	 * Remove an entity from an index
	 * If $value is not given, all reference of the entity for the key
	 * are removed.
	 * If $key is not given, all reference of the entity are removed.
	 *
	 * @param Tea_Neo4j_Client $client
	 * @param Tea_Neo4j_Index $index
	 * @param Tea_Neo4j_PropertyContainer $entity
	 * @param string $key
	 * @param string $value
	 * @return boolean
	 */
	public function __construct(Tea_Neo4j_Client $client, Tea_Neo4j_Index $index, Tea_Neo4j_PropertyContainer $entity, $key=null, $value=null)
	{
		parent::__construct($client);
		$this->index = $index;
		$this->entity = $entity;
		$this->key = $key;
		$this->value = $value;
	}

	/**
	 * Return the data to pass
	 *
	 * @return mixed
	 */
	protected function getData()
	{
		return null;
	}

	/**
	 * Return the transport method to call
	 *
	 * @return string
	 */
	protected function getMethod()
	{
		return 'delete';
	}

	/**
	 * Return the path to use
	 *
	 * @return string
	 */
	protected function getPath()
	{
		if (!$this->entity || !$this->entity->hasId()) {
			throw new Tea_Neo4j_Exception('No entity to index specified');
		}

		$type = trim((string)$this->index->getType());
		if ($type != Tea_Neo4j_Index::TypeNode && $type != Tea_Neo4j_Index::TypeRelationship) {
			throw new Tea_Neo4j_Exception('No type specified for index');
		} else if ($type == Tea_Neo4j_Index::TypeNode && !($this->entity instanceof Tea_Neo4j_Node)) {
			throw new Tea_Neo4j_Exception('Cannot remove a node from a non-node index');
		} else if ($type == Tea_Neo4j_Index::TypeRelationship && !($this->entity instanceof Tea_Neo4j_Relationship)) {
			throw new Tea_Neo4j_Exception('Cannot remove a relationship from a non-relationship index');
		}

		$name = trim((string)$this->index->getName());
		if (!$name) {
			throw new Tea_Neo4j_Exception('No name specified for index');
		}

		$name = rawurlencode($name);
		$key = trim((string)$this->key);
		$value = trim((string)$this->value);

		$uri = '/index/'.$type.'/'.$name.'/';
		if ($key) {
			$uri .= rawurlencode($key).'/';
			if ($value) {
				$uri .= rawurlencode($value).'/';
			}
		}
		$uri .= $this->entity->getId();

		return $uri;
	}

	/**
	 * Use the results
	 *
	 * @param integer $code
	 * @param array   $headers
	 * @param array   $data
	 * @return integer on failure
	 */
	protected function handleResult($code, $headers, $data)
	{
		if ((int)($code / 100) != 2) {
			$this->throwException('Unable to remove entity from index', $code, $headers, $data);
		}
		return true;
	}
}

