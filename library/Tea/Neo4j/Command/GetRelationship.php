<?php
//namespace Everyman\Neo4j\Command;
//use Everyman\Neo4j\Command,
//	Everyman\Neo4j\Client,
//	Everyman\Neo4j\Exception,
//	Everyman\Neo4j\Relationship,
//	Everyman\Neo4j\Node;

/**
 * Get and populate a relationship
 */
class Tea_Neo4j_Command_GetRelationship extends Tea_Neo4j_Command
{
	protected $rel = null;

	/**
	 * Set the relationship to drive the command
	 *
	 * @param Tea_Neo4j_Client $client
	 * @param Tea_Neo4j_Relationship $rel
	 */
	public function __construct(Tea_Neo4j_Client $client, Tea_Neo4j_Relationship $rel)
	{
		parent::__construct($client);
		$this->rel = $rel;
	}

	/**
	 * Return the data to pass
	 *
	 * @return mixed
	 */
	protected function getData()
	{
		return null;
	}

	/**
	 * Return the transport method to call
	 *
	 * @return string
	 */
	protected function getMethod()
	{
		return 'get';
	}

	/**
	 * Return the path to use
	 *
	 * @return string
	 */
	protected function getPath()
	{
		if (!$this->rel->hasId()) {
			throw new Tea_Neo4j_Exception('No relationship id specified');
		}
		return '/relationship/'.$this->rel->getId();
	}

	/**
	 * Use the results
	 *
	 * @param integer $code
	 * @param array   $headers
	 * @param array   $data
	 * @return boolean true on success
	 * @throws Tea_Neo4j_Exception on failure
	 */
	protected function handleResult($code, $headers, $data)
	{
		if ((int)($code / 100) == 2) {
			$this->rel = $this->getEntityMapper()->populateRelationship($this->rel, $data);
			$this->getEntityCache()->setCachedEntity($this->rel);
			return true;
		} else {
			$this->throwException('Unable to retrieve relationship', $code, $headers, $data);
		}
	}
}

