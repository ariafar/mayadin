<?php
//namespace Everyman\Neo4j\Command\Batch;
//use Everyman\Neo4j\Client,
//	Everyman\Neo4j\Relationship,
//	Everyman\Neo4j\Command\UpdateRelationship as SingleUpdateRelationship;

/**
 * Update a relationship in a batch
 */
class Tea_Neo4j_Command_Batch_UpdateRelationship extends Tea_Neo4j_Command_Batch_Command
{
	/**
	 * Set the operation to drive the command
	 *
	 * @param Tea_Neo4j_Client $client
	 * @param Tea_Neo4j_Relationship $rel
	 * @param integer $opId
	 */
	public function __construct(Tea_Neo4j_Client $client, Tea_Neo4j_Relationship $rel, $opId)
	{
		parent::__construct($client, new Tea_Neo4j_Command_UpdateRelationship($client, $rel), $opId);
	}

	/**
	 * Return the data to pass
	 *
	 * @return array
	 */
	protected function getData()
	{
		$opData = array(array(
			'method' => strtoupper($this->base->getMethod()),
			'to' => $this->base->getPath(),
			'body' => $this->base->getData(),
			'id' => $this->opId,
		));
		return $opData;
	}
}

