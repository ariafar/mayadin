<?php
//namespace Everyman\Neo4j\Command\Batch;
//use Everyman\Neo4j\Client,
//	Everyman\Neo4j\Index,
//	Everyman\Neo4j\Batch,
//	Everyman\Neo4j\PropertyContainer,
//	Everyman\Neo4j\Command\RemoveFromIndex as SingleRemoveFromIndex;

/**
 * Remove the given entity from the index
 */
class Tea_Neo4j_Command_Batch_RemoveFromIndex extends Tea_Neo4j_Command_Batch_Command
{
	/**
	 * Set the operation to drive the command
	 *
	 * @param Tea_Neo4j_Client $client
	 * @param Tea_Neo4j_Index $index
	 * @param Tea_Neo4j_PropertyContainer $entity
	 * @param string $key
	 * @param string $value
	 * @param integer $opId
	 */
	public function __construct(Tea_Neo4j_Client $client, Index $index, Tea_Neo4j_PropertyContainer $entity, $key=null, $value=null, $opId)
	{
		parent::__construct($client, new Tea_Neo4j_Command_RemoveFromIndex($client, $index, $entity, $key, $value), $opId);
	}

	/**
	 * Return the data to pass
	 *
	 * @return array
	 */
	protected function getData()
	{
		$opData = array(array(
			'method' => strtoupper($this->base->getMethod()),
			'to' => $this->base->getPath(),
			'id' => $this->opId,
		));
		return $opData;
	}
}

