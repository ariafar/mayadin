<?php
//namespace Everyman\Neo4j;

/**
 * Represents a relationship between two nodes
 */
class Tea_Neo4j_Relationship extends Tea_Neo4j_PropertyContainer
{
	const DirectionAll       = 'all';
	const DirectionIn        = 'in';
	const DirectionOut       = 'out';

	protected $start = null;
	protected $end = null;
	protected $type = null;

	/**
	 * Delete this relationship
	 *
	 * @return Tea_Neo4j_PropertyContainer
	 * @throws Tea_Neo4j_Exception on failure
	 */
	public function delete()
	{
		$this->client->deleteRelationship($this);
		return $this;
	}

	/**
	 * Get the end node
	 *
	 * @return Tea_Neo4j_Node
	 */
	public function getEndNode()
	{
		$this->loadProperties();
		return $this->end;
	}

	/**
	 * Get the start node
	 *
	 * @return Tea_Neo4j_Node
	 */
	public function getStartNode()
	{
		$this->loadProperties();
		return $this->start;
	}

	/**
	 * Get the relationship type
	 *
	 * @return string
	 */
	public function getType()
	{
		$this->loadProperties();
		return $this->type;
	}

	/**
	 * Load this relationship
	 *
	 * @return Tea_Neo4j_PropertyContainer
	 * @throws Tea_Neo4j_Exception on failure
	 */
	public function load()
	{
		$this->client->loadRelationship($this);
		return $this;
	}

	/**
	 * Save this node
	 *
	 * @return Tea_Neo4j_PropertyContainer
	 * @throws Tea_Neo4j_Exception on failure
	 */
	public function save()
	{
		$this->client->saveRelationship($this);
		return $this;
	}

	/**
	 * Set the end node
	 *
	 * @param Tea_Neo4j_Node $end
	 * @return Tea_Neo4j_Relationship
	 */
	public function setEndNode(Tea_Neo4j_Node $end)
	{
		$this->end = $end;
		return $this;
	}

	/**
	 * Set the start node
	 *
	 * @param Tea_Neo4j_Node $start
	 * @return Relationship
	 */
	public function setStartNode(Tea_Neo4j_Node $start)
	{
		$this->start = $start;
		return $this;
	}

	/**
	 * Set the type
	 *
	 * @param string $type
	 * @return Tea_Neo4j_Relationship
	 */
	public function setType($type)
	{
		$this->type = $type;
		return $this;
	}
}
