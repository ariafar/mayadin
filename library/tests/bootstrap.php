<?php

//program constants
defined('DS') || define('DS', DIRECTORY_SEPARATOR);
defined('PS') || define('PS', PATH_SEPARATOR);


// Define path to application directory
defined('APPLICATION_PATH')
        || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../application'));

// Define application environment
defined('APPLICATION_ENV')
        || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'testing'));

// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
            realpath(APPLICATION_PATH . '/../library'),
            get_include_path(),
        )));

defined('TEST_USER_EMAIL')
        || define('TEST_USER_EMAIL', 'test@application.com');
defined('TEST_USER_PASS')
        || define('TEST_USER_PASS', 'test');
require_once 'PHPUnit/Autoload.php';
require_once 'Zend/Loader/Autoloader.php';
$loader = Zend_Loader_Autoloader::getInstance();
$loader->registerNamespace('Zend_');
$loader->registerNamespace('Tea_');
$loader->registerNamespace('PHPUnit_');

abstract class BaseTestCase extends Zend_Test_PHPUnit_ControllerTestCase
{

    public function setUp()
    {
        $this->appConfig = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', APPLICATION_ENV);
        $this->bootstrap = new Zend_Application(APPLICATION_ENV, APPLICATION_PATH . '/configs/application.ini');
        Zend_Registry::set('bootstrap', $this->bootstrap->getBootstrap());
        parent::setUp();
        return;
    }

    public function tearDown()
    {

    }

    public function tearUp()
    {
    }
}
