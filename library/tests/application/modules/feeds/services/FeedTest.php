<?php

class Feeds_Service_FeedTest extends Tea_Test_ControllerTestCase
{
    public function setUp()
    {
        parent::setUp();
        $this->cleanupTable('users');
        $this->cleanupTable('activities');
        $this->cleanupTable('activity_comments');
        $this->cleanupTable('activity_comment_likes');
        $this->cleanupTable('feeds');
    }

    public function tearDown()
    {
    }

    public function testGetInstance()
    {
        $instance = Feeds_Service_Feed::getInstance();
        $this->assertInstanceOf('Feeds_Service_Feed', $instance);
    }

    public function testCreateFeed()
    {
        /*
         * feed is created
         * 
         * feed retrieves by id, 
         * and it's properties compare with original feed
         */
        
        $feedService = Feeds_Service_Feed::getInstance();
        
        $user       = $this->createTestUser();
        $activity   = $this->createTestActivity($user);
        
        $now = Tea_Model_Entity::getGmtTimestamp('now');
        
        $feed = new Feeds_Model_Feed();
        $feed->setUserId($user->getId());
        $feed->setActivityId($activity->getId());
        $feed->setCreatedById($user->getId());
        $feed->setUpdatedById($user->getId());
        $feed->setNew(true);
        $feed = $feedService->save($feed);
        
        $testFeed = $feedService->getByPK($feed->getId());
        $this->assertEquals($testFeed->getUserId(), $user->getId());
        $this->assertEquals($testFeed->getActivityId(), $activity->getId());
        $this->assertEquals($testFeed->getDeleted(), 0);
    }    
    
    public function testUpdateFeed()
    {
        /*
         * feed is created
         * update_date of this feed, is updated
         * 
         * feed retrieves by id, 
         * and it's properties compare with modified feed
         */
        
        $feedService = Feeds_Service_Feed::getInstance();
        
        $user       = $this->createTestUser();
        $activity   = $this->createTestActivity($user);
        $feed       = $this->createTestFeed($user, $activity);
        
        sleep(1);
        $now = Tea_Model_Entity::getGmtTimestamp('now');
        
        $feed = $feedService->getByPK($feed->getId());
        $feed->setUpdatedById($user->getId());
        $feed->setUpdateDate($now);
        $feed->setNew(false);
        $feed = $feedService->save($feed);
        
        $testFeed = $feedService->getByPK($feed->getId());
        $this->assertEquals($testFeed->getUpdateDate(), $now);
        $this->assertEquals($testFeed->getUserId(), $user->getId());
        $this->assertEquals($testFeed->getActivityId(), $activity->getId());
        $this->assertEquals($testFeed->getDeleted(), 0);
    }

    public function testDeleteFeed()
    {
        /*
         * feed is created
         * feed is deleted
         * 
         * feed retrieves by id, 
         * and result of this search should be null
         */

        $feedService = Feeds_Service_Feed::getInstance();
        
        $user       = $this->createTestUser();
        $activity   = $this->createTestActivity($user);
        $feed       = $this->createTestFeed($user, $activity);
        
        $feedService->remove($feed);
        $testFeed = $feedService->getByPK($feed->getId());
        $this->assertEquals($testFeed, null);
    }
}
