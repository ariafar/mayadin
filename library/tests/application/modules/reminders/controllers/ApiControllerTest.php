<?php

class Reminders_ApiControllerTest extends Tea_Test_ControllerTestCase
{

    public function setUp()
    {
        parent::setUp();
    }

    public function testIndexAction()
    {
        $this->cleanupTable('likes');
        $this->cleanupTable('comments');
        $this->cleanupTable('follows');
        $this->cleanupTable('shares');
        $this->cleanupTable('votes');
        $this->cleanupTable('posts');
        $this->cleanupTable('users');

        $user = $this->createTestUser();

        $this->dispatch('/api/reminders/');
        $this->assertResponseCode(200);
        $this->assertModule('reminders');
        $this->assertController('Api');
        $this->assertAction('index');
    }

    public function testPostAction()
    {
        $this->cleanupTable('reminders');

        $this->resetRequest();
        $this->resetResponse();
        $this->request->setMethod('POST')
            ->setPost(
                array(
                    'email'    => 'test@gando.com',
                    'password' => 'test'
                )
        );
        $this->request->setHeader('Accept', 'application/json');
        $this->dispatch('/users/auth/login');
        $this->assertResponseCode(200);

        $model = new Tea_Model_Entity();
        $now   = $model->getDateTime('now');

        $this->resetRequest();
        $this->resetResponse();
        $this->request->setMethod('POST')
            ->setPost(
                array(
                    'user_id'       => Users_Service_Auth::getInstance()->getCurrentUser()->getId(),
                    'type'          => 'EMAIL',
                    'title'         => 'testing',
                    'description'   => 'description',
                    'start_date'    => $now,
                    'type'          => 'EMAIL',
                    'status'        => 'SENT',
                    'ref_type'      => 1,
                    'ref_id'        => 1,
                    'deleted'       => 1,
                    'created_by_id' => Users_Service_Auth::getInstance()->getCurrentUser()->getId(),
                    'creation_date' => $now,
                    'updated_by_id' => Users_Service_Auth::getInstance()->getCurrentUser()->getId(),
                    'update_date'   => $now,
                )
        );
        $this->request->setHeader('Accept', 'application/json');
        $this->dispatch('api/reminders');

        $this->assertResponseCode(200);
        $result        = $this->response->getBody();
        $reminderArray = json_decode($result, true);
        $this->assertEquals($reminderArray['user_id'], Users_Service_Auth::getInstance()->getCurrentUser()->getId());
        $this->assertEquals($reminderArray['type'], 'EMAIL');
        $this->assertEquals($reminderArray['title'], 'testing');

        $this->resetRequest();
        $this->resetResponse();
        $this->dispatch('/api/reminders');
        $this->assertResponseCode(200);
        $this->assertModule('reminders');
        $this->assertController('Api');
        $this->assertAction('index');
        $body   = $this->response->getBody();
        $this->assertNotEmpty($body);
        $result = json_decode($this->response->getBody(), true);
        $this->assertEquals($result['count'], 1);

        return $reminderArray['id'];
    }

    /**
     * @depends testPostAction
     */
    public function testGetAction($id)
    {
        $this->resetRequest();
        $this->resetResponse();
        $this->request->setMethod('Get');
        $this->dispatch('/api/reminders/' . $id);
        $this->assertResponseCode(200);
        $this->assertModule('reminders');
        $this->assertController('Api');
        $this->assertAction('get');
        $body   = $this->response->getBody();
        $this->assertNotEmpty($body);
        $result = json_decode($body, true);
        $this->assertEquals($result['id'], $id);
        return $id;
    }

    /**
     * @depends testGetAction
     */
    public function testPutAction($id)
    {
        $this->resetRequest();
        $this->resetResponse();
        $this->request->setMethod('POST')
            ->setPost(
                array(
                    'email'    => TEST_USER_EMAIL,
                    'password' => TEST_USER_PASS
                )
        );
        $this->request->setHeader('Accept', 'application/json');
        $this->dispatch('/users/auth/login');
        $this->assertResponseCode(200);

        $this->request->setMethod('PUT')
            ->setPost(
                array(
                    'title' => 'modified title',
                )
        );
        $this->request->setHeader('Accept', 'application/json');
        $this->dispatch('api/reminders/' . $id);
        $this->assertResponseCode(200);

        $this->resetRequest();
        $this->resetResponse();
        $this->request->setMethod('GET');
        $this->dispatch('/api/reminders/' . $id);
        $this->assertResponseCode(200);
        $this->assertModule('reminders');
        $this->assertController('Api');
        $this->assertAction('get');
        $body     = $this->response->getBody();
        $this->assertNotEmpty($body);
        $reminder = json_decode($body, true);
        $this->assertEquals($reminder['title'], 'modified title');
        return $id;
    }

    /**
     * @depends testPutAction
     */
    public function testDeleteAction($id)
    {
        $this->resetRequest();
        $this->resetResponse();
        $this->request->setMethod('POST')
            ->setPost(
                array(
                    'email'    => TEST_USER_EMAIL,
                    'password' => TEST_USER_PASS
                )
        );
        $this->request->setHeader('Accept', 'application/json');
        $this->dispatch('/users/auth/login');
        $this->assertResponseCode(200);

        $this->resetRequest();
        $this->resetResponse();
        $this->request->setMethod('DELETE');
        $this->dispatch('/api/reminders/' . $id);
        $this->assertResponseCode(200);
        $this->assertModule('reminders');
        $this->assertController('Api');
        $this->assertAction('delete');

        $body = $this->response->getBody();
        $this->assertNotEmpty($body);
        $this->assertEquals($body, 'Success');

        $this->resetRequest();
        $this->resetResponse();
        $this->request->setMethod('GET');
        $this->dispatch('/api/reminders/' . $id);
        $this->assertResponseCode(404);
        $this->assertModule('reminders');
        $this->assertController('Api');
        $this->assertAction('get');

        $body = $this->response->getBody();
        $this->assertNotEmpty($body);
        $this->assertEquals($body, 'Post Not Found');
    }

}

?>
