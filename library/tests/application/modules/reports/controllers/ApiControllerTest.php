<?php

class Reports_ApiControllerTest extends Tea_Test_ControllerTestCase
{

    public function setUp()
    {
        parent::setUp();
    }

    public function testIndexAction()
    {
        $this->dispatch('/api/reports/');
        $this->assertResponseCode(200);
        $this->assertModule('reports');
        $this->assertController('Api');
        $this->assertAction('index');
    }

    public function testPostAction()
    {
        $reportSrv = Reports_Service_Report::getInstance();
        $reportSrv->removeAll();
        $this->resetRequest();
        $this->resetResponse();
        $this->request->setMethod('POST')
            ->setPost(
                array(
                    'email'    => 'test@gando.dev',
                    'password' => 'test'
                )
        );
        $this->request->setHeader('Accept', 'application/json');
        $this->dispatch('/users/auth/login');
        $this->assertResponseCode(200);

        $model = new Tea_Model_Entity();
        $now   = $model->getDateTime('now');

        $this->resetRequest();
        $this->resetResponse();
        $this->request->setMethod('POST')
            ->setPost(
                array(
                    'reporter_id'   => Users_Service_Auth::getInstance()->getCurrentUser()->getId(),
                    'reported_id'   => Users_Service_Auth::getInstance()->getCurrentUser()->getId(),
                    'report_type'   => 'spam',
                    'reported_type' => 'user',
                    'reported_url'  => 'http://testing.com',
                    'created_by_id' => Users_Service_Auth::getInstance()->getCurrentUser()->getId(),
                    'creation_date' => $now,
                    'updated_by_id' => Users_Service_Auth::getInstance()->getCurrentUser()->getId(),
                    'update_date'   => $now,
                )
        );
        $this->request->setHeader('Accept', 'application/json');
        $this->dispatch('api/reports');
        $this->assertResponseCode(200);
        $result         = $this->response->getBody();
        $report         = json_decode($result, true);
        $this->assertEquals($report['reporter_id'], Users_Service_Auth::getInstance()->getCurrentUser()->getId());
        $this->assertEquals($report['report_type'], 'SPAM');
        $this->assertEquals($report['reported_url'], 'http://testing.com');


        $this->resetRequest();
        $this->resetResponse();
        $this->dispatch('/api/reports');
        $this->assertResponseCode(200);
        $this->assertModule('reports');
        $this->assertController('Api');
        $this->assertAction('index');
        $body   = $this->response->getBody();
        $this->assertNotEmpty($body);
        $result = json_decode($this->response->getBody(), true);
        $this->assertEquals($result['count'], 1);

        return $report['id'];
    }

    /**
     * @depends testPostAction
     */
    public function testGetAction($id)
    {
        $this->resetRequest();
        $this->resetResponse();
        $this->request->setMethod('Get');
        $this->dispatch('/api/reports/' . $id);
        $this->assertResponseCode(200);
        $this->assertModule('reports');
        $this->assertController('Api');
        $this->assertAction('get');
        $body   = $this->response->getBody();
        $this->assertNotEmpty($body);
        $result = json_decode($body, true);
        $this->assertEquals($result['id'], $id);
        return $id;
    }

    /**
     * @depends testGetAction
     */
    public function testPutAction($id)
    {
        $this->resetRequest();
        $this->resetResponse();
        $this->request->setMethod('POST')
            ->setPost(
                array(
                    'email'    => 'test@gando.dev',
                    'password' => TEST_USER_PASS
                )
        );
        $this->request->setHeader('Accept', 'application/json');
        $this->dispatch('/users/auth/login');
        $this->assertResponseCode(200);

        $this->request->setMethod('PUT')
            ->setPost(
                array(
                    'status' => 'CHECKED',
                )
        );
        $this->request->setHeader('Accept', 'application/json');
        $this->dispatch('api/reports/' . $id);
        $this->assertResponseCode(200);

        $this->resetRequest();
        $this->resetResponse();
        $this->request->setMethod('GET');
        $this->dispatch('/api/reports/' . $id);
        $this->assertResponseCode(200);
        $this->assertModule('reports');
        $this->assertController('Api');
        $this->assertAction('get');
        $body   = $this->response->getBody();
        $this->assertNotEmpty($body);
        $report = json_decode($body, true);
        $this->assertEquals($report['status'], 'CHECKED');
        return $id;
    }

    /**
     * @depends testPutAction
     */
    public function testDeleteAction($id)
    {
        $this->resetRequest();
        $this->resetResponse();
        $this->request->setMethod('POST')
            ->setPost(
                array(
                    'email'    => 'test@gando.dev',
                    'password' => TEST_USER_PASS
                )
        );
        $this->request->setHeader('Accept', 'application/json');
        $this->dispatch('/users/auth/login');
        $this->assertResponseCode(200);

        $this->resetRequest();
        $this->resetResponse();
        $this->request->setMethod('DELETE');
        $this->dispatch('/api/reports/' . $id);
        $this->assertResponseCode(200);
        $this->assertModule('reports');
        $this->assertController('Api');
        $this->assertAction('delete');

        $body = $this->response->getBody();
        $this->assertNotEmpty($body);
        $this->assertEquals($body, 'Success');

        $this->resetRequest();
        $this->resetResponse();
        $this->request->setMethod('GET');
        $this->dispatch('/api/reports/' . $id);
        $this->assertResponseCode(404);
        $this->assertModule('reports');
        $this->assertController('Api');
        $this->assertAction('get');

        $body = $this->response->getBody();
        $this->assertNotEmpty($body);
        $this->assertEquals($body, 'Report Not Found');
    }

}

?>
