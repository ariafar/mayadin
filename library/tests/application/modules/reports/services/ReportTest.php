<?php

class Reports_Service_ReportTest extends Tea_Test_ControllerTestCase
{

    public function setUp()
    {
        parent::setUp();
    }

    public function testGetInstance()
    {
        $instance = Reports_Service_Report::getInstance();
        $this->assertInstanceOf('Reports_Service_Report', $instance);
    }

    public function testCreate()
    {
        $reportSrv = Reports_Service_Report::getInstance();

        $report     = new Reports_Model_Report();
        $user       = $this->createTestUser();
        $report->setReporterId($user->getId());
        $report->setReportedId($user->getId());
        $report->setReportType('spam');
        $report->setNew(true);
        $report->setCreatedById($user->getId());
        $report->setUpdatedById($user->getId());
        $report     = $reportSrv->save($report);
        $testReport = $reportSrv->getByPK($report->getId());
        $this->assertEquals($report, $testReport);
        $this->assertEquals($testReport->getReportType(), 'SPAM');
    }

    public function testUpdate()
    {
        $reportSrv = Reports_Service_Report::getInstance();

        $report = new Reports_Model_Report();
        $user   = $this->createTestUser();
        $report->setReporterId($user->getId());
        $report->setReportedId($user->getId());
        $report->setReportType('spam');
        $report->setNew(true);
        $report->setCreatedById($user->getId());
        $report->setUpdatedById($user->getId());
        $report = $reportSrv->save($report);
        $report->setReportType('insult');
        $report->setNew(FALSE);
        $report = $reportSrv->save($report);
        $this->assertEquals($report->getReportType(), 'INSULT');
    }

    public function testDelete()
    {

        $reportService = Reports_Service_Report::getInstance();

        $report   = new Reports_Model_Report();
        $user     = $this->createTestUser();
        $report->setReporterId($user->getId());
        $report->setReportedId($user->getId());
        $report->setReportType('spam');
        $report->setNew(true);
        $report->setCreatedById($user->getId());
        $report->setUpdatedById($user->getId());
        $report   = $reportService->save($report);
        $reportId = $report->getId();
        $reportService->remove($report);
        $report   = $reportService->getByPK($reportId);
        $this->assertEquals($report, null);
    }

}

?>
