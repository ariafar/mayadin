<?php

class Sites_Service_InviteTest extends Tea_Test_ControllerTestCase
{

    public function setUp()
    {
        parent::setUp();
        Sites_Service_Invite::getInstance()->removeAll();
    }

    public function testGetInstance()
    {
        $instance = Sites_Service_Invite::getInstance();
        $this->assertInstanceOf('Sites_Service_Invite', $instance);
    }

    public function testCreate()
    {
        $user          = $this->createTestUser();
        $siteService   = Sites_Service_Site::getInstance();
        $site          = new Sites_Model_Site;
        $site->setTitle('testing');
        $site->setStatus('NOTPUBLISHED');
        $site->setDescription('testing');
        $site->setCreatedById($user->getId());
        $site          = $siteService->save($site);
        $invite        = new Sites_Model_Invite();
        $inviteService = Sites_Service_Invite::getInstance();
        $invite->setSiteId($site->getId());
        $invite->setUserId($user->getId());
        $invite->setType(2);
        $invite->setEmail('test@test.com');
        $invite        = $inviteService->save($invite);
        $this->assertEquals($invite->getSiteId(), $site->getId());
        $this->assertEquals($invite->getUserId(), $user->getId());
        $this->assertEquals($invite->getType(), 'poweruser');
        $this->assertEquals($invite->getEmail(), 'test@test.com');
    }

    
}

?>
