<?php

class VotingTest extends Tea_Test_ControllerTestCase
{

    public function setUp()
    {
        parent::setUp();
        $this->cleanupTable('likes');
        $this->cleanupTable('comments');
        $this->cleanupTable('follows');
        $this->cleanupTable('shares');
        $this->cleanupTable('votes');
        $this->cleanupTable('posts');
        $this->cleanupTable('poll_votes');
        $this->cleanupTable('poll_voting_items');
        $this->cleanupTable('poll_assignees');
        $this->cleanupTable('poll_voting_responses');
        $this->cleanupTable('polls');
        $this->cleanupTable('users');
    }

    public function testGetInstance()
    {
        $instance = Polls_Service_Voting::getInstance();
        $this->assertInstanceOf('Polls_Service_Voting', $instance);
    }

    public function testCreateVote()
    {
        $voteService = Polls_Service_Voting::getInstance();
        $vote        = new Polls_Model_Voting();
        $user        = $this->createTestUser();
        $vote->setQuestion('testing');
        $vote->setResponseCount(1);
        $vote->setWeight(0.5);
        $vote->setCreatedById($user->getId());
        $vote->setUpdatedById($user->getId());

        $pollService = Polls_Service_Poll::getInstance();
        $poll        = new Polls_Model_Poll();
        $poll->setTitle('test title');
        $poll->setDescription('test description');
        $poll->setStatus(2);
        $poll->setPrivacy(1);
        $poll->setNew(true);
        $poll->setCreatedById($user->getId());
        $poll->setUpdatedById($user->getId());
        $poll        = $pollService->save($poll);

        $vote->setPollId($poll->getId());
        $vote     = $voteService->save($vote);
        $testpoll = $voteService->getByPK($vote->getId());
        $this->assertEquals($vote, $testpoll);
    }
    

}

?>
