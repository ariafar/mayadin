<?php

class Posts_Service_ShareTest extends Tea_Test_ControllerTestCase
{

    public function setUp()
    {
        parent::setUp();
        $this->cleanupTable('likes');
        $this->cleanupTable('comments');
        $this->cleanupTable('follows');
        $this->cleanupTable('shares');
        $this->cleanupTable('votes');
        $this->cleanupTable('posts');
        $this->cleanupTable('users');
    }

    public function testGetInstance()
    {
        $instance = Posts_Service_Share::getInstance();
        $this->assertInstanceOf('Posts_Service_Share', $instance);
    }

    public function testCreateShare()
    {
        $shareService = Posts_Service_Share::getInstance();
        $share        = new Posts_Model_Share();
        $user         = $this->createTestUser();
        $post         = $this->createTestPost($user);
        $share->setPostId($post->getId());
        $share->setSharedById($user->getId());
        $share->setNew(true);
        $share        = $shareService->save($share);
        $testShare    = $shareService->getByPK($share->getPostId(), $share->getSharedById());
        $this->assertEquals($share, $testShare);
        $this->assertEquals($testShare->getPostId(), $post->getId());
        $this->assertEquals($testShare->getSharedById(), $user->getId());
    }

    public function testDeleteShare()
    {
        $shareService = Posts_Service_Share::getInstance();
        $share        = new Posts_Model_Share();
        $user         = $this->createTestUser();
        $post         = $this->createTestPost($user);
        $share->setPostId($post->getId());
        $share->setSharedById($user->getId());
        $share->setNew(true);
        $share        = $shareService->save($share);
        $shareService->removeAllSharesForDeletedPost($post);
        $shareService->remove($share);
    }

    public function testUpdateShare()
    {
        $shareService = Posts_Service_Share::getInstance();
        $share        = new Posts_Model_Share();
        $user         = $this->createTestUser();
        $post         = $this->createTestPost($user);
        $share->setPostId($post->getId());
        $share->setSharedById($user->getId());
        $share->setNew(false);
        $share        = $shareService->save($share);
    }

}

?>
