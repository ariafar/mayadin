<?php

class Posts_Service_PostTest extends Tea_Test_ControllerTestCase
{

    public function setUp()
    {
        parent::setUp();
        $this->cleanupTable('likes');
        $this->cleanupTable('comments');
        $this->cleanupTable('follows');
        $this->cleanupTable('shares');
        $this->cleanupTable('votes');
        $this->cleanupTable('posts');
        $this->cleanupTable('users');
    }

    public function testGetInstance()
    {
        $instance = Posts_Service_Post::getInstance();
        $this->assertInstanceOf('Posts_Service_Post', $instance);
    }

    public function testCreatePost()
    {
        $postService = Posts_Service_Post::getInstance();

        $post     = new Posts_Model_Post();
        $user     = $this->createTestUser();
        $post->setPosterId($user->getId());
        $post->setTitle('test title');
        $post->setBody('test body');
        $post->setType(1);
        $post->setNew(true);
        $post->setCreatedById($user->getId());
        $post->setUpdatedById($user->getId());
        $post     = $postService->save($post);
        $testPost = $postService->getByPK($post->getId());
        $this->assertEquals($post, $testPost);
    }

    public function testUpdatePost()
    {
        $postService = Posts_Service_Post::getInstance();

        $post = new Posts_Model_Post();
        $user = $this->createTestUser();
        $post->setPosterId($user->getId());
        $post->setTitle('test title');
        $post->setBody('test body');
        $post->setType(1);
        $post->setNew(true);
        $post->setCreatedById($user->getId());
        $post->setUpdatedById($user->getId());
        $post = $postService->save($post);
        $post->setNew(FALSE);
        $post->setType(2);
        $post->setTitle('test title modified');
        $post->setBody('test body modified');
        $post = $postService->save($post);

        $this->assertEquals($post->getType(), 2);
        $this->assertEquals($post->getTitle(), 'test title modified');
        $this->assertEquals($post->getBody(), 'test body modified');
    }

    public function testDeletePost()
    {

        $postService = Posts_Service_Post::getInstance();

        $user   = $this->createTestUser();
        $post   = $this->createTestPost($user);
        $postId = $post->getId();
        $postService->remove($post);
        $post   = $postService->getByPK($postId);
        $this->assertEquals($post, null);
    }

}

?>
