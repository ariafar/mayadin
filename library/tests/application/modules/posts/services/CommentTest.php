
<?php

class Posts_Service_CommentTest extends Tea_Test_ControllerTestCase
{

    public function setUp()
    {
        parent::setUp();
        $this->cleanupTable('likes');
        $this->cleanupTable('comments');
        $this->cleanupTable('follows');
        $this->cleanupTable('shares');
        $this->cleanupTable('votes');
        $this->cleanupTable('posts');
        $this->cleanupTable('users');
    }

    public function testGetInstance()
    {
        $instance = Posts_Service_Comment::getInstance();
        $this->assertInstanceOf('Posts_Service_Comment', $instance);
    }

    public function testCreateComment()
    {
        $commentService = Posts_Service_Comment::getInstance();
        $comment        = new Posts_Model_Comment();
        $user           = $this->createTestUser();
        $post           = $this->createTestPost($user);
        $comment->setPostId($post->getId());
        $comment->setAuthorId($user->getId());
        $comment->setMessage('testing');
        $comment->setNew(true);
        $comment        = $commentService->save($comment);
        $testComment    = $commentService->getByPK($comment->getId());
        //$testComment->setMessage('testin2');
        $this->assertEquals($comment, $testComment);
        $this->assertEquals($testComment->getPostId(), $post->getId());
        $this->assertEquals($testComment->getAuthorId(), $user->getId());
        $this->assertEquals($testComment->getMessage(), 'testing');
    }

    public function testCommentDeletion()
    {
        $commentService = Posts_Service_Comment::getInstance();
        $user           = $this->createTestUser();
        $post           = $this->createTestPost($user);
        $comment        = $this->createTestComment($post, $user);
        $testId         = $comment->getId();
        $commentService->remove($comment);
        $testComment    = $commentService->getByPK($testId);
        $this->assertEquals($testComment, null);
    }

}

?>
