<?php

class Posts_Service_CommentLikeTest extends Tea_Test_ControllerTestCase
{

    public function setUp()
    {
        parent::setUp();
        $this->cleanupTable('likes');
        $this->cleanupTable('comments');
        $this->cleanupTable('follows');
        $this->cleanupTable('shares');
        $this->cleanupTable('votes');
        $this->cleanupTable('posts');
        $this->cleanupTable('users');
    }

    public function testGetInstance()
    {
        $instance = Posts_Service_CommentLike::getInstance();
        $this->assertInstanceOf('Posts_Service_CommentLike', $instance);
    }

    public function testCreateLike()
    {
        $likeService = Posts_Service_CommentLike::getInstance();
        $like        = new Posts_Model_CommentLike();
        $user        = $this->createTestUser();
        $post        = $this->createTestPost($user);
        $comment     = $this->createTestComment($post, $user);
        $like->setCommentId($comment->getId());
        $like->setLikerId($user->getId());
        $like->setNew(true);
        $like        = $likeService->save($like);
        $testLike    = $likeService->getByPK($like->getCommentId(), $like->getLikerId());
        $this->assertEquals($testLike->getCommentId(), $comment->getId());
        $this->assertEquals($testLike->getLikerId(), $user->getId());
    }

}

?>
