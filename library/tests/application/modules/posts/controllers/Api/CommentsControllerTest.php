<?php

class Posts_Api_CommentsControllerTest extends Tea_Test_ControllerTestCase
{

    public function setUp()
    {
        parent::setUp();
    }

    public function testIndexAction()
    {
        $this->cleanupTable('comments');
        $this->cleanupTable('votes');
        $this->cleanupTable('posts');
        $this->cleanupTable('users');



        $user = $this->createTestUser();
        $post = $this->createTestPost($user);

        $this->dispatch('/api/posts/' . $post->getId() . '/comments');
        $this->assertResponseCode(200);
        $this->assertModule('posts');
        $this->assertController('Api_comments');
        $this->assertAction('index');
        return $post->getId();
    }

    /**
     * @depends testIndexAction
     */
    public function testPostAction($postId)
    {
        $this->resetRequest();
        $this->resetResponse();
        $this->request->setMethod('POST')
            ->setPost(
                array(
                    'email'    => 'test@gando.com',
                    'password' => 'test'
                )
        );
        $this->request->setHeader('Accept', 'application/json');
        $this->dispatch('/users/auth/login');
        $this->assertResponseCode(200);



        $this->resetRequest();
        $this->resetResponse();
        $this->request->setMethod('POST')
            ->setPost(
                array(
                    'post_id'   => $postId,
                    'author_id' => Users_Service_Auth::getInstance()->getCurrentUser()->getId(),
                    'message'   => 'comment testing',
                )
        );
        $this->request->setHeader('Accept', 'application/json');
        $this->dispatch('api/posts/' . $postId . '/comments');
        $this->assertResponseCode(200);
        $result     = $this->response->getBody();
        $postArray  = json_decode($result, true);
        $this->assertEquals($postArray['post_id'], $postId);
        $this->assertEquals($postArray['author_id'], Users_Service_Auth::getInstance()->getCurrentUser()->getId());
        $this->assertEquals($postArray['message'], 'comment testing');

        $this->resetRequest();
        $this->resetResponse();
        $this->dispatch('api/posts/' . $postId . '/comments');
        $this->assertResponseCode(200);
        $this->assertModule('posts');
        $this->assertController('Api_comments');
        $this->assertAction('index');
        $body   = $this->response->getBody();
        $this->assertNotEmpty($body);
        $result = json_decode($this->response->getBody(), true);
        $this->assertEquals($result['count'], 1);

        return $postArray['id'];
    }

    /**
     * @depends testPostAction
     */
    public function testGetAction($id)
    {
        $this->resetRequest();
        $this->resetResponse();
        $this->request->setMethod('Get');
        $table   = new Posts_Model_DbTable_Comments;
        $comment = $table->select()->where('id =?', $id);
        $comment = $table->fetchRow($comment);
        $this->dispatch('/api/posts/' . $comment['post_id'] . '/comments/' . $id);
        $this->assertResponseCode(200);
        $this->assertModule('posts');
        $this->assertController('Api_comments');
        $this->assertAction('get');
        $body    = $this->response->getBody();
        $this->assertNotEmpty($body);
        $result  = json_decode($body, true);
        $this->assertEquals($result['id'], $id);
        return $id;
    }

    /**
     * @depends testGetAction
     */
    public function testDeleteAction($id)
    {
        $this->resetRequest();
        $this->resetResponse();
        $this->request->setMethod('POST')
            ->setPost(
                array(
                    'email'    => TEST_USER_EMAIL,
                    'password' => TEST_USER_PASS
                )
        );
        $this->request->setHeader('Accept', 'application/json');
        $this->dispatch('/users/auth/login');
        $this->assertResponseCode(200);


        $this->resetRequest();
        $this->resetResponse();
        $this->request->setMethod('DELETE');
        $table   = new Posts_Model_DbTable_Comments;
        $comment = $table->select()->where('id =?', $id);
        $comment = $table->fetchRow($comment);
        $this->dispatch('/api/posts/' . $comment['post_id'] . '/comments/' . $id);
        $this->assertResponseCode(200);
        $this->assertModule('posts');
        $this->assertController('Api_comments');
        $this->assertAction('delete');

        $body = $this->response->getBody();
        $this->assertNotEmpty($body);
        $this->assertEquals($body, 'Success');

        $this->resetRequest();
        $this->resetResponse();
        $this->request->setMethod('GET');
        $this->dispatch('/api/posts/' . $comment['post_id'] . '/comments/' . $id);
        $this->assertResponseCode(404);
        $this->assertModule('posts');
        $this->assertController('Api_comments');
        $this->assertAction('get');

        $body = $this->response->getBody();
        $this->assertNotEmpty($body);
        $this->assertEquals($body, 'Comment Not Found');
    }

}

?>
