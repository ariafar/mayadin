<?php

class Posts_Api_Comments_LikesControllerTest extends Tea_Test_ControllerTestCase
{

    public function setUp()
    {
        parent::setUp();
    }

    public function testIndexAction()
    {
        $this->cleanupTable('votes');
        $this->cleanupTable('likes');
        $this->cleanupTable('comments');
        $this->cleanupTable('posts');
        $this->cleanupTable('users');

        $this->resetRequest();
        $this->resetResponse();
        $user    = $this->createTestUser();
        $post    = $this->createTestPost($user);
        $comment = $this->createTestComment($post, $user);
        $like    = $this->createTestLike($comment, $user);
        $this->dispatch('/api/posts/' . $post->getId() . '/comments/' . $comment->getId() . '/likes');
        $this->assertResponseCode(200);
        $this->assertModule('posts');
        $this->assertController('Api_comments_likes');
        $this->assertAction('index');
        return array(
            'commentId' => $comment->getId(),
            'posterId'  => $user->getId(),
            'likeId'    => $like->getId(),
            'postId'    => $post->getId(),
        );
    }

    /**
     * @depends testIndexAction
     */
    public function testPostAction($data)
    {
        $this->resetRequest();
        $this->resetResponse();
        $this->request->setMethod('POST')
            ->setPost(
                array(
                    'email'    => 'test@gando.com',
                    'password' => 'test'
                )
        );
        $this->request->setHeader('Accept', 'application/json');
        $this->dispatch('/users/auth/login');
        $this->assertResponseCode(200);

        $this->resetRequest();
        $this->resetResponse();
        $this->request->setMethod('POST')
            ->setPost(
                array(
                    'comment_id' => $data['commentId'],
                    'liker_id'   => $data['posterId'],
                // 'post_id' => $data['postId'],
                )
        );

        $this->request->setHeader('Accept', 'application/json');
        $this->dispatch('api/posts/' . $data['postId'] . '/comments/' . $data['commentId'] . '/likes');
        $this->assertResponseCode(200);
        $result    = $this->response->getBody();
        $postArray = json_decode($result, true);
        $this->assertEquals($postArray['comment_id'], $data['commentId']);
        $this->assertEquals($postArray['liker_id'], $data['posterId']);
        $this->resetRequest();
        $this->resetResponse();
        $this->dispatch('api/posts/' . $data['postId'] . '/comments/' . $data['commentId'] . '/likes/');
        $this->assertResponseCode(200);
        $this->assertModule('posts');
        $this->assertController('Api_comments_likes');
        $this->assertAction('index');
        $body      = $this->response->getBody();
        $this->assertNotEmpty($body);
        $result    = json_decode($this->response->getBody(), true);
        $this->assertEquals($result['count'], 1);
        return $data;
    }

    /**
     * @depends testPostAction
     */
    public function testDeleteAction($data)
    {
        $this->resetRequest();
        $this->resetResponse();
        $this->request->setMethod('POST')
            ->setPost(
                array(
                    'email'    => 'test@gando.com',
                    'password' => 'test'
                )
        );
        $this->request->setHeader('Accept', 'application/json');
        $this->dispatch('/users/auth/login');
        $this->assertResponseCode(200);



        $this->resetRequest();
        $this->resetResponse();
        $this->request->setMethod('DELETE')
            ->setPost(
                array(
                    'comment_id' => $data['commentId'],
                    'liker_id'   => $data['posterId'],
                )
        );

        $this->request->setHeader('Accept', 'application/json');
        $this->dispatch('api/posts/' . $data['postId'] . '/comments/' . $data['commentId'] . '/likes');
        $this->assertResponseCode(200);


        $this->assertModule('posts');
        $this->assertController('Api_comments_likes');
        $this->assertAction('delete');

        $body = $this->response->getBody();
        $this->assertNotEmpty($body);
        $this->assertEquals($body, 'Unliked');
    }

}

?>
