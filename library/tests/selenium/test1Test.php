<?php

class ExampleTest extends PHPUnit_Extensions_Selenium2TestCase
{
    protected function setUp()
    {
        $this->setPort(4444);
        $this->setHost('localhost');
        $this->setBrowser('firefox');
        $this->setBrowserUrl("http://192.168.0.4/");
    }

    public function testMyTestCase()
    {
        $this->open("/");
        $this->assertEquals(null, $this->title());
    }
}
