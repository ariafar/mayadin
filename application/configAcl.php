<?php

$acl = new Zend_Acl();

$roles  = array('admin', 'user', 'guest');

foreach ($roles as $role) {
    $acl->addRole(new Zend_Acl_Role($role));
}

$resources = array (
);

foreach ($resources as $resource) {
    $acl->add(new Zend_Acl_Resource($resource));
}

// Here comes credential definiton for admin user.
$acl->allow('admin'); // Has access to everything.

// Here comes credential definition for normal user.
$acl->allow('user'); // Has access to everything...

$guestResources = array (
);

foreach ($guestResources as $res) {
    $acl->allow('guest', $res);
}

// Finally I store whole ACL definition to registry for use
// in AuthPlugin plugin.
$registry = Zend_Registry::getInstance();
$registry->set('acl', $acl);
