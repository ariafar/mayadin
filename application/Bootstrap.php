<?php

require_once dirname(__FILE__) . '/Bootstrap/Abstract.php';

class Bootstrap extends Bootstrap_Abstract
{

    protected function _initPlugins()
    {
        $this->_initAppAutoload();

        $this->bootstrap('FrontController');
        $front = $this->getResource('FrontController');
        //$front->registerPlugin(new Tea_Controller_Plugin_SetTimezone());
        $front->registerPlugin(new Tea_Controller_Plugin_Accept());
        $acl = new Tea_Acl();
        Zend_Registry::set('acl', $acl);
        $front->registerPlugin(new Tea_Controller_Plugin_Auth());
        Zend_Controller_Action_HelperBroker::addPrefix('Tea_Controller_Action_Helper');
    }

    protected function _initRoutes()
    {
        $front = Zend_Controller_Front::getInstance();
        $front->setDefaultModule('default');
        $front->setDefaultControllerName('index');
        $front->setDefaultAction('index');

        $hostnameRoute = new Tea_Controller_Router_Route_Subdomain(
                        'subdomain',
                        'domain',
                        array(
                            'module' => 'houses',
                            'controller' => 'preview',
                            'action' => 'index'
                        )
        );
        $plainPathRoute = new Zend_Controller_Router_Route_Static(
                        '/',
                        array(
                            'module' => 'houses',
                            'controller' => 'preview',
                            'action' => 'index'
                        )
        );
        $pageRoute = new Zend_Controller_Router_Route_Regex(
                        'page/(\d+)',
                        array(
                            'module' => 'houses',
                            'controller' => 'preview',
                            'action' => 'index'
                        ),
                        array(
                            1 => 'page_id'
                        )
        );

        $chain1 = new Zend_Controller_Router_Route_Chain();
        $chain1->chain($hostnameRoute);
        $chain1->chain($pageRoute);

        $chain2 = new Zend_Controller_Router_Route_Chain();
        $chain2->chain($hostnameRoute);
        $chain2->chain($plainPathRoute);

        //$front->getRouter()->addRoute('chain1', $chain1);
        //$front->getRouter()->addRoute('chain2', $chain2);

        $moduls = array(
            'news',
            'markets',
            'questionsAnswers',
            'educations',
            'users',
            'humans',
            'galleries',
            'evaluations',
            'resources',
            'reports',
            'stations',
            'memories',
            'links',
            'offices',
            'categories',
            'external',
            'products',
            'pricedetails',
            'priceheads',
            'activities'
        );

        $route = new Tea_Controller_Router_Route_Rest($front, array(), $moduls);
        $front->getRouter()->addRoute('api', $route);

        $route = new Tea_Controller_Router_Route_Admin($front);
        $front->getRouter()->addRoute('admin', $route);
    }

}
