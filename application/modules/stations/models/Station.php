<?php

class Stations_Model_Station extends Tea_Model_Entity
{

    protected $_properties = array(
        'id' => NULL,
        'area' => NULL,
        'name' => NULL,
        'address' => NULL,
        'firstTellNumber' => NULL,
        'secondTellNumber' => NULL,
        'latitude' => NULL,
        'longitude' => NULL,
        'creationDate' => NULL,
        'updateDate' => NULL,
        'deleted' => 0
    );

    public function __construct()
    {
        parent::__construct();

        $this->setCreationDate('now');
        $this->setUpdateDate('now');
    }

    public function fill($record)
    {
        foreach ($record as $key => $value) {
            switch ($key) {
                case 'id':
                case 'area':
                case 'name' :
                case 'address' :
                case 'firstTellNumber' :
                case 'secondTellNumber':
                case 'latitude':
                case 'longitude':
                case 'creationDate':
                case 'updateDate':
                case 'deleted':
                    $this->_properties[$key] = $value;
                    break;
            }
        }
    }

}