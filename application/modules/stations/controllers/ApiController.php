<?php

class Stations_ApiController extends Tea_Controller_Rest_Action
{

    public function init()
    {
        parent::init();
    }

    public function indexAction()
    {
        $updateDate = $this->getParam("updateDate");
        $deleted = $this->_getParam('deleted');
        $orderBy = $this->_getParam('order_by', 'updateDate');
        $orderDir = $this->_getParam('order_dir', 'desc');
        $sort = array($orderBy => $orderDir);
        $filter = array();

        isset($updateDate) && $filter["updateDate"] = $updateDate;
        isset($deleted) && $filter["deleted"] = $deleted;

        $stations_service = Stations_Service_Station::getInstance();
        $stations = $stations_service->getList($filter, $sort, $count);
        $list = array();
        foreach ($stations as $item) {
            $item = $item->toArray();
            $list[] = $item;
        }

        $response = array(
            'count' => $count,
            'list' => $list
        );
        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody(json_encode($response));
    }

    public function getAction()
    {
        $id = $this->_getParam('id');

        $service = Stations_Service_Station::getInstance();
        $station = $service->getByPK($id);

        if ($station)
            $result = $station->toArray();
        else
            $result = array();

        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody(json_encode($result));
    }

    public function postAction()
    {
        $params = $this->getParams();
        $authService = Users_Service_Auth::getInstance();

        $user = $authService->getCurrentUser();
//        if (!isset($user)) {
//            $this->getResponse()
//                    ->setHttpResponseCode(404)
//                    ->appendBody('Unauthorized');
//            return;
//        }

        $station = new Stations_Model_Station();
        $station->setupdateDate('now');
        $station->fill($params);
        $result = Stations_Service_Station::getInstance()
                ->save($station);
        if ($result) {
            $this->getResponse()
                    ->setHttpResponseCode(200)
                    ->appendBody(json_encode($result->toArray()));
            return;
        }
    }

    public function putAction()
    {
        $service = Stations_Service_Station::getInstance();
        $params = $this->getParams();

        $station = $service->getByPK($params['id']);
        //TO Do
        if (!$station instanceof Stations_Model_Station) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody('Not Found (Station)');
            return;
        }

        $station->fill($params);
        $station->setUpdateDate('now');
        $station->setNew(false);
        $station = $service->save($station);

        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody(json_encode($station->toArray()));
    }

    public function deleteAction()
    {
        $service = Stations_Service_Station::getInstance();
        $stationId = $this->_getParam('id');
        $station = $service->getByPK($stationId);

        if (!$station instanceof Stations_Model_Station) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody("Not Found (Station)");
            return;
        }

        $service->remove($station);

        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody("Success");
    }

}

