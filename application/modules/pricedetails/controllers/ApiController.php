<?php

class Pricedetails_ApiController extends Tea_Controller_Rest_Action
{

    public function init()
    {
        parent::init();
    }

    public function indexAction()
    {
        $limit = $this->_getParam('limit', 20);
        $start = $this->_getParam('start', 0);
        $updateDate = $this->getParam("updateDate");
        $type = $this->getParam("type");
        $deleted = $this->_getParam('deleted');
        $parentId = $this->getParam("parentId");

        $orderBy = $this->_getParam('order_by', 'updateDate');
        $orderDir = $this->_getParam('order_dir', 'desc');
        $sort = array($orderBy => $orderDir);

        $filter = array();

        isset($updateDate) && $filter["updateDate"] = $updateDate;
        isset($parentId) && $filter["parentId"] = $parentId;
        isset($type) && $filter["type"] = $type;
        isset($deleted) && $filter["deleted"] = $deleted;

        $dt = new DateTime($str);
        $dt->setTimeZone(new DateTimeZone('Asia/Tehran'));

        $pricedetails_service = Pricedetails_Service_Pricedetail::getInstance();
        $pricedetails = $pricedetails_service->getList($filter, $sort, $start, $count, $limit);
        $list = array();
        foreach ($pricedetails as $item) {
            $item = $item->toArray();
            $r_service = Resources_Service_Resource::getInstance();
            $images = $r_service->getImagesByParent(array(
                "parentId" => $item["id"],
                "parentType" => "pricedetails"
                    ));
            $item["photos"] = $images;
            $list[] = $item;
        }

        $response = array(
            'count' => $count,
            'list' => $list,
            'updateDate' => $dt->format('Y-m-d H:i:s')
        );
        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody(json_encode($response));
    }

    public function getAction()
    {
        $id = $this->_getParam('id');

        $service = Pricedetails_Service_Pricedetail::getInstance();
        $pricedetail = $service->getByPK($id);

        if ($pricedetail)
            $result = $pricedetail->toArray();
        else
            $result = array();

        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody(json_encode($result));
    }

    public function postAction()
    {
        $params = $this->getParams();
        $authService = Users_Service_Auth::getInstance();

        $user = $authService->getCurrentUser();

        $images = $params["images"];
        $pricedetail = new Pricedetails_Model_Pricedetail();
        $pricedetail->setupdateDate('now');
        $pricedetail->fill($params);
        $result = Pricedetails_Service_Pricedetail::getInstance()
                ->save($pricedetail);

        if ($result) {
            $new_item = $result->toArray();

//            if (count($images)) {
//                foreach ($images as $img) {
//                    $r_model = new Resources_Model_Resource();
//                    $r_service = Resources_Service_Resource::getInstance();
//
//                    $img['parentId'] = $result->getId();
//                    $r_model->fill($img);
//                    $r_model->setNew(false);
//                    $r_service->save($r_model);
//                }
//
//                $new_item["photos"] = $images;
//            }

            $this->getResponse()
                    ->setHttpResponseCode(200)
                    ->appendBody(json_encode($new_item));
            return;
        }
    }

    public function putAction()
    {
        $service = Pricedetails_Service_Pricedetail::getInstance();
        $params = $this->getParams();

        $pricedetail = $service->getByPK($params['id']);
        //TO Do
        if (!$pricedetail instanceof Pricedetails_Model_Pricedetail) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody('Not Found (Pricedetail)');
            return;
        }

        $pricedetail->fill($params);
        $pricedetail->setUpdateDate('now');
        $pricedetail->setNew(false);
        $pricedetail = $service->save($pricedetail);

        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody(json_encode($pricedetail->toArray()));
    }

    public function deleteAction()
    {
        $service = Pricedetails_Service_Pricedetail::getInstance();
        $pricedetailId = $this->_getParam('id');
        $pricedetail = $service->getByPK($pricedetailId);

        if (!$pricedetail instanceof Pricedetails_Model_Pricedetail) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody("Not Found (Pricedetail)");
            return;
        }

        $service->remove($pricedetail);

        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody("Success");
    }

}

