<?php

class Pricedetails_Model_Pricedetail extends Tea_Model_Entity
{

    protected $_properties = array(
        'productPriceNumber' => NULL,
        'productCode' => NULL,
        'rowNumber' => NULL,
        'price' => NULL,
        'grade1Price' => NULL,
        'grade2Price' => NULL,
        'premiumPrice' => NULL,
        'packingPrice' => NULL,
        'updateDate' => NULL,
        'creationDate' => NULL,
        'deleted' => 0
    );

    public function __construct()
    {
        parent::__construct();

        $this->setCreationDate('now');
        $this->setUpdateDate('now');
    }

    public function fill($record)
    {
        foreach ($record as $key => $value) {
            switch ($key) {
                case 'productPriceNumber' :
                case 'productCode' :
                case 'rowNumber' :
                case 'price' :
                case 'grade1Price':
                case 'grade2Price' :
                case 'premiumPrice':
                case 'packingPrice' :
                case 'updateDate':
                case 'creationDate' :
                case 'deleted':
                    $this->_properties[$key] = $value;
                    break;
            }
        }
    }

}

