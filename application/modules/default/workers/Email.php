<?php

/**
 * Sample data
 *  $data = array (
 *       'template'   => 'signup',
 *       'to'         => 'alireza.meskin@gmail.com',
 *       'subject'    => 'Signup',
 *       'from_name'  => 'Application.com',
 *       'from_email' => 'noreply@application.com',
 *       'params'     => array (
 *           'first_name' => 'Alireza',
 *           'last_name'  => 'Meskin'
 *       )
 *  );
 *  Tea_Service_Gearman::getInstance()->doBackground('default', 'email', $data);
 */

class Default_Worker_Email
{
    public function work($data)
    {
        return;
        $data['to'] = 'alireza.meskin@gmail.com';

        $config    = Zend_Registry::get('config');
        $fromName  = isset($data['from_name'])
            ? $data['from_name'] : $config->mail->from->name;
        $fromEmail = isset($data['from_email'])
            ? $data['from_email'] : $config->mail->from->address;

        $html = new Zend_View();
        $html->setScriptPath(APPLICATION_PATH . '/modules/default/views/emails/');

        if (is_array($data['params'])) {
            foreach ($data['params'] as $key => $value) {
                $html->assign($key, $value);
            }
        }

        $bodyText = '';
        $template = $html->getScriptPath($data['template'] . '.phtml');
        if (is_readable($template)) {
            $bodyText = $html->render($data['template'] . '.phtml');
        }

        // configure base stuff
        $mail = new Zend_Mail('utf-8');
        $mail->addTo($data['to']);
        $mail->setSubject($data['subject']);
        $mail->setFrom($fromEmail, $fromName);
        $mail->setBodyHtml($bodyText);
        $mail->send();
    }
}
