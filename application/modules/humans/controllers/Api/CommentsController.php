<?php

class Feeds_Api_CommentsController extends Tea_Controller_Rest_Action
{

    public function init()
    {
        parent::init();
    }

    public function indexAction()
    {
        $activityService    = Feeds_Service_Activity::getInstance();
        $commentService     = Feeds_Service_ActivityComment::getInstance();
        
        $limit       = $this->_getParam('limit', 30);
        $start       = $this->_getParam('start', 0);
        $orderBy     = $this->_getParam('order_by', 'creation_date');
        $orderDir    = $this->_getParam('order_dir', 'asc');

        $activityId = $this->_getParam('id');
        $activity   = $activityService->getByPK($activityId);
        if (!$activity instanceof Feeds_Model_Activity) {
            $this->getResponse()
                ->setHttpResponseCode(404)
                ->appendBody("Not Found (Activity)");
            return;
        }
        
        $filter = array(
            'activity_id' => $activity->getId()
        );
        
        $sort = array($orderBy => $orderDir);
        
        $comments = $commentService->getList($filter, $sort, $start, $count, $limit, $this->_currentUser);

        $list = array();
        foreach ($comments as $comment) {
            $_comment = $comment->toArray();
            $_comment['author']     = $comment->getAuthor()->toArray();
            $_comment['is_liked']   = $comment->getIsLiked();
            
            $list[] = $_comment;
        }

        $result = array(
            'count' => $count,
            'list'  => $list
        );

        $response = json_encode($result);
        $this->getResponse()
            ->setHttpResponseCode(200)
            ->appendBody($response);
    }

    public function getAction()
    {
        $activityService    = Feeds_Service_Activity::getInstance();
        $commentService     = Feeds_Service_Comment::getInstance();
        
        $activityId = $this->_getParam('id');
        $activity   = $activityService->getByPK($activityId);
        if (!$activity instanceof Feeds_Model_Activity) {
            $this->getResponse()
                ->setHttpResponseCode(404)
                ->appendBody("Not Found (Activity)");
            return;
        }
        
        $commentId = $this->_getParam('sub_id');
        $comment   = $commentService->getDetailsByPK($commentId, null, $this->_currentUser);
        if (!$comment instanceof Feeds_Model_ActivityComment) {
            $this->getResponse()
                ->setHttpResponseCode(404)
                ->appendBody("Not Found (Comment)");
            return;
        }
        if ($comment->getActivityId() != $activity->getId()) {
            $this->getResponse()
                ->setHttpResponseCode(404)
                ->appendBody("Comment Not For This Activity");
            return;
        }
        
        $author = $comment->getAuthor();
        if (!$author instanceof Users_Model_User) {
            $this->getResponse()
                ->setHttpResponseCode(404)
                ->appendBody("Not Found (Author)");
            return;
        }
                
        $_comment = $comment->toArray();
        $_comment['author']     = $author->toArray();
        $_comment['is_liked']   = $comment->getIsLiked();
        
        $response = json_encode($_comment);
        $this->getResponse()
            ->setHttpResponseCode(200)
            ->appendBody($response);
    }

    public function postAction()
    {
        $activityService    = Feeds_Service_Activity::getInstance();
        $commentService     = Feeds_Service_ActivityComment::getInstance();

        if ($this->_currentUser == null) {
            $this->getResponse()
                ->setHttpResponseCode(404)
                ->appendBody("Not Found (User)");
            return;
        }
        
        $activityId = $this->_getParam('id');
        $activity   = $activityService->getByPK($activityId);
        if (!$activity instanceof Feeds_Model_Activity) {
            $this->getResponse()
                ->setHttpResponseCode(404)
                ->appendBody("Not Found (Activity)");
            return;
        }
        
        $valid = $this->checkParams(array(
            'message'
        ));
        if (!$valid) {
            return;
        }

        $params = $this->getParams();
        
        unset($params['id']);
        
        $comment = new Feeds_Model_ActivityComment();
        $comment->fill($params);
        $comment->setActivityId($activity->getId());
        $comment->setAuthorId($this->_currentUser->getId());
        $comment->setNew(true);
        $comment = $commentService->save($comment);
        
        $_author    = $this->_currentUser->toArray();
        $_comment   = $comment->toArray();
        $_comment['author']     = $_author;
        $_comment['is_liked']   = false;
        
        $response = json_encode($_comment);
        $this->getResponse()
            ->setHttpResponseCode(200)
            ->appendBody($response);
    }

    public function deleteAction()
    {
        $activityService    = Feeds_Service_Activity::getInstance();
        $commentService     = Feeds_Service_Comment::getInstance();
        
        $activityId     = $this->_getParam('id');
        $activity       = $activityService->getByPK($activityId);
        if (!$activity instanceof Feeds_Model_Activity) {
            $this->getResponse()
                ->setHttpResponseCode(404)
                ->appendBody("Not Found (Activity)");
            return;
        }

        $commentId  = $this->_getParam('sub_id');
        $comment    = $commentService->getByPK($commentId);
        if (!$comment instanceof Feeds_Model_ActivityComment) {
            $this->getResponse()
                ->setHttpResponseCode(404)
                ->appendBody("comment Not Found");
            return;
        }
        if ($comment->getActivityId() != $activity->getId()) {
            $this->getResponse()
                ->setHttpResponseCode(404)
                ->appendBody("Comment Not For This Activity");
            return;
        }
        
        //------------------- Check User Access ---------------------
        if ($this->_currentUser == null) {
            $this->getResponse()
                ->setHttpResponseCode(401)
                ->appendBody("Unauthorized");
            return;
        }
        
        if (($this->_currentUser->getId() != $comment->getAuthorId()) &&
             $this->_currentUser->getId() != $activity->getActorId()) {
            $this->getResponse()
                ->setHttpResponseCode(403)
                ->appendBody("Forbidden");
            return;
        }
        //-----------------------------------------------------------
        
        $commentService->remove($comment);

        $response = json_encode("Success");
        $this->getResponse()
            ->setHttpResponseCode(200)
            ->appendBody($response);
    }

}
