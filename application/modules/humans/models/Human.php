<?php

class Humans_Model_Human extends Tea_Model_Entity
{

    const TYPE_EPICER = 0;
    const TYPE_MANAGER = 1;

    protected $_properties = array(
        'id' => NULL,
        'photoId' => NULL,
        'name' => NULL,
        'type' => NULL,
        'locationService' => NULL,
        'family' => NULL,
        'birthday' => NULL,
        'testimonyAddress' => NULL,
        'martyrdom' => NULL,
        'history' => NULL,
        'degree' => NULL,
        'testimonyDate' => NULL,
        'updateDate' => NULL,
        'deleted' => 0
    );

    public function __construct()
    {
        parent::__construct();

        $this->setUpdateDate('now');
    }

    public function fill($record)
    {
        foreach ($record as $key => $value) {
            switch ($key) {
                case 'id' :
                case 'type' :
                case 'history' :
                case 'locationService' :
                case 'name' :
                case 'family':
                case 'photoId':
                case 'birthday':
                case 'testimonyAddress':
                case 'martyrdom':
                case 'degree':
                case 'testimonyDate' :
                case 'updateDate':
                case 'deleted' :
                    $this->_properties[$key] = $value;
                    break;
            }
        }
    }

}

