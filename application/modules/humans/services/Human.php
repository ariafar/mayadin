<?php

class Humans_Service_Human extends Tea_Service_Abstract
{

    private static $_instance = null;
    private $_table = null;

    private function __construct()
    {
        $this->_table = new Humans_Model_DbTable_Humans();
    }

    public static function getInstance()
    {
        if (self::$_instance === null) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    public function getByPK($id, Humans_Model_Human $human = null)
    {
        $rows = $this->_table->find($id);
        if (count($rows) == 0) {
            return null;
        }
        $row = $rows->current()->toArray();
        if (!$human instanceof Humans_Model_Human) {
            $human = new Humans_Model_Human();
        }
        $human->fill($row);
        $human->setNew(false);
        return $human;
    }

    public function getList($filter, $sort, $start, &$count, $limit = 10)
    {
        $select = $this->_table->getDefaultAdapter()->select();
        $cSelect = clone $select;

        $select->from(array('u' => 'humans'), array('*'));
        $cSelect->from(array('u' => 'humans'), array('COUNT(*) AS count'));

        if (is_array($sort)) {
            foreach ($sort as $key => $ord) {
                $key = "u.$key";
                $select->order($key . ' ' . strtoupper($ord));
            }
        }

        if (is_array($filter)) {
            foreach ($filter as $key => $value) {
                switch ($key) {
                    case 'title':
                        $select->where("CONCAT(u.title, u.lastName) LIKE '%$value%'");
                        break;

                    case 'updateDate':
                        $select->where("u.updateDate > '$value'");
                        $cSelect->where("u.updateDate > '$value'");
                        break;


                    default:
                        $key = "u.$key";
                        $select->where("{$key} = ?", $value);
                        $cSelect->where("{$key} = ?", $value);
                }
            }
        }

        $rows = $this->_table->getAdapter()->fetchRow($cSelect);
        $count = (int) $rows['count'];

        $select->limit($limit, $start);

        $humans = array();
        foreach ($this->_table->getAdapter()->fetchAll($select) as $row) {
            $item = new Humans_Model_Human();
            $item->fill($row);
            $humans[] = $item;
        }

        return $humans;
    }

    public function save(Humans_Model_Human $human)
    {
        if ($human->isNew()) {
            $data = $human->toArray(true);
            $pk = $this->_table->insert($data);
            if ($pk) {
                return $this->getByPK($pk, $human);
            }
        } else {
            $id = $human->getId();
            $data = $human->toArray(true);
            $where = $this->_table->getAdapter()->quoteInto('id = ?', $id);
            $this->_table->update($data, $where);
            return $this->getByPK($id, $human);
        }

        return false;
    }

    public function remove(Humans_Model_Human $human)
    {
        $where = $this->_table->getAdapter()->quoteInto('id = ?', $human->getId());
        $this->_table->delete($where);
    }

    public function removeByConditions($conditions = array())
    {
        $where = '';
        foreach ($conditions as $property => $value) {
            if (strlen($where) > 0) {
                $where .= ' AND ';
            }
            $where .= $this->_table->getAdapter()->quoteInto($property . ' = ?', $value);
        }

        $this->_table->delete($where);
    }

    public function removeByQuery($property, $query)
    {
        $this->_table->delete("$property IN ($query)");
    }

    public function removeAll()
    {
        $this->_table->delete('');
    }

}
