<?php

class Educations_Model_Education extends Tea_Model_Entity
{

    const CATEGORY_SHORT_HEALTH_MESSAGES = 6;
    const CATEGORY_DO_YOU_KNOW = 7;

    protected $_properties = array(
        'id' => NULL,
        'title' => NULL,
        'description' => NULL,
        'category' => NULL,
        'creationDate' => NULL,
        'updateDate' => NULL,
        'deleted' => 0
    );

    public function __construct()
    {
        parent::__construct();

        $this->setCreationDate('now');
        $this->setUpdateDate('now');
    }

    public function fill($record)
    {
        foreach ($record as $key => $value) {
            switch ($key) {
                case 'id' :
                case 'title' :
                case 'description' :
                case 'category' :
                case 'creationDate' :
                case 'updateDate' :
                case 'deleted' :
                    $this->_properties[$key] = $value;
                    break;
            }
        }
    }

}

