<?php

class QuestionsAnswers_Model_QuestionAnswer extends Tea_Model_Entity
{

    const TYPE_QUESTION_ANSWER = 1;
    const TYPE_SUGGESTION_FEEDBACK = 2;

    protected $_properties = array(
        'id' => NULL,
        'type' => NULL,
        'askerName' => NULL,
        'askerFamily' => NULL,
        'askerEmail' => NULL,
        'title' => NULL,
        'question' => NULL,
        'answer' => NULL,
        'creationDate' => NULL,
        'updateDate' => NULL,
        'deleted' => 0
    );

    public function __construct()
    {
        parent::__construct();

        $this->setCreationDate('now');
        $this->setUpdateDate('now');
    }

    public function fill($record)
    {
        foreach ($record as $key => $value) {
            switch ($key) {
                case 'id' :
                case 'type' :
                case 'askerName' :
                case 'askerFamily' :
                case 'askerEmail' :
                case 'question' :
                case 'title':
                case 'answer' :
                case 'creationDate' :
                case 'updateDate' :
                case 'deleted' :
                    $this->_properties[$key] = $value;
                    break;
            }
        }
    }

}

