<?php

class Evaluations_Model_Response extends Tea_Model_Entity
{

    protected $_properties = array(
        'id' => null,
        'responderId' => null,
        'responderName' => null,
        'questionId' => null,
        'evaluationId' => null,
        'value' => null,
        'creationDate' => null,
        'updateDate' => null,
    );

    public function __construct()
    {
        parent::__construct();

        $this->setCreationDate('now');
        $this->setUpdateDate('now');
    }

    public function fill($record)
    {
        foreach ($record as $key => $value) {
            switch ($key) {
                case 'id' :
                case 'responderId' :
                case 'responderName' :
                case 'questionId' :
                case 'evaluationId' :
                case 'value' :
                case 'creationDate' :
                case 'updateDate':
                    $this->_properties[$key] = $value;
                    break;
            }
        }
    }

}
