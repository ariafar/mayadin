<?php

class Polls_Model_Assignee extends Tea_Model_Entity
{

    protected $_properties = array(
        'assignee_id'       => null,
        'poll_id'           => null,
        'status'            => null,
        'created_by_id'     => null,
        'creation_date'     => null,
        'updated_by_id'     => null,
        'update_date'       => null,
    );

    public function __construct()
    {
        parent::__construct();

        $this->setCreationDate('now');
        $this->setUpdateDate('now');
    }

    public function fill($record)
    {
        foreach ($record as $key => $value) {
            switch ($key) {
                case 'assignee_id' :
                case 'poll_id' :
                case 'status' :
                case 'created_by_id' :
                case 'creation_date' :
                case 'updated_by_id' :
                case 'update_date' :
                    $this->_properties[$key] = $value;
                    break;
            }
        }
    }
}
