ALTER TABLE polls
ADD FOREIGN KEY (created_by_id)                   REFERENCES users(id),
ADD FOREIGN KEY (updated_by_id)                   REFERENCES users(id);



ALTER TABLE poll_votes
ADD FOREIGN KEY (poll_id)                REFERENCES polls(id);



ALTER TABLE poll_voting_items
ADD FOREIGN KEY (voting_id)                       REFERENCES poll_votes(id),
ADD FOREIGN KEY (poll_id)                REFERENCES polls(id);



ALTER TABLE poll_assignees
ADD FOREIGN KEY (assignee_id)                     REFERENCES users(id),
ADD FOREIGN KEY (poll_id)                REFERENCES polls(id);


ALTER TABLE poll_voting_responses
ADD FOREIGN KEY (responder_id)                    REFERENCES users(id),
ADD FOREIGN KEY (voting_id)                       REFERENCES poll_votes(id),
ADD FOREIGN KEY (poll_id)                REFERENCES polls(id);
