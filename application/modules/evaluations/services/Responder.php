<?php

class Evaluations_Service_Responder extends Tea_Service_Abstract
{

    private static $_instance = null;
    private $_table = null;

    private function __construct()
    {
        $this->_table = new Evaluations_Model_DbTable_Responders();
    }

    public static function getInstance()
    {
        if (self::$_instance === null) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    public function getByPK($responderId, $evaluationId, Evaluations_Model_Responder $questionResponder = null)
    {
        $rows = $this->_table->find($responderId, $evaluationId);
        if (count($rows) == 0) {
            return null;
        }
        $row = $rows->current()->toArray();

        if (!$questionResponder instanceof Evaluations_Model_Responder) {
            $questionResponder = new Evaluations_Model_Responder();
        }
        $questionResponder->fill($row);
        $questionResponder->setNew(false);
        return $questionResponder;
    }

    public function getByResponderId($responderId, $evaluationId, Evaluations_Model_Responder $questionResponder = null)
    {
        $cSelect = $this->_table->select();
        $cSelect->where("evaluationId = ?", $evaluationId);
        $cSelect->where("responderId = ?", $responderId);
        $cSelect->from($this->_table, array('COUNT(DISTINCT evaluationId, responderId) AS count'));

        $rows = $this->_table->fetchRow($cSelect);
        $count = (int) $rows['count'];

        return $count;
    }

    public function getList($filter, $sort, $start, &$count, $limit = 10)
    {
        $select = $this->_table->select();
        $select->where("evaluationId = ?", $avaluation->getId());

        if (is_array($filter)) {
            foreach ($filter as $key => $value) {
                $select->where("{$key} = ?", $value);
            }
        }

        if (is_array($sort)) {
            foreach ($sort as $key => $ord) {
                $select->order($key . ' ' . strtoupper($ord));
            }
        }
        $cSelect = clone $select;
        $cSelect->from($this->_table, array('COUNT(*) AS count'));
        $rows = $this->_table->fetchRow($cSelect);
        $count = (int) $rows['count'];

        $select->limit($limit, $start);

        $result = array();
        foreach ($this->_table->fetchAll($select) as $row) {
            $questionResponder = new Evaluations_Model_Responder();
            $questionResponder->fill($row);
            $questionResponder->setNew(false);
            $result[] = $questionResponder;
        }

        return $result;
    }

    public function countEvaluationResponders(Evaluations_Model_Evaluation $avaluation)
    {
        $cSelect = $this->_table->select();
        $cSelect->where("evaluationId = ?", $avaluation->getId());
        $cSelect->from($this->_table, array('COUNT(DISTINCT evaluationId, responderId) AS count'));

        $rows = $this->_table->fetchRow($cSelect);
        $count = (int) $rows['count'];

        return $count;
    }

    public function countQuestionResponders(Evaluations_Model_Question $question)
    {
        $cSelect = $this->_table->select();
        $cSelect->where("questionId = ?", $question->getId());
        $cSelect->from($this->_table, array('COUNT(DISTINCT questionId, responderId) AS count'));

        $rows = $this->_table->fetchRow($cSelect);
        $count = (int) $rows['count'];

        return $count;
    }

    public function countItemResponders(Evaluations_Model_QuestionItem $item)
    {
        $cSelect = $this->_table->select();
        $cSelect->where("questionId = ?", $item->getQuestionId());
        $cSelect->where("value = ?", $item->getValue());
        $cSelect->from($this->_table, array('COUNT(DISTINCT responderId) AS count'));

        $rows = $this->_table->fetchRow($cSelect);
        $count = (int) $rows['count'];

        return $count;
    }

    public function save(Evaluations_Model_Responder $questionResponder)
    {
        if ($questionResponder->isNew()) {
            $data = $questionResponder->toArray(true);
            $pk = $this->_table->insert($data);
            if ($pk) {
                return $this->getByPK($pk['responderId'], $pk['evaluationId']);
            }
        } else {
            $data = $questionResponder->toArray(true);
            $where1 = $this->_table->getAdapter()->quoteInto('responderId= ?', $questionResponder->getResponderId());
            $where2 = $this->_table->getAdapter()->quoteInto('questionId = ?', $questionResponder->getQuestionId());
            $where = $where1 . " AND " . $where2;
            $this->_table->update($data, $where);
            return $this->getByPK($questionResponder->getResponderId(), $questionResponder->getQuestionId(), $questionResponder->getValue(), $questionResponder);
        }
        return false;
    }

    public function remove(Evaluations_Model_Responder $questionResponder)
    {
        $where1 = $this->_table->getAdapter()->quoteInto('responderId= ?', $questionResponder->getResponderId());
        $where2 = $this->_table->getAdapter()->quoteInto('questionId = ?', $questionResponder->getQuestionId());
        $where3 = $this->_table->getAdapter()->quoteInto('value = ?', $questionResponder->getValue());
        $where = $where1 . " AND " . $where2 . " AND " . $where3;
        $this->_table->delete($where);
    }

    public function removeAll()
    {
        $this->_table->delete('');
    }

    public function getTopScores($evaluationId)
    {
        $select = $this->_table->select();
        $select->where("evaluationId = ?", $evaluationId);
        $select->from($this->_table, array('MAX(trueAnswersCount),id,responderId,responderName,evaluationId,trueAnswersCount'));
        $rows = $this->_table->fetchAll($select);

        $result = array();
        foreach ($rows as $row) {
            $questionResponder = new Evaluations_Model_Responder();
            $questionResponder->fill($row);
            $questionResponder->setNew(false);
            $result[] = $questionResponder;
        }

        return $result;
    }

}

?>
