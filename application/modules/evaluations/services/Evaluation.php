<?php

class Evaluations_Service_Evaluation extends Tea_Service_Abstract
{

    private static $_instance = null;
    private $_table = null;

    private function __construct()
    {
        $this->_table = new Evaluations_Model_DbTable_Evaluations();
    }

    public static function getInstance()
    {
        if (self::$_instance === null) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    public function getByPK($id, Evaluations_Model_Evaluation $evaluation = null, $filter = array())
    {
        $questionService = Evaluations_Service_Question::getInstance();

        $rows = $this->_table->find($id);
        if (count($rows) == 0) {
            return null;
        }
        $row = $rows->current()->toArray();
        if (!$evaluation instanceof Evaluations_Model_Evaluation) {
            $evaluation = new Evaluations_Model_Evaluation();
        }
        $evaluation->setNew(false);
        $evaluation->fill($row);


        // ------------------ Retrieve Questions -------------------
        $q_filter = array(
            'evaluationId' => $evaluation->getId()
        );
        if (isset($filter['deleted'])) {
            $q_filter['deleted'] = $filter['deleted'];
        }
//        $sort = array(
//            'ordering' => 'asc'
//        );

        $questions = $questionService->getList($q_filter, $sort, 0, $vCount, 1000);

        $evaluation->setQuestions($questions);
        return $evaluation;
    }

    public function getList($filter, $sort, $start, &$count, $limit = 10)
    {
        $questionService = Evaluations_Service_Question::getInstance();

        $select = $this->_table->getDefaultAdapter()->select();
        $cSelect = clone $select;

        $cSelect->from(array('p' => 'evaluations'), array('COUNT(*) AS count'));

        $select->from(array('p' => 'evaluations'));

        if (is_array($filter)) {
            foreach ($filter as $key => $value) {
                switch ($key) {
                    case 'updateDate':
                        $select->where("p.updateDate >= '$value'");
                        $cSelect->where("p.updateDate >= '$value'");
                        break;

                    default:
                        $key = "p.$key";
                        $select->where("{$key} = ?", $value);
                        $cSelect->where("{$key} = ?", $value);
                }
            }
        }

        if (is_array($sort)) {
            foreach ($sort as $key => $ord) {
                $key = "p.$key";
                $select->order($key . ' ' . strtoupper($ord));
            }
        }

        $rows = $this->_table->getAdapter()->fetchRow($cSelect);
        $count = (int) $rows['count'];

        $select->limit($limit, $start);

        $evaluations = array();
        foreach ($this->_table->getAdapter()->fetchAll($select) as $row) {

            $evaluation = new Evaluations_Model_Evaluation();
            $evaluation->fill($row);
            $evaluation->setNew(false);

            // ------------------ Retrieve Questions -------------------
            $filter = array(
                'evaluationId' => $evaluation->getId()
            );
//            $sort = array(
//                'ordering' => 'asc'
//            );

            $questions = $questionService->getList($filter, $sort, 0, $vCount, 1000);
            $evaluation->setQuestions($questions);

            $evaluations[] = $evaluation;
        }

        return $evaluations;
    }

    public function countRefEvaluations($refType, $refId)
    {
        $cSelect = $this->_table->select();
        $cSelect->where("ref_type = ?", $refType);
        $cSelect->where("ref_id = ?", $refId);
        $cSelect->from($this->_table, array('COUNT(*) AS count'));

        $rows = $this->_table->fetchRow($cSelect);
        $count = (int) $rows['count'];

        return $count;
    }

    public function countUserEvaluations(Users_Model_User $user)
    {
        $cSelect = $this->_table->getDefaultAdapter()->select();
        $cSelect->from(array('p' => 'polls'), array('COUNT(*) AS count'));
        $cSelect->where("created_by_id = ?", $user->getId());

        $rows = $this->_table->getAdapter()->fetchRow($cSelect);
        $count = (int) $rows['count'];

        return $count;
    }

    public function calculateStatistics(Evaluations_Model_Evaluation $evaluation)
    {
        $evaluation = $this->getByPK($evaluation->getId());
        $questions = $evaluation->getQuestions();

        $results = array();
        foreach ($questions as $question) {

            $result = array(
                'questionId' => $question->getId(),
                'items' => array()
            );

            if ($evaluation->getType() == Evaluations_Model_Evaluation::TYPE_POLL) {
                $denominator = 0;
                foreach ($question->getItems() as $item) {
                    $denominator+= $item->getResponseCount();
                }
            } else {
                $denominator = $evaluation->getResponseCount();
            }

            foreach ($question->getItems() as $item) {
                $percent = ($evaluation->getResponseCount() == 0) ? 0 :
                        (float) ($item->getResponseCount() / $denominator);

                $result['items'][] = array(
                    'value' => $item->getValue(),
                    'percent' => $percent
                );
            }

            $results[] = $result;
        }

        return $results;
    }

    public function save(Evaluations_Model_Evaluation $evaluation, $triggerEvent = true)
    {
        if ($evaluation->isNew()) {
            $data = $evaluation->toArray(true);
            $pk = $this->_table->insert($data);
            if ($pk) {
                $evaluation = $this->getByPK($pk, $evaluation);
//                if ($triggerEvent) {
//                    Tea_Hook_Registry::dispatchEvent('create_poll', $evaluation);
//                }
                return $evaluation;
            }
        } else {
            $id = $evaluation->getId();
            $data = $evaluation->toArray(true);
            $where = $this->_table->getAdapter()->quoteInto('id = ?', $id);
            $this->_table->update($data, $where);

            $evaluation = $this->getByPK($id, $evaluation);
//            if ($triggerEvent) {
//                Tea_Hook_Registry::dispatchEvent('edit_poll', $evaluation);
//            }
            return $evaluation;
        }

        return false;
    }

    public function saveCompleteEvaluation(Evaluations_Model_Evaluation $evaluation)
    {
        $questionService = Evaluations_Service_Question::getInstance();
        $questionItemService = Evaluations_Service_QuestionItem::getInstance();

        $questions = $evaluation->getQuestions();

        $this->_table->getAdapter()->beginTransaction();
        try {
            $evaluation = $this->save($evaluation, false);

            if (is_array($questions)) {
                foreach ($questions as $question) {
                    $items = $question->getItems();

                    $question->setEvaluationId($evaluation->getId());
                    $question = $questionService->save($question, false);

                    if (is_array($items)) {
                        foreach ($items as $item) {
                            $item->setEvaluationId($evaluation->getId());
                            $item->setQuestionId($question->getId());
                            $item = $questionItemService->save($item, false);
                        }
                    }
                }
            }

            $this->_table->getAdapter()->commit();
        } catch (Exception $e) {
            $this->_table->getAdapter()->rollBack();
            return $e->getMessage();
        }

        $evaluation = $this->getByPK($evaluation->getId());
//        Tea_Hook_Registry::dispatchEvent('create_poll', $evaluation);
        return $evaluation;
    }

    public function responseToEvaluation(Users_Model_User $user, Evaluations_Model_Evaluation $evaluation, $responses)
    {
        $questionResponseService = Evaluations_Service_Response::getInstance();

        $this->_table->getAdapter()->beginTransaction();
        $questionResponseService->removeUserResponsesOfEvaluation($user, $evaluation);

        foreach ($responses as $response) {
            $response = $questionResponseService->save($response);
        }

        $this->_table->getAdapter()->commit();

        $data = array(
            'evaluation' => $evaluation,
            'user' => $user
        );

        $this->_updateResponseCount($evaluation);
    }

    private function _updateResponseCount(Evaluations_Model_Evaluation $evaluation)
    {
        $evaluationService = Evaluations_Service_Evaluation::getInstance();
        $questionService = Evaluations_Service_Question::getInstance();
        $questionItemService = Evaluations_Service_QuestionItem::getInstance();
        $questionResponseService = Evaluations_Service_Response::getInstance();

        $evaluation = $evaluationService->getByPK($evaluation->getId());

        $respCount = $questionResponseService->countEvaluationResponses($evaluation);
        $evaluation->setResponseCount($respCount);
        $evaluation->setNew(false);
        $evaluationService->save($evaluation);

        $questions = $evaluation->getQuestions();
        foreach ($questions as $question) {
            $respCount = $questionResponseService->countQuestionResponses($question);
            $question->setResponseCount($respCount);
            $question->setNew(false);
            $questionService->save($question);

            $items = $question->getItems();
            foreach ($items as $item) {
                $respCount = $questionResponseService->countItemResponses($item);
                $item->setResponseCount($respCount);
                $item->setNew(false);
                $questionItemService->save($item);
            }
        }
    }

    public function remove(Evaluations_Model_Evaluation $evaluation)
    {
        $questionService = Evaluations_Service_Question::getInstance();
        $assigneeService = Evaluations_Service_Assignee::getInstance();

        $this->_table->getAdapter()->beginTransaction();
        try {
            //------------- delete related tables entries ---------------
            $questionService->removeByEvaluation($evaluation);
            $assigneeService->removeByEvaluation($evaluation);
            //-----------------------------------------------------------

            $where = $this->_table->getAdapter()->quoteInto('id = ?', $evaluation->getId());
            $this->_table->delete($where);

            $this->_table->getAdapter()->commit();
        } catch (Exception $e) {
            $this->_table->getAdapter()->rollBack();
            return $e->getMessage();
        }

        Tea_Hook_Registry::dispatchEvent('delete_poll', $evaluation);
    }

    public function removeAll()
    {
        $questionService = Evaluations_Service_Question::getInstance();
        $assigneeService = Evaluations_Service_Assignee::getInstance();

        $this->_table->getAdapter()->beginTransaction();
        try {
            //------------- delete related tables entries ---------------
            $questionService->removeAll();
            $assigneeService->removeAll();
            //-----------------------------------------------------------

            $this->_table->delete('');

            $this->_table->getAdapter()->commit();
        } catch (Exception $e) {
            $this->_table->getAdapter()->rollBack();
            return $e->getMessage();
        }
    }

    public function getWinner(Evaluations_Model_Evaluation $evaluation)
    {
        $responderService = Evaluations_Service_Responder::getInstance();
        $winners = $responderService->getTopScores($evaluation->getId());
        return $winners;
    }

}

?>
