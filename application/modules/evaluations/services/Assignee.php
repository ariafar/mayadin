<?php

class Polls_Service_Assignee extends Tea_Service_Abstract
{
    private static $_instance = null;
    private $_table    = null;

    private function __construct()
    {
        $this->_table = new Polls_Model_DbTable_Assignees();
    }

    public static function getInstance()
    {
        if (self::$_instance === null) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    public function getByPK($assignee_id, $poll_id, Polls_Model_Assignee $assignee = null)
    {
        $rows = $this->_table->find($assignee_id, $poll_id);
        if (count($rows) == 0) {
            return null;
        }
        $row = $rows->current()->toArray();

        if (!$assignee instanceof Polls_Model_Assignee) {
            $assignee = new Polls_Model_Assignee();
        }
        $assignee->fill($row);
        $assignee->setNew(false);
        return $assignee;
    }
    
    public function getList($filter, $sort, $start, &$count, $limit = 10)
    {
        $select = $this->_table->select();

        if (is_array($filter)) {
            foreach ($filter as $key => $value) {
                $select->where("{$key} = ?", $value);
            }
        }

        if (is_array($sort)) {
            foreach ($sort as $key => $ord) {
                $select->order($key . ' ' . strtoupper($ord));
            }
        }
        $cSelect = clone $select;
        $cSelect->from($this->_table, array('COUNT(*) AS count'));
        $rows  = $this->_table->fetchRow($cSelect);
        $count = (int) $rows['count'];

        $select->limit($limit, $start);

        $result = array();
        foreach ($this->_table->fetchAll($select) as $row) {
            $assignee = new Polls_Model_Assignee();
            $assignee->fill($row);
            $assignee->setNew(false);
            $result[] = $assignee;
        }

        return $result;
    }
    
    public function save(Polls_Model_Assignee $assignee)
    {
        if ($assignee->isNew()) {
            $data = $assignee->toArray(true);
            $pk   = $this->_table->insert($data);
            if ($pk) {
                return $this->getByPK($pk['assignee_id'], $pk['poll_id'], $assignee);
            }
        } else {
            $data   = $assignee->toArray(true);
            $where1 = $this->_table->getAdapter()->quoteInto('assignee_id= ?', $assignee->getAssigneeId());
            $where2 = $this->_table->getAdapter()->quoteInto('poll_id= ?', $assignee->getPollId());
            $where  = $where1 . " AND " . $where2;
            $this->_table->update($data, $where);
            return $this->getByPK($assignee->getAssigneeId(), $assignee->getPollId(), $assignee);
        }

        return false;
    }

    public function remove(Polls_Model_Assignee $assignee)
    {
        $where1 = $this->_table->getAdapter()->quoteInto('assignee_id= ?', $assignee->getAssigneeId());
        $where2 = $this->_table->getAdapter()->quoteInto('poll_id= ?', $assignee->getPollId());
        $where  = $where1 . " AND " . $where2;
        $this->_table->delete($where);
    }
    
    public function removeByPoll(Polls_Model_Poll $poll)
    {
        $where = $this->_table->getAdapter()->quoteInto('poll_id = ?', $poll->getId());
        $this->_table->delete($where);
    }

    public function removeAll()
    {
        $this->_table->delete('');
    }
}

?>
