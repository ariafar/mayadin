<?php

class Evaluations_Service_QuestionItem extends Tea_Service_Abstract
{

    private static $_instance = null;
    private $_table = null;

    private function __construct()
    {
        $this->_table = new Evaluations_Model_DbTable_QuestionItems();
    }

    public static function getInstance()
    {
        if (self::$_instance === null) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    public function getByPK($id, Evaluations_Model_QuestionItem $questionItem = null)
    {
        $rows = $this->_table->find($id);
        if (count($rows) == 0) {
            return null;
        }
        $row = $rows->current()->toArray();

        if (!$questionItem instanceof Evaluations_Model_QuestionItem) {
            $questionItem = new Evaluations_Model_QuestionItem();
        }
        $questionItem->fill($row);
        $questionItem->setNew(false);
        return $questionItem;
    }

    public function getList($filter, $sort, $start, &$count, $limit = 10)
    {
        $select = $this->_table->select();

        if (is_array($filter)) {
            foreach ($filter as $key => $value) {
                $select->where("{$key} = ?", $value);
            }
        }

        if (is_array($sort)) {
            foreach ($sort as $key => $ord) {
                $select->order($key . ' ' . strtoupper($ord));
            }
        }
        $cSelect = clone $select;
        $cSelect->from($this->_table, array('COUNT(*) AS count'));
        $rows = $this->_table->fetchRow($cSelect);
        $count = (int) $rows['count'];

        $select->limit($limit, $start);

        $result = array();
        foreach ($this->_table->fetchAll($select) as $row) {
            $questionItem = new Evaluations_Model_QuestionItem();
            $questionItem->fill($row);
            $questionItem->setNew(false);
            $result[] = $questionItem;
        }

        return $result;
    }

    public function save(Evaluations_Model_QuestionItem $questionItem, $triggerEvent = true)
    {
        if ($questionItem->isNew()) {
            $data = $questionItem->toArray(true);
            $pk = $this->_table->insert($data);
            if ($pk) {
                $questionItem = $this->getByPK($pk, $questionItem);
                if ($triggerEvent) {
                    $avaluation = Evaluations_Service_Evaluation::getInstance()->getByPK($questionItem->getEvaluationId());
//                    Tea_Hook_Registry::dispatchEvent('edit_poll', $avaluation);
                }
                return $questionItem;
            }
        } else {
            $id = $questionItem->getId();
            $data = $questionItem->toArray(true);
            $where = $this->_table->getAdapter()->quoteInto('id = ?', $id);
            $this->_table->update($data, $where);
            $questionItem = $this->getByPK($id, $questionItem);
            if ($triggerEvent) {
                $avaluation = Evaluations_Service_Evaluation::getInstance()->getByPK($questionItem->getEvaluationId());
                Tea_Hook_Registry::dispatchEvent('edit_poll', $avaluation);
            }
            return $questionItem;
        }

        return false;
    }

    public function remove(Evaluations_Model_QuestionItem $questionItem, $triggerEvent = true)
    {
        $questionResponseService = Evaluations_Service_Response::getInstance();

        $this->_table->getAdapter()->beginTransaction();
        try {
            //------------- delete related tables entries ---------------
            $questionResponseService->removeByItem($questionItem);
            //-----------------------------------------------------------

            $where = $this->_table->getAdapter()->quoteInto('id = ?', $questionItem->getId());
            $this->_table->delete($where);

            $this->_table->getAdapter()->commit();
        } catch (Exception $e) {
            $this->_table->getAdapter()->rollBack();
            return $e->getMessage();
        }

        if ($triggerEvent) {
            $avaluation = Evaluations_Service_Evaluation::getInstance()->getByPK($questionItem->getEvaluationId());
//            Tea_Hook_Registry::dispatchEvent('edit_poll', $avaluation);
        }
    }

    public function removeByEvaluation(Evaluations_Model_Evaluation $avaluation)
    {
        $questionResponseService = Evaluations_Service_Response::getInstance();
        $this->_table->getAdapter()->beginTransaction();
        try {
            //------------- delete related tables entries ---------------
            $questionResponseService->removeByEvaluation($avaluation);
            //-----------------------------------------------------------

            $where = $this->_table->getAdapter()->quoteInto('poll_id = ?', $avaluation->getId());
            $this->_table->delete($where);

            $this->_table->getAdapter()->commit();
        } catch (Exception $e) {
            $this->_table->getAdapter()->rollBack();
            return $e->getMessage();
        }
    }

    public function removeByQuestion(Evaluations_Model_Question $question)
    {

        $where = $this->_table->getAdapter()->quoteInto('questionId = ?', $question->getId());
        $this->_table->delete($where);
    }

    public function removeAll()
    {
        $questionResponseService = Evaluations_Service_Response::getInstance();

        $this->_table->getAdapter()->beginTransaction();
        try {
            //------------- delete related tables entries ---------------
            $questionResponseService->removeAll();
            //-----------------------------------------------------------

            $this->_table->delete('');

            $this->_table->getAdapter()->commit();
        } catch (Exception $e) {
            $this->_table->getAdapter()->rollBack();
            return $e->getMessage();
        }
    }

}

?>
