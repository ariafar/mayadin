<?php

class Evaluations_IndexController extends Tea_Controller_Action
{

    public function init()
    {
        $this->view->moduleLoader()->appendModule('polls');
        $this->view->headLink()->appendStylesheet('css/polls.css');
    }

}