<?php

class Evaluations_Api_QuestionsController extends Tea_Controller_Rest_Action
{

    public function init()
    {
        parent::init();
    }
   
    public function postAction()
    {
        $evaluationService    = Evaluations_Service_Evaluation::getInstance();
        $questionService  = Evaluations_Service_Question::getInstance();

        if ($this->_currentUser == null) {
            $this->getResponse()
                ->setHttpResponseCode(404)
                ->appendBody("Not Found (User)");
            return;
        }
        
        $evaluationId = $this->_getParam('id');
        $evaluation   = $evaluationService->getByPK($evaluationId);
        if (!$evaluation instanceof Evaluations_Model_Evaluation) {
            $this->getResponse()
                ->setHttpResponseCode(404)
                ->appendBody("Not Found (Evaluation)");
            return;
        }
        
        $params = $this->getParams();
        unset($params['id']);
        
        $valid = $this->checkParams(array(
            'question'
        ));
        if (!$valid) {
            return;
        }
            
        $question = new Evaluations_Model_Question();
        $question->fill($params);
        $question->setEvaluationId($evaluation->getId());
        $question->setCreatedById($this->_currentUser->getId());
        $question->setUpdatedById($this->_currentUser->getId());
        $question->setNew(true);
        
        $items = array();
        foreach ($params['items'] as $itemParams) {
            
            $itemRequiredParams = array(
                'text',
                'value'
            );

            foreach ($itemRequiredParams as $reqParam) {
                if (!isset($itemParams[$reqParam])) {
                    $this->getResponse()
                        ->setHttpResponseCode(404)
                        ->appendBody("Not Found " . $reqParam);
                    return;
                }

                if (is_string($itemParams[$reqParam])) {
                    if (strlen(trim($itemParams[$reqParam])) == 0) {
                        $this->getResponse()
                            ->setHttpResponseCode(404)
                            ->appendBody("Not Found " . $reqParam);
                        return;
                    }
                }
            }
            
            $item = new Evaluations_Model_QuestionItem();
            $item->fill($itemParams);
            $item->setNew(true);
            
            $items[] = $item;
        }
        
        $question->setItems($items);
        
        $question = $questionService->saveCompleteQuestion($question);
        $items  = $question->getItems();
        
        $_question = $question->toArray();
        $_question['items'] = array();
        foreach ($items as $item) {
            $_question['items'][] = $item->toArray();
        }

        $response = json_encode($_question);
        $this->getResponse()
            ->setHttpResponseCode(200)
            ->appendBody($response);
    }
    
    public function putAction()
    {
        $evaluationService = Evaluations_Service_Evaluation::getInstance();
        $questionService  = Evaluations_Service_Question::getInstance();
        
        $user = $this->_currentUser;
        if ($user == null) {
            $this->getResponse()
                ->setHttpResponseCode(404)
                ->appendBody("Not Found (User)");
            return;
        }

        $evaluationId = $this->_getParam('id');
        $evaluation   = $evaluationService->getByPK($evaluationId);
        if (!$evaluation instanceof Evaluations_Model_Evaluation) {
            $this->getResponse()
                ->setHttpResponseCode(404)
                ->appendBody("Not Found (Evaluation)");
            return;
        }
        
        $questionId   = $this->_getParam('sub_id');
        $question     = $questionService->getByPK($questionId);
        if (!$question instanceof Evaluations_Model_Question) {
            $this->getResponse()
                ->setHttpResponseCode(404)
                ->appendBody("Not Found (Question)");
            return;
        }
        
        $invariableParams = array(
            'id',
            'evaluationId',
            'responseCount',
            'creationDate'
        );

        $params = $this->getParams();
        foreach ($invariableParams as $invariableParam) {
            if (isset($params[$invariableParam])) {
                unset ($params[$invariableParam]);
            }
        }
        
        $question->fill($params);
        $question->setUpdateDate("now");
        $question->setNew(false);
        $question = $questionService->save($question);

        $evaluation->setUpdateDate($question->getUpdateDate());
        $evaluation->setNew(false);
        $evaluationService->save($evaluation);

        if (!$question) {
            $this->getResponse()
                ->setHttpResponseCode(400)
                ->appendBody("Question Not modified");
            return;
        }
        
        $this->getResponse()
            ->setHttpResponseCode(200)
            ->appendBody(json_encode($question));
    }
    
    public function deleteAction()
    {
        $evaluationService    = Evaluations_Service_Evaluation::getInstance();
        $questionService  = Evaluations_Service_Question::getInstance();

        $evaluationId = $this->_getParam('id');
        $evaluation   = $evaluationService->getByPK($evaluationId);
        if (!$evaluation instanceof Evaluations_Model_Evaluation) {
            $this->getResponse()
                ->setHttpResponseCode(404)
                ->appendBody("Not Found (Evaluation)");
            return;
        }
        
        $questionId = $this->_getParam('sub_id');
        $question   = $questionService->getByPK($questionId);
        if (!$question instanceof Evaluations_Model_Question) {
            $this->getResponse()
                ->setHttpResponseCode(404)
                ->appendBody("Not Found (Question)");
            return;
        }

        $questionService->remove($question);

        $this->getResponse()
            ->setHttpResponseCode(200)
            ->appendBody("Success");
    }

}
