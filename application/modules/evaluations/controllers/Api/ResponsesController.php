<?php

class Evaluations_Api_ResponsesController extends Tea_Controller_Rest_Action
{

    public function init()
    {
        parent::init();
    }

    public function indexAction()
    {
        $evaluationService = Evaluations_Service_Evaluation::getInstance();

        $evaluationId = $this->_getParam('id');
        $evaluation = $evaluationService->getByPK($evaluationId);

        $results = $evaluationService->calculateStatistics($evaluation);

        $response = json_encode($results);
        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody($response);
    }

    public function postAction()
    {
        $evaluationService = Evaluations_Service_Evaluation::getInstance();
        $responsServise = Evaluations_Service_Response::getInstance();

        $evaluationId = $this->_getParam('id');
        $responderId = $this->getParam('responderId');
        $responderName = $this->getParam('responderName');
        $trueAnswersCount = $this->getParam('trueAnswersCount');
        $evaluation = $evaluationService->getByPK($evaluationId);
        $questions = $evaluation->getQuestions();

        if (!$responderName && !$responderName) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody("no Responder");
            return;
        }

        $reCount = $responsServise->getByResponderId($responderId, $evaluationId);
        if ($reCount > 0) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody("Responsed Befor!");
            return;
        }

        $status = $evaluation->getStatus();
        if ($status == 1 ) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody("Evaluation Closed!");
            return;
        }


        if (!$evaluation instanceof Evaluations_Model_Evaluation) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody("Not Found (Evaluation)");
            return;
        }

        //----- extract ids of questions and their items from evaluation's info
        $questionsItemValues = array();
        foreach ($questions as $question) {

            $items = $question->getItems();
            $questionsItemValues[$question->getId()] = array();

            foreach ($items as $item) {
                $questionsItemValues[$question->getId()][] = $item->getValue();
            }
        }
        //-------------------------------------------------------------

        $list = $this->getParam('list', array());

        $responses = array();
        foreach ($list as $item) {
            $questionId = $item['questionId'];
            $values = $item['values'];

            if (!array_key_exists($questionId, $questionsItemValues)) {
                $this->getResponse()
                        ->setHttpResponseCode(404)
                        ->appendBody("Not Found (Question)");
                return;
            }

            foreach ($values as $value) {

                if (array_search($value, $questionsItemValues[$questionId]) === false) {
                    $this->getResponse()
                            ->setHttpResponseCode(404)
                            ->appendBody("Not Found (Value)");
                    return;
                }

                $response = new Evaluations_Model_Response();
                $response->setResponderId($responderId);
                $response->setResponderName($responderName);
                $response->setQuestionId($questionId);
                $response->setEvaluationId($evaluation->getId());
                $response->setValue($value);
                $response->setNew(true);

                $responses[] = $response;
            }
        }

        $user = new Users_Model_User;
        $user->setId($responderId);
        $evaluationService->responseToEvaluation($user, $evaluation, $responses);
        $results = $evaluationService->calculateStatistics($evaluation);

        if ($results) {
            $responderService = Evaluations_Service_Responder::getInstance();
            $responderModel = new Evaluations_Model_Responder();
            $responderModel->fill(array(
                'responderId' => $responderId,
                'responderName' => $responderName,
                'evaluationId' => $evaluationId,
                'trueAnswersCount' => $trueAnswersCount
            ));

            $rowCount = $responderService->getByResponderId($responderId, $evaluationId);

            if ($rowCount == 0) {
                $responderModel->setNew(true);
            } else {
                $responderModel->setNew(false);
            }
            $responderService->save($responderModel);
        }
        $response = json_encode($results);
        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody($response);
    }

}