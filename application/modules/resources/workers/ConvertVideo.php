<?php
require 'FFMpeg/vendor/autoload.php';
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use FFMpeg\FFMpeg;
use FFMpeg\FFProbe;
use FFMpeg\Format\Video;
use FFMpeg\Format\Video\Ogg;
use FFMpeg\Format\Video\X264;

class Resources_Worker_ConvertVideo
{
    public function work($data)
    {
        $res   = $data['resource'];
        $owner = $data['owner'];
        $tmp   = tempnam(
            sys_get_temp_dir(),
            pathinfo($res->getFileName(), PATHINFO_FILENAME)
        );
        $tmp  .= '.' . pathinfo($res->getFileName(), PATHINFO_EXTENSION);
        $pubPath = realpath(APPLICATION_PATH . '/../public');
        copy ($pubPath . '/'. $res->getReference(), $tmp);

        $mp4Fle  = tempnam(
            sys_get_temp_dir(),
            pathinfo($res->getFileName(), PATHINFO_FILENAME)
        );
        $mp4Fle .= '.mp4';

        $ogvFle  = tempnam(
            sys_get_temp_dir(),
            pathinfo($res->getFileName(), PATHINFO_FILENAME)
        );
        $ogvFle .= '.ogv';

        // Create a logger
        $logger = new Logger('MyLogger');
        $logger->pushHandler(new StreamHandler('/tmp/ffmpeg.log', Logger::DEBUG));

        // You have to pass a Monolog logger
        // This logger provides some usefull infos about what's happening
        $ffmpeg  = FFMpeg::load($logger);
        $ffprobe = FFProbe::load($logger);
        $ffmpeg->setProber($ffprobe);

        $x264Format = new X264();
        $x264Format->setDimensions(320, 240)
            //->setAudioCodec('libvo_aacenc')
            ->setFrameRate(15)
            ->setFrameRate(25);

        $oggFormat = new Ogg();
        $oggFormat->setDimensions(320, 240)
            ->setFrameRate(15)
            ->setGopSize(25);

        $ffmpeg->open($tmp)
            ->encode($x264Format, $mp4Fle)
            ->encode($oggFormat, $ogvFle)
            ->close();

        $orgRes = clone $res;
        $orgRes->setNew(true);
        $orgRes->setId(NULL);


        $pubPath = realpath(APPLICATION_PATH . '/../public');
        $pInfo   = pathinfo($mp4Fle);
        $newFile = $pInfo['basename'];
        $newPath = '/upfiles/' . substr(md5(microtime()), 0, 2) . '/' . $pInfo['basename'];
        $relPath = $pubPath . DIRECTORY_SEPARATOR . $newPath;
        copy($mp4Fle, $relPath);

        $mp4Res = new Core_Model_Resource();
        $mp4Res->setName($pInfo['basename']);
        $mp4Res->setFileSize(filesize($relPath));
        $mp4Res->setFileType(mime_content_type($relPath));
        $mp4Res->setReferenceType(Resources_Model_Resource::REFERENCE_TYPE_FILE);
        $mp4Res->setReference($newPath);
        $mp4Res->setCreatedById($res->getCreatedById());
        $mp4Res->setNew(true);

        $pubPath = realpath(APPLICATION_PATH . '/../public');
        $pInfo   = pathinfo($ogvFle);
        $newFile = $pInfo['basename'];
        $newPath = '/upfiles/' . substr(md5(microtime()), 0, 2) . '/' . $pInfo['basename'];
        $relPath = $pubPath . DIRECTORY_SEPARATOR . $newPath;
        copy($ogvFle, $relPath);

        $ogvRes = new Core_Model_Resource();
        $ogvRes->setName($pInfo['basename']);
        $ogvRes->setFileSize(filesize($relPath));
        $ogvRes->setFileType(mime_content_type($relPath));
        $ogvRes->setReferenceType(Resources_Model_Resource::REFERENCE_TYPE_FILE);
        $ogvRes->setReference($newPath);
        $ogvRes->setCreatedById($res->getCreatedById());
        $ogvRes->setNew(true);

        $resSrv = Core_Service_Resource::getInstance();
        $resSrv->save($orgRes, false);
        $resSrv->save($mp4Res, false);
        $resSrv->save($ogvRes, false);

        $resources = array(
            'origin' => $orgRes->getId(),
            'mp4'    => $mp4Res->getId(),
            'ogv'    => $ogvRes->getId()
        );
        $res->setReferenceType(Core_Model_Resource::REFERENCE_TYPE_CONTAINER);
        $res->setReference(serialize($resources));
        $res->setFilePath('');
        $res->setFileSize(0);
        $res->setUpdateDate('now');

        $resSrv->save($res, false);

        unlink($tmp);
        unlink($mp4Fle);
        unlink($ogvFle);
    }
}
