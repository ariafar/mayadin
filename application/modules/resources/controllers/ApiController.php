<?php

class Resources_ApiController extends Tea_Controller_Rest_Action
{

    public function init()
    {
        parent::init();
    }

    public function indexAction()
    {
        $limit = $this->_getParam('limit');
        $start = $this->_getParam('start', 0);
        $name = $this->_getParam('name');
        $type = $this->_getParam('type', null);
        $parentId = $this->getParam("parentId");
        $parentType = $this->getParam("parentType");
        $deleted = $this->_getParam('deleted');
        $orderBy = $this->_getParam('order_by', 'updateDate');
        $orderDir = $this->_getParam('order_dir', 'desc');

        $filter = array();
        isset($parentId) && $filter["parentId"] = $parentId;
        isset($parentType) && $filter["parentType"] = $parentType;
        isset($deleted) && $filter["deleted"] = $deleted;

        $sort = array($orderBy => $orderDir);

        if (isset($name))
            $filter['name'] = $name;
        switch (strtolower($type)) {
            case 'image':
                $filter['file_type'] = array(
                    'image/png',
                    'image/jpg',
                    'image/jpeg',
                    'image/gif'
                );
                break;
            case 'document':
                $filter['file_type'] = array(
                    'application/msword',
                    'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                    'application/vnd.openxmlformats-officedoc',
                    'application/excel',
                    'application/vnd.ms-excel',
                    'application/x-excel',
                    'application/x-msexcel',
                    'application/mspowerpoint',
                    'application/powerpoint',
                    'application/vnd.ms-powerpoint',
                    'application/x-mspowerpoint',
                    'application/pdf',
                    'application/vnd.oasis.opendocument.text'
                );
                break;

            case 'video':
                $filter['file_type'] = 'video/*';
                break;

            case 'audio':
                $filter['file_type'] = 'audio/*';
                break;
        }

//        if ($this->_currentUser == null) {
//            $filter['created_by_id'] = '0';
//        } else {
//            $filter['created_by_id'] = $this->_currentUser->getId();
//        }

        $service = Resources_Service_Resource::getInstance();
        $resources = $service->getList($filter, $sort, $start, $count, $limit);

        $list = array();
        foreach ($resources as $resource) {
            $list[] = $resource->toArray();
        }

        $result = array(
            'count' => $count,
            'list' => $list
        );

        $response = json_encode($result);
        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody($response);
    }

    public function getAction()
    {
        $service = Resources_Service_Resource::getInstance();
        $resourceId = $this->_getParam('id');
        $resource = $service->getByPK($resourceId);

        if (!$resource instanceof Resources_Model_Resource) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody("Not Found (Resource)");
            return;
        }

        $response = json_encode($resource->toArray());
        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody($response);
    }

    public function postAction()
    {
        $resources_service = Resources_Service_Resource::getInstance();

        $basePath = realpath(APPLICATION_PATH . '/../public');

        $dir_name = ( $this->_currentUser) ? $this->_currentUser->getId() : 0;
        if (!file_exists($basePath . '/upfiles/' . $dir_name)) {
            @mkdir($basePath . '/upfiles/' . $dir_name, 0777);
        }

        $date = new DateTime();
        $timestamp = $date->getTimestamp();

        $path = $basePath . '/upfiles/' . $dir_name;
        $path .= '/' . $timestamp . '_' . $_FILES['file']['name'];
        $meta = array();

        move_uploaded_file($_FILES['file']['tmp_name'], $path);

        $imageInfo = getimagesize($path);
        if (is_array($imageInfo)) {
            $type = $imageInfo['mime'];
            $meta = array(
                'width' => $imageInfo[0],
                'height' => $imageInfo[1]
            );
        } else {
            $type = $_FILES['file']['type'];
        }

        $resource = new Resources_Model_Resource();
//        $name = urlencode($_FILES['file']['name']);
        $name = rawurlencode($_FILES['file']['name']);
        $resource->setName($timestamp . '_' . $_FILES['file']['name']);
        $resource->setFileSize(filesize($path));
        $resource->setFileType($type);
        $resource->setMetaData($meta);
        $resource->setLocation('/upfiles/' . $dir_name . '/' . $timestamp . '_' . $name);

        $parentId = $this->_getParam("parentId");
        $parentType = $this->_getParam("parentType");
        if ($parentType) {
            $service = $this->getServicebyParentType($parentType);
            $resource->setParentType($parentType);
        }

        isset($parentId) && $resource->setParentId($parentId);

        $resource->setNew(true);
        $resource = $resources_service->save($resource);

        if (isset($parentType) && isset($parentId)) {
            $updateDate = $resource->getCreationDate();
            $this->updateParent($parentType, $parentId, $updateDate);
        }

        $response = json_encode($resource->toArray());
        $this->getResponse()->clearBody();
        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody($response);
    }

    public function deleteAction()
    {
        $resourceService = Resources_Service_Resource::getInstance();

        $resourceId = $this->_getParam('id');
        $resource = $resourceService->getByPK($resourceId);

        if (!$resource instanceof Resources_Model_Resource) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody("Not Found (Resource)");
            return;
        }

        $resourceService->remove($resource);

        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody("Success");
    }

    public function putAction()
    {
        $service = Resources_Service_Resource::getInstance();
        $params = $this->getParams();

        $resource = $service->getByPK($params['id']);
        //TO Do
        if (!$resource instanceof Resources_Model_Resource) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody('Not Found (Resource)');
            return;
        }

        $resource->fill($params);
        $resource->setUpdateDate('now');
        $resource->setNew(false);
        $resource = $service->save($resource);

        $parentType = $resource->getParentType();
        $parentId = $resource->getParentId();

        if (isset($parentType) && isset($parentId)) {
            $updateDate = $resource->getUpdateDate();
            $this->updateParent($parentType, $parentId, $updateDate);
        }

        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody(json_encode($resource->toArray()));
    }

    private function getServicebyParentType($parentType)
    {
        if ($parentType) {
            switch ($parentType) {
                case "news" :
                    $service = News_Service_News::getInstance();
                    break;

                case "educations" :
                    $service = Educations_Service_Education::getInstance();
                    break;

                case "humans" :
                    $service = Humans_Service_Human::getInstance();
                    break;

                case "gallery" :
                    $service = Galleries_Service_Gallery::getInstance();
                    break;

                case "reports" :
                    $service = Reports_Service_Report::getInstance();
                    break;

                case "links" :
                    $service = Links_Service_Link::getInstance();
                    break;

                case "markets" :
                    $service = Markets_Service_Market::getInstance();
                    break;

                case "category" :
                    $service = Categories_Service_Category::getInstance();
                    break;
            }
            return $service;
        }
    }

    private function updateParent($parentType, $parentId, $updateDate)
    {

        $service = $this->getServicebyParentType($parentType);
        $parent = $service->getByPk($parentId);

        if ($parent) {
            $parent->setUpdateDate($updateDate);
            if ($parentType == "gallery") {
                $imageCount = $parent->getImageCount() + 1;
                $parent->setImageCount($imageCount);
            }
            $parent->setNew(false);
            $service->save($parent);
        }
    }

}

