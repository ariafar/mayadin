<?php

class Markets_ApiController extends Tea_Controller_Rest_Action
{

    public function init()
    {
        parent::init();
    }

    public function indexAction()
    {
        $limit = $this->_getParam('limit', 20);
        $start = $this->_getParam('start', 0);
        $updateDate = $this->getParam("updateDate");
        $type = $this->getParam("type");
        $deleted = $this->_getParam('deleted');
        $mrktParent = $this->getParam("mrktParent", -1);

        $orderBy = $this->_getParam('order_by', 'updateDate');
        $orderDir = $this->_getParam('order_dir', 'desc');
        $sort = array($orderBy => $orderDir);

        $filter = array();

        isset($updateDate) && $filter["updateDate"] = $updateDate;
        isset($mrktParent) && $filter["mrktParent"] = $mrktParent;
        isset($type) && $filter["type"] = $type;
        isset($deleted) && $filter["deleted"] = $deleted;

        $dt = new DateTime($str);
        $dt->setTimeZone(new DateTimeZone('Asia/Tehran'));

        $markets_service = Markets_Service_Market::getInstance();
        $markets = $markets_service->getList($filter, $sort, $start, $count, $limit);
        $list = array();
        foreach ($markets as $item) {
            $item = $item->toArray();
            $r_service = Resources_Service_Resource::getInstance();
            $images = $r_service->getImagesByParent(array(
                "parentId" => $item["id"],
                "parentType" => "markets"
                    ));
            $item["photos"] = $images;
            $list[] = $item;
        }

        $response = array(
            'count' => $count,
            'list' => $list,
            'updateDate' => $dt->format('Y-m-d H:i:s')
        );
        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody(json_encode($response));
    }

    public function getAction()
    {
        $id = $this->_getParam('id');

        $service = Markets_Service_Market::getInstance();
        $market = $service->getByPK($id);

        if ($market)
            $result = $market->toArray();
        else
            $result = array();

        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody(json_encode($result));
    }

    public function postAction()
    {
        $params = $this->getParams();
        $authService = Users_Service_Auth::getInstance();

        $user = $authService->getCurrentUser();

        $images = $params["images"];
        $market = new Markets_Model_Market();
        $market->setupdateDate('now');
        $market->fill($params);
        $result = Markets_Service_Market::getInstance()
                ->save($market);

        if ($result) {
            $new_item = $result->toArray();

//            if (count($images)) {
//                foreach ($images as $img) {
//                    $r_model = new Resources_Model_Resource();
//                    $r_service = Resources_Service_Resource::getInstance();
//
//                    $img['parentId'] = $result->getId();
//                    $r_model->fill($img);
//                    $r_model->setNew(false);
//                    $r_service->save($r_model);
//                }
//
//                $new_item["photos"] = $images;
//            }

            $this->getResponse()
                    ->setHttpResponseCode(200)
                    ->appendBody(json_encode($new_item));
            return;
        }
    }

    public function putAction()
    {
        $service = Markets_Service_Market::getInstance();
        $params = $this->getParams();

        $market = $service->getByPK($params['id']);
        //TO Do
        if (!$market instanceof Markets_Model_Market) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody('Not Found (Market)');
            return;
        }

        $market->fill($params);
        $market->setUpdateDate('now');
        $market->setNew(false);
        $market = $service->save($market);

        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody(json_encode($market->toArray()));
    }

    public function deleteAction()
    {
        $service = Markets_Service_Market::getInstance();
        $marketId = $this->_getParam('id');
        $market = $service->getByPK($marketId);

        if (!$market instanceof Markets_Model_Market) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody("Not Found (Market)");
            return;
        }

        $service->remove($market);

        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody("Success");
    }

}

