<?php

class Categories_Service_Table extends Tea_Service_Abstract
{

    private static $_instance = null;
    private $_table = null;

    private function __construct()
    {
        $this->_table = new Categories_Model_DbTable_Tables();
    }

    public static function getInstance()
    {
        if (self::$_instance === null) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    public function getByPK($id, Categories_Model_Table $gallery = null)
    {
        $rows = $this->_table->find($id);
        if (count($rows) == 0) {
            return null;
        }
        $row = $rows->current()->toArray();
        if (!$gallery instanceof Tables_Model_Table) {
            $gallery = new Categories_Model_Table();
        }
        $gallery->fill($row);
        $gallery->setNew(false);
        return $gallery;
    }

    public function getList($filter, $sort, $start, &$count, $limit = 10)
    {
        $select = $this->_table->getDefaultAdapter()->select();
        $cSelect = clone $select;

        $select->from(array('u' => 'tables'), array('*'));
        $cSelect->from(array('u' => 'tables'), array('COUNT(*) AS count'));

        if (is_array($sort)) {
            foreach ($sort as $key => $ord) {
                $key = "u.$key";
                $select->order($key . ' ' . strtoupper($ord));
            }
        }

        if (is_array($filter)) {
            foreach ($filter as $key => $value) {
                switch ($key) {
                    case 'updateDate':
                        $select->where("u.updateDate > '$value'");
                        $cSelect->where("u.updateDate > '$value'");
                        break;

                    default:
                        $key = "u.$key";
                        $select->where("{$key} = ?", $value);
                        $cSelect->where("{$key} = ?", $value);
                        break;
                }
            }
        }

        $rows = $this->_table->getAdapter()->fetchRow($cSelect);
        $count = (int) $rows['count'];

        $select->limit($limit, $start);

        $tables = array();
        foreach ($this->_table->getAdapter()->fetchAll($select) as $row) {
            $item = new Categories_Model_Table();
            $item->fill($row);
            $tables[] = $item;
        }

        return $tables;
    }

    public function save(Categories_Model_Table $gallery)
    {
        if ($gallery->isNew()) {
            $data = $gallery->toArray(true);
            $pk = $this->_table->insert($data);
            if ($pk) {
                return $this->getByPK($pk, $gallery);
            }
        } else {
            $id = $gallery->getId();
            $data = $gallery->toArray(true);
            $where = $this->_table->getAdapter()->quoteInto('id = ?', $id);
            $this->_table->update($data, $where);
            return $this->getByPK($id, $gallery);
        }

        return false;
    }

    public function remove(Categories_Model_Table $gallery)
    {
        $where = $this->_table->getAdapter()->quoteInto('id = ?', $gallery->getId());
        $this->_table->delete($where);
    }

    public function removeByConditions($conditions = array())
    {
        $where = '';
        foreach ($conditions as $property => $value) {
            if (strlen($where) > 0) {
                $where .= ' AND ';
            }
            $where .= $this->_table->getAdapter()->quoteInto($property . ' = ?', $value);
        }

        $this->_table->delete($where);
    }

    public function removeByQuery($property, $query)
    {
        $this->_table->delete("$property IN ($query)");
    }

    public function removeAll()
    {
        $this->_table->delete('');
    }

}
