<?php

class Categories_Api_TablesController extends Tea_Controller_Rest_Action
{

    public function init()
    {
        parent::init();
    }

    public function indexAction()
    {
        $tableService = Categories_Service_Table::getInstance();

        $categoryId = $this->_getParam('id');

        $filter = array();

        isset($categoryId) && $filter['categoryId'] = $categoryId;

        $tables = $tableService->getList($filter, $sort, $start, $count, $limit);

        $list = array();
        $fieldsArray = array();
        $productsArray = array();
        
        foreach ($tables as $table) {
            $_table = $table->toArray();
            $fieldService = Categories_Service_Field::getInstance();
            $fields = $fieldService->getList(array("tableId" => $_table["id"]), $sort, $start, $count, $limit);
            foreach ($fields as $field) {
                $_field = $field->toArray();
                $fieldsArray[] = $_field;
            }

            $producService = Categories_Service_Product::getInstance();
            $products = $producService->getList(array("tableId" => $_table["id"]), $sort, $start, $count, $limit);
            foreach ($products as $product) {
                $productsArray[] = $product->toArray();
            }
            
            $_table["fields"] = $fieldsArray;
            $_table["products"] = $productsArray;
            $list[] = $_table;
        }

        $result = array(
            'count' => $count,
            'list' => $list
        );

        $response = json_encode($result);
        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody($response);
    }

    public function postAction()
    {
        $categoryService = Categories_Service_Category::getInstance();
        $tableService = Categories_Service_Table::getInstance();

        if ($this->_currentUser == null) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody("Not Found (User)");
            return;
        }

        $categoryId = $this->_getParam('id');
        $category = $categoryService->getByPK($categoryId);
        if (!$category instanceof Categories_Model_Category) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody("Not Found (Category)");
            return;
        }

        $params = $this->getParams();
        unset($params['id']);

        $table = new Categories_Model_Table();
        $table->fill($params);
        $table->setCategoryId($category->getId());
        $table->setNew(true);

        $new_table = $tableService->save($table);
        $fields = array();

        if ($new_table) {
            foreach ($params['fields'] as $fieldName) {
                $field = new Categories_Model_Field();
                $field->setName($fieldName);
                $field->setTableId($new_table->getId());
                $field->setNew(true);

                $field_service = Categories_Service_Field::getInstance();
                $newField = $field_service->save($field);
                $fields[] = $newField->toArray();
            }
        }

        $_table = $new_table->toArray();
        $_table['fields'] = $fields;

        $response = json_encode($_table);
        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody($response);
    }

    public function putAction()
    {
        $categoryService = Categories_Service_Category::getInstance();
        $tableService = Categories_Service_Table::getInstance();

        $user = $this->_currentUser;
        if ($user == null) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody("Not Found (User)");
            return;
        }

        $categoryId = $this->_getParam('id');
        $category = $categoryService->getByPK($categoryId);
        if (!$category instanceof Categories_Model_Category) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody("Not Found (Category)");
            return;
        }

        $tableId = $this->_getParam('sub_id');
        $table = $tableService->getByPK($tableId);
        if (!$table instanceof Categories_Model_Table) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody("Not Found (Table)");
            return;
        }

        $invariableParams = array(
            'id',
            'categoryId',
            'responseCount',
            'creationDate'
        );

        $params = $this->getParams();
        foreach ($invariableParams as $invariableParam) {
            if (isset($params[$invariableParam])) {
                unset($params[$invariableParam]);
            }
        }

        $table->fill($params);
        $table->setUpdateDate("now");
        $table->setNew(false);
        $table = $tableService->save($table);

        $category->setUpdateDate($table->getUpdateDate());
        $category->setNew(false);
        $categoryService->save($category);

        if (!$table) {
            $this->getResponse()
                    ->setHttpResponseCode(400)
                    ->appendBody("Table Not modified");
            return;
        }

        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody(json_encode($table));
    }

    public function deleteAction()
    {
        $categoryService = Categories_Service_Category::getInstance();
        $tableService = Categories_Service_Table::getInstance();

        $categoryId = $this->_getParam('id');
        $category = $categoryService->getByPK($categoryId);
        if (!$category instanceof Categories_Model_Category) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody("Not Found (Category)");
            return;
        }

        $tableId = $this->_getParam('sub_id');
        $table = $tableService->getByPK($tableId);
        if (!$table instanceof Categories_Model_Table) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody("Not Found (Table)");
            return;
        }

        $tableService->remove($table);

        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody("Success");
    }

}
