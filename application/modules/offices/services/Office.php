<?php

class Offices_Service_Office extends Tea_Service_Abstract
{

    private static $_instance = null;
    private $_table = null;

    private function __construct()
    {
        $this->_table = new Offices_Model_DbTable_Offices();
    }

    public static function getInstance()
    {
        if (self::$_instance === null) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    public function getByPK($id, Offices_Model_Office $office = null)
    {
        $rows = $this->_table->find($id);
        if (count($rows) == 0) {
            return null;
        }
        $row = $rows->current()->toArray();
        if (!$office instanceof Offices_Model_Office) {
            $office = new Offices_Model_Office();
        }
        $office->fill($row);
        $office->setNew(false);
        return $office;
    }

    public function getList($filter, $sort, $start, &$count, $limit = 10)
    {
        $select = $this->_table->getDefaultAdapter()->select();
        $cSelect = clone $select;

        $select->from(array('u' => 'offices'), array('*'));
        $cSelect->from(array('u' => 'offices'), array('COUNT(*) AS count'));

        if (is_array($sort)) {
            foreach ($sort as $key => $ord) {
                $key = "u.$key";
                $select->order($key . ' ' . strtoupper($ord));
            }
        }

        if (is_array($filter)) {
            foreach ($filter as $key => $value) {
                switch ($key) {
                    case 'updateDate':
                        $select->where("u.updateDate > '$value'");
                        $cSelect->where("u.updateDate > '$value'");
                        break;

                    default:
                        $key = "u.$key";
                        $select->where("{$key} = ?", $value);
                        $cSelect->where("{$key} = ?", $value);
                }
            }
        }

        $rows = $this->_table->getAdapter()->fetchRow($cSelect);
        $count = (int) $rows['count'];

        $select->limit($limit, $start);

        $offices = array();
        foreach ($this->_table->getAdapter()->fetchAll($select) as $row) {
            $item = new Offices_Model_Office();
            $item->fill($row);
            $offices[] = $item;
        }

        return $offices;
    }

    public function save(Offices_Model_Office $office)
    {
        if ($office->isNew()) {
            $data = $office->toArray(true);
            $pk = $this->_table->insert($data);
            if ($pk) {
                return $this->getByPK($pk, $office);
            }
        } else {
            $id = $office->getId();
            $data = $office->toArray(true);
            $where = $this->_table->getAdapter()->quoteInto('id = ?', $id);
            $this->_table->update($data, $where);
            return $this->getByPK($id, $office);
        }

        return false;
    }

    public function remove(Offices_Model_Office $office)
    {
        $where = $this->_table->getAdapter()->quoteInto('id = ?', $office->getId());
        $this->_table->delete($where);
    }

    public function removeAll()
    {
        $this->_table->delete('');
    }

}
