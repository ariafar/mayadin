﻿define([
    'jquery',
    'underscore',
    'backbone',
    'modules/images/collections/images',
    'text!modules/images/templates/configs.html',
    'modules/images/views/images_list',
    'i18n!modules/images/nls/fa-ir/labels',
    'libs/browserplus-min',
    'libs/jqueryPlugins/plupload.full',
    'libs/jqueryPlugins/jquery.plupload.queue',
    'libs/jqueryPlugins/jquery.lightbox-0.5',
    
    ], function ($, _, Backbone, ImagesCollection, ConfigsTpl, ImagesListView, Labels) {
        
        var SnippetView = Backbone.View.extend({
            initialize: function() {
                var $this = this;
                
                var tpl  = _.template(ConfigsTpl,{
                    labels  : Labels
                });
                this.$el.html($(tpl));

                if(this.model)
                    this.imagesCollection =  new ImagesCollection(this.model.get('photos'));
                
                return this;

            },

            events: {
                'click .save-config'        : 'saveConfigs'
            },

            afterRender : function () {
                this.registerEvents();
                this.showImages();
            },
            showImages : function()
            {
                var $this = this;
                var imagesListView = new ImagesListView({
                    collection  : $this.imagesCollection,
                    el          : $("#upload_image_container", $this.$el)
                });
            },
            registerEvents : function()
            {
                var $this = this;
                
                var uploaderBTn = new plupload.Uploader({
                    runtimes            : 'html5,html4',
                    browse_button       : 'image_upload_btn',
                    container           : 'btn_container',
                    max_file_size       : '10mb',
                    url                 :'/api/resources?parentId=2&parentType=news',
                    filters             : [{
                        title      : "Image files",
                        extensions : "jpg,gif,png"
                    }],
                    resize              : {
                        width           : 320,
                        height          : 240,
                        quality         : 90
                    },
                    init                : {
                        FilesAdded: function(up, files) {
                            _.each(files, function(file){
                                file.parentType = "news";
                                file.parentId = 1;
                            })
                            //                            $('.progress', $this.$el).show();
                            up.start();
                        },
                        UploadProgress : function(up, file){
                        //                            $("span.plupload_total_status", this.$el).html(up.total.percent + "%");
                        //                            $("div.bar", $this.$el).css("width", up.total.percent + "%");
                        //                            $("span.plupload_upload_status", $this.$el).html("Uploaded "+up.total.uploaded + "/" + up.files.length+" files");
                        },
                        FileUploaded : function(up, file, resp){
                        //                            $('.progress', this.$el).fadeOut(7000);
                        //                            var photo = $.parseJSON(resp.response);
                        //                            $this.imagesCollection.add(photo)
                        }
                    }
                });
                uploaderBTn.init();

            },

            chooseSlideshow : function(e)
            {
                $('.slideshow-choice-active', this.$el).removeClass('slideshow-choice-active');
                $(e.target).parents('.slideshow-choice').addClass('slideshow-choice-active');
            },

            saveConfigs: function(resp) {

                var _this = this;

                this.model.set({
                    'photos'    : _this.collection.toJSON()
                });
                this.model.trigger("save-configs", _this.$el);
                this.$el.modal('hide');



                a = {
                    "title": "نظر سنجی در مورد ازدواج",
                    "type": "1",
                    "questions" : [{
                            "question" : "how old are you?",
                            "type"  : "1",
                            "items" : [{
                                    "text" : "aaa",
                                    "value" : "1"
                            },{
                                    "text" : "bbb",
                                    "value" : "2"
                            },{
                                    "text" : "ccc",
                                    "value" : "3"
                            },{
                                    "text" : "ddd",
                                    "value" : "4"
                            }]
                    }]

                }
            }

       

        });

        return SnippetView;
    });
