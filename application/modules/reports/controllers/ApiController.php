<?php

class Reports_ApiController extends Tea_Controller_Rest_Action
{

    public function init()
    {
        parent::init();
    }

    public function indexAction()
    {
        $limit = $this->_getParam('limit', 20);
        $start = $this->_getParam('start', 0);
        $updateDate = $this->getParam("updateDate");
        $deleted = $this->_getParam('deleted');

        $orderBy = $this->_getParam('order_by', 'updateDate');
        $orderDir = $this->_getParam('order_dir', 'desc');
        $sort = array($orderBy => $orderDir);

        $filter = array();

        isset($updateDate) && $filter["updateDate"] = $updateDate;
        isset($deleted) && $filter["deleted"] = $deleted;

        $dt = new DateTime($str);
        $dt->setTimeZone(new DateTimeZone('Asia/Tehran'));

        $reports_service = Reports_Service_Report::getInstance();
        $reports = $reports_service->getList($filter, $sort, $start, $count, $limit);
        $list = array();
        foreach ($reports as $item) {
            $item = $item->toArray();
            $r_service = Resources_Service_Resource::getInstance();
            $images = $r_service->getImagesByParent(array(
                "parentId" => $item["id"],
                "parentType" => "report"
                    ));
            $item["photos"] = $images;
            $list[] = $item;
        }

        $response = array(
            'count' => $count,
            'list' => $list,
            'updateDate' => $dt->format('Y-m-d H:i:s')
        );
        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody(json_encode($response));
    }

    public function getAction()
    {
        $id = $this->_getParam('id');

        $service = Reports_Service_Report::getInstance();
        $report = $service->getByPK($id);

        if ($report)
            $result = $report->toArray();
        else
            $result = array();

        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody(json_encode($result));
    }

    public function postAction()
    {
        $params = $this->getParams();
        $authService = Users_Service_Auth::getInstance();

        $user = $authService->getCurrentUser();
//        if (!isset($user)) {
//            $this->getResponse()
//                    ->setHttpResponseCode(404)
//                    ->appendBody('Unauthorized');
//            return;
//        }

        $report = new Reports_Model_Report();
        $report->setupdateDate('now');
        $report->fill($params);
        $result = Reports_Service_Report::getInstance()
                ->save($report);
        if ($result) {
            $this->getResponse()
                    ->setHttpResponseCode(200)
                    ->appendBody(json_encode($result->toArray()));
            return;
        }
    }

    public function putAction()
    {
        $service = Reports_Service_Report::getInstance();
        $params = $this->getParams();

        $report = $service->getByPK($params['id']);
        //TO Do
        if (!$report instanceof Reports_Model_Report) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody('Not Found (Report)');
            return;
        }

        $report->fill($params);
        $report->setUpdateDate('now');
        $report->setNew(false);
        $report = $service->save($report);

        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody(json_encode($report->toArray()));
    }

    public function deleteAction()
    {
        $service = Reports_Service_Report::getInstance();
        $reportId = $this->_getParam('id');
        $report = $service->getByPK($reportId);

        if (!$report instanceof Reports_Model_Report) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody("Not Found (Report)");
            return;
        }

        $service->remove($report);

        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody("Success");
    }

}

