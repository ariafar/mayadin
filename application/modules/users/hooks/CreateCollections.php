<?php

class Users_Hook_CreateCollections
{
    public function execute($event, $data)
    {
        switch ($event) {
        case 'join_user':
            $this->_createDefaultCollections($data);
            break;
        }
    }

    private function _createDefaultCollections(Users_Model_User $user)
    {
        $coll = new Users_Model_UserCollection();
        $coll->setUserId($user->getId());
        $coll->setTitle('Gold Thread');
        $coll->setCreatedById($user->getId());
        $coll->setCreationDate('now');
        $coll->setRole('thread_owner');
        $coll->setDescription('contains sites which you own');

        Users_Service_UserCollection::getInstance()->save($coll);

        $coll = new Users_Model_UserCollection();
        $coll->setUserId($user->getId());
        $coll->setTitle('Silver Thread');
        $coll->setCreatedById($user->getId());
        $coll->setCreationDate('now');
        $coll->setRole('thread_member');
        $coll->setDescription(' contains sites which you are a member');

        Users_Service_UserCollection::getInstance()->save($coll);

        $coll = new Users_Model_UserCollection();
        $coll->setUserId($user->getId());
        $coll->setTitle('Bronz Thread');
        $coll->setCreatedById($user->getId());
        $coll->setCreationDate('now');
        $coll->setRole('thread_follow');
        $coll->setDescription('contains sites which you follow!');

        Users_Service_UserCollection::getInstance()->save($coll);
    }
}
