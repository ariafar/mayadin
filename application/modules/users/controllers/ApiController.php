<?php

class Users_ApiController extends Tea_Controller_Rest_Action
{

    public function init()
    {
        parent::init();
    }

    public function indexAction()
    {
        $limit = $this->_getParam('limit', 10);
        $start = $this->_getParam('start', 0);
        $orderDir = $this->_getParam('order_dir', 'desc');
        $filter = $this->_getParam('options', array());
        $sort = array($orderBy => $orderDir);

        $users = Users_Service_User::getInstance()
                ->getList($filter, $sort, $start, $count, $limit);

        $list = array();
        foreach ($users as $user) {
            if ($user->getId() != $this->_currentUser->getId()) {
                $user_array = $user->toArray();
                $user = Users_Service_User::getInstance()
                        ->getDetails($user, $this->_currentUser);

                $s_service = Users_Service_Service::getInstance();
                $service = $s_service->getByUserId($user->getId());
                if ($service) {
                    if($service->getExpireDate()){
                        $fuzzyDate = $this->getFuzzyDate($service->getExpireDate());
                    }
                    $service = $service->toArray();
                    if(isset($fuzzyDate))
                        $service["fuzzy_expire_date"] = $fuzzyDate;

                    $regions = array();
                    foreach (explode(',', $service['userRegions']) as $key => $value) {
                        $region = Regions_Service_Region::getInstance()->getByPk($value);
                        if (isset($region))
                            $regions[] = $region->toArray();
                    }

                    $user_array['service'] = $service;
                    $user_array['regions'] = $regions;
                }
            }

            $list[] = $user_array;
        }

        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody(json_encode(array(
                            'count' => $count,
                            'list' => $list
                        )));
    }

    public function getAction()
    {
        $userId = $this->_getParam('id');
        if ($userId == 'me' || $userId == '0') {
            if (!$this->_currentUser instanceof Users_Model_User) {
                $this->getResponse()
                        ->setHttpResponseCode(404)
                        ->appendBody("Not Found User");
                return;
            }
            $userId = $this->_currentUser->getId();
        }

        $userService = Users_Service_User::getInstance();
        $user = $userService->getByPK($userId);

        if ($user)
            $result = $user->toArray();
        else
            $result = array();

        if ($user) {

            $s_service = Users_Service_Service::getInstance();
            $service = $s_service->getByUserId($user->getId());
            if ($service) {
                if (strtotime(date('Y-m-d')) > strtotime($service->getExpireDate())) {
                    $service->setFuzzyExpireDate(0);
                } else {
                    $user_servic = Users_Service_User::getInstance();
                    $fuzzy_date = $user_servic->getFuzzyDate($service->getExpireDate());
                    $service->setFuzzyExpireDate($fuzzy_date);
                }
                $service = $service->toArray();
            }

            $user = $user->toArray();
            $user['service'] = $service;
        }



        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody(json_encode($user));
    }

    public function postAction()
    {
        $user_service = Users_Service_User::getInstance();

        $params = $this->getParams();
        $username = $params['username'];
        $password = $params['password'];

        $u = $user_service->getByEmail($username);
        if (isset($u)) {
            $this->getResponse()
                    ->setHttpResponseCode(401)
                    ->appendBody("Error : " . "Duplicate Username");
            return;
        }

        $user = new Users_Model_User();
        $user->fill($params);

        $profile = new Users_Model_Region();
        $profile->fill($params);

        $result = Users_Service_User::getInstance()
                ->save($user);
        if (!isset($result)) {
            $this->getResponse()
                    ->setHttpResponseCode(400)
                    ->appendBody("Error : " . $result);
            return;
        }

        $user = $user->toArray(false, null, array('password'));

        if (empty($username) || empty($password)) {
            $this->view->found = false;
            return;
        }

        $authService = Users_Service_Auth::getInstance();
        $validation = $authService->authenticate($username, $password);

        if ($validation == false) {
            $this->view->found = false;
            return;
        }

        $authService->saveCurrentUser();
        $user = $authService->getCurrentUser();
        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody(json_encode($user->toArray()));
    }

    public function putAction()
    {

        $params = $this->getParams();

        $user = $this->_currentUser;
        if ($user->getPassword() != $params['old']) {
            $this->getResponse()
                    ->setHttpResponseCode(401)
                    ->appendBody("old password is wrong");
            return;
        }

        $user->fill($params);

        if (Users_Service_User::getInstance()->save($user)) {
            $user = $user->toArray(false, null, array('password'));

            $this->getResponse()
                    ->setHttpResponseCode(200)
                    ->appendBody(json_encode("Success"));
        } else {
            $this->getResponse()
                    ->setHttpResponseCode(400)
                    ->appendBody("User Not modified");
        }
    }

    public function deleteAction()
    {
        $userService = Users_Service_User::getInstance();

        $user = $userService->getByPK($this->_getParam('id'));
        if (!$user instanceof Users_Model_User) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody("Not Found User");
            return;
        }

        $userService->remove($user);

        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody("Success");
    }

}
