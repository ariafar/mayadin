<?php

class Users_AuthController extends Tea_Controller_Action
{

    public function init()
    {
//        if (Zend_Auth::getInstance()->hasIdentity()
//                && $this->_request->getActionName() !== 'logout'
//                && $this->_request->getActionName() !== 'google-accept'
//                && $this->_request->getActionName() !== 'twitter-callback'
//                && $this->_request->getActionName() !== 'denied'
//        ) {
//            $config = Zend_Registry::get('config');
//            $this->_redirect($config->app->url);
//            return;
//        }

        if (Zend_Auth::getInstance()->hasIdentity() && $this->_request->getActionName() !== 'login') {
            Users_Service_Auth::getInstance()->clearCurrentUser();
        }

        $contextSwitch = $this->_helper->getHelper('contextSwitch');
        $contextSwitch->addActionContext('login', array('ajax', 'json'))
                ->addActionContext('denied', array('ajax', 'json'))
                ->addActionContext('logout', array('ajax', 'json'))
                ->initContext();
    }

    public function loginAction()
    {
        $authService = Users_Service_Auth::getInstance();
        $regionService = Users_Service_Region::getInstance();

        if (!$this->_request->isPost()) {
            return;
        }
        $username = $this->_getParam('username');
        $password = $this->_getParam('password');

        if (empty($username) || empty($password)) {
            $this->view->found = false;
            return;
        }

        $validation = $authService->authenticate($username, $password);

        if ($validation == false) {
            $this->view->found = false;
            return;
        }

//        if ($this->_getParam('remember_me', false)) {
//            // remember the session for 604800s = 7 days
//            Zend_Session::rememberMe(604800);
//        } else {
//            // do not remember the session
//            Zend_Session::forgetMe();
//        }

        $authService->saveCurrentUser();

        $user = $authService->getCurrentUser();

        $current_user = $user->toArray(false, null, array('password'));

        $this->view->found = true;
        $this->view->user = $current_user;

    }

    public function logoutAction()
    {
        Users_Service_Auth::getInstance()->clearCurrentUser();
        $config = Zend_Registry::get('config');
        $this->_redirect($config->app->url, array('prependBase' => false));
    }

    public function userToArray(Users_Model_User $user)
    {
        $_user = $user->toArray();
        $_user['creation_date'] = $user->getCreationDate()->format('Y-m-d H:i');
        $_user['update_date'] = $user->getUpdateDate()->format('Y-m-d H:i');
        if (isset($_user['photo_id'])) {
            $_user['photo_url'] = $this->view->url(
                    array(
                'id' => $_user['photo_id']
                    ), 'resource'
            );
        }
        $_user['profile_url'] = $this->view->url(
                array(
            'controller' => 'auth',
            'module' => 'users',
            'action' => 'profile',
            'id' => $_user['id']
                ), 'default'
        );
        return $_user;
    }

}
