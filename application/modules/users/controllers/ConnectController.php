<?php

class Users_ConnectController extends Tea_Controller_Action
{
    
    public function facebookAction()
    {
        $authService = Users_Service_Auth::getInstance();
        $currentUser = $authService->getCurrentUser();
        
        if (!$currentUser instanceof Users_Model_User) {
            $this->getResponse()
                ->setHttpResponseCode(404)
                ->appendBody("Not Found (User)");
            return;
        }
        $userId = $currentUser->getId();
            
        require_once 'facebook/facebook.php';
        $config = Zend_Registry::get('config');
        $params = array(
            'redirectUri' => $config->oauth->facebook->redirectUri,
            'callbackUrl' => $config->oauth->facebook->callbackUrl,
            'appId'       => $config->oauth->facebook->appId,
            'secret'      => $config->oauth->facebook->secret,
            'cookie'      => $config->oauth->facebook->cookie
        );
        $facebook = new Facebook($params);
        $uid      = $facebook->getUser();

        if ($uid) {
            $params = array(
                'user_id'       => $userId,
                'oauth_id'      => $uid,
                'provider'      => 'facebook',
                'access_token'  => $facebook->getAccessToken()
            );

            $networkService = Users_Service_Network::getInstance();
            $networkModel = new Users_Model_Network();
            $networkModel->fill($params);
            $networkModel->setNew(true);
            $networkService->save($networkModel);

            $this->view->found = true;
            $this->view->first_login = true;

            $facebook->destroySession();
        } else {
            $params = array(
                'scope' => 'offline_access,email,read_stream,publish_stream,
                user_location,user_education_history,user_work_history,
                user_birthday,user_relationships'
            );
            $loginUrl = $facebook->getLoginUrl($params);
            $this->_redirect($loginUrl);
            return;
        }
    }
    
    public function twitterAction()
    {
        $authService = Users_Service_Auth::getInstance();
        $currentUser = $authService->getCurrentUser();
        
        if (!$currentUser instanceof Users_Model_User) {
            $this->getResponse()
                ->setHttpResponseCode(404)
                ->appendBody("Not Found (User)");
            return;
        }
        $userId = $currentUser->getId();
        
        $session  = new Zend_Session_Namespace('twitter_oauth');
        $session->redirect = $this->view->url(
                array(
                    'module'     => 'users',
                    'controller' => 'connect',
                    'action'     => 'twitter'
                ),
                'default'
       );
        
        $configs = Zend_Registry::get('config');
        $consumer = new Zend_Oauth_Consumer($configs->oauth->twitter->toArray());

        if (!isset($session->token)) {
            $session->requestToken = serialize($consumer->getRequestToken());
            $consumer->redirect();
            return;
        }

        $token   = $session->token;
        $options = array(
            'username'       => $token->screen_name,
            'accessToken'   => $token,
            'consumerKey'    => $configs->oauth->twitter->consumerKey,
            'consumerSecret' => $configs->oauth->twitter->consumerSecret,
        );
        $twitter = new Zend_Service_Twitter($options);

        $params = array(
            'user_id'             => $userId,
            'oauth_id'            => $token->user_id,
            'provider'            => 'twitter',
            'username'            => $token->screen_name,
            'access_token'        => $token->getToken(),
            'access_token_secret' => $token->getTokenSecret(),
        );

        $networkService = Users_Service_Network::getInstance();
        $networkModel   = new Users_Model_Network();
        $networkModel->fill($params);
        $networkModel->setNew(true);
        $networkService->save($networkModel);

        $this->view->found = true;
        $this->view->first_login = true;
    }
}