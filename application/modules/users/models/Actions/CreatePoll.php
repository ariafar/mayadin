<?php

class Users_Model_Actions_CreatePoll extends Tea_Model_Actions_Abstract
{
    public function getNotificationMessage($activity)
    {
        $actor = $activity->getActor();
        if (!$actor instanceof Users_Model_User) {
            return false;
        }

        return $actor->getFirstName() . ' ' . $actor->getLastname() . ' Created a poll on your wall';
    }

    public function getFollowers($activity)
    {
        $followService = Users_Service_Follow::getInstance();
        $followersOfCreator = $followService->getUserFollowersIds($activity->getActorId());
        $followersOfRef     = $followService->getUserFollowersIds($activity->getObjId());
        
        $uids = array(
            $activity->getActorId(),
            $activity->getObjId()
        );
        $uids = array_merge($uids, $followersOfCreator);
        $uids = array_merge($uids, $followersOfRef);

        return array_unique($uids);
    }

    public function getNotifReceivers($activity)
    {
        $uids = array(
            $activity->getObjId()
        );

        $uids = array_unique($uids);
        if (array_search($activity->getActorId(), $uids) !== false) {
            unset($uids[$activity->getActorId()]);
        }
        
        return $uids;
    }

}
