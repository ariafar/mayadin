<?php

class Users_Model_User extends Tea_Model_Entity
{

    protected $_properties = array(
        'id' => null,
        'name' => null,
        'family' => null,
        'username' => null,
        'password' => null,
    );

    public function __construct($default = true)
    {
        parent::__construct();

        if ($default) {
            $this->setJoinDate('now');
        }
    }

    public function fill($record)
    {
        foreach ($record as $key => $value) {
            switch ($key) {
                case 'id':
                case 'name':
                case 'family':
                case 'password':
                case 'username':
                    $this->_properties[$key] = $value;
                    break;
            }
        }
    }

    public function setProfile(Users_Model_Region $profile)
    {
        $this->_profile = $profile;
        $this->_properties['profile_id'] = $profile->getId();
    }

    public static function hasProperty($property)
    {
        switch ($property) {
            case 'id':
            case 'name':
            case 'family':
            case 'password':
            case 'username':
                return true;
        }

        return false;
    }

    public function getPhoto()
    {
        if (!isset($this->_photo)) {
            if (!empty($this->_properties['photo_id'])) {
                $this->_photo = Resources_Service_Resource::getInstance()
                        ->getByPK($this->_properties['photo_id']);
            }
        }

        if (isset($this->_photo)) {
            return $this->_photo;
        }

        return null;
    }

    public function setPhoto(Resources_Model_Resource $photo)
    {
        $this->_photo = $photo;
        $this->_properties['photo_id'] = $photo->getId();
    }

    public function setIsFollowed($isFollowed = true)
    {
        $this->_is_followed = $isFollowed;
    }

    public function getIsFollowed()
    {
        if (!isset($this->_is_followed)) {
            $this->_is_followed = false;
        }

        return $this->_is_followed;
    }


    public function getUrl()
    {
        $config = Zend_Registry::get('config');
        $url = $config->app->url;
        $url = $url . '/users/' . $this->_properties['id'];
        return $url;
    }

    public function getPhotoUrl()
    {
        if (empty($this->_properties['photo_id'])) {
            return null;
        }

        $config = Zend_Registry::get('config');
        $url = $config->app->url;
        $url = $url . '/r/' . $this->_properties['photo_id'];

        return $url;
    }


}
