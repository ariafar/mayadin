<?php

class Users_Validate_ExistUser extends Zend_Validate_Abstract
{
    const NOT_EXIST = 'notExist';
    
    protected $_messageTemplates = array(
        self::NOT_EXIST => "No user was found by this id",
    );
    
    public function isValid($value)
    {
        $userService = Users_Service_User::getInstance();
        $authService = Users_Service_Auth::getInstance();
        
        if ($value == 'me' || $value == '0') {
            $user = $authService->getCurrentUser();
        } else {
            $user = $userService->getByPK($value);    
        }
        
        if (!$user instanceof Users_Model_User) {
            $this->_error(self::NOT_EXIST);
            return false;
        }
        
        return true;
    }
}
