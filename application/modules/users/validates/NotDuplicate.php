<?php

class Users_Validate_NotDuplicate extends Zend_Validate_Abstract
{
    const IS_DUPLICATE = 'isDuplicate';
    
    protected $_messageTemplates = array(
        self::IS_DUPLICATE => "Value is duplicated",
    );
    
    public function isValid($value)
    {
        $userService = Users_Service_User::getInstance();
        
        $filter = array(
            'email' => $value
        );
        $userService->getList($filter, array(), 0, $count, 1);
        
        if ($count > 0) {
            $this->_error(self::IS_DUPLICATE);
            return false;
        }
        
        return true;
    }
}
