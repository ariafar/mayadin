SET FOREIGN_KEY_CHECKS=0;

DROP TABLE IF EXISTS `user_roles`;
CREATE TABLE IF NOT EXISTS `user_roles` (
    `user_id`   BIGINT(20)  UNSIGNED NOT NULL,
    `role_id`   BIGINT(20)  UNSIGNED NOT NULL,

    PRIMARY KEY (`user_id`, `role_id`)
) ENGINE=InnoDB  AUTO_INCREMENT=1 ;


DROP TABLE IF EXISTS `roles`;
CREATE TABLE IF NOT EXISTS `roles` (
    `id`          BIGINT(20)      UNSIGNED NOT NULL AUTO_INCREMENT,
    `title`       VARCHAR(255)    NOT NULL,
    `context`     VARCHAR(255)    NOT NULL,
    `context_id`  VARCHAR(100)    NULL,

    PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `role_permissions`;
CREATE TABLE IF NOT EXISTS `role_permissions` (
    `id`                BIGINT(20)      UNSIGNED NOT NULL AUTO_INCREMENT,
    `role_id`           BIGINT(20)      UNSIGNED NOT NULL,
    `context`           VARCHAR(255)    NOT NULL,
    `action`            VARCHAR(255)    NOT NULL,
    `item_id`           VARCHAR(255)    NOT NULL,
    `permission`        ENUM('ALLOWED', 'BLOCKED') NOT NULL DEFAULT 'ALLOWED',

    PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci AUTO_INCREMENT=1 ;
