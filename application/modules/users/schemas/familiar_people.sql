SET FOREIGN_KEY_CHECKS=0;

DROP TABLE IF EXISTS `user_familiar_people`;
CREATE TABLE IF NOT EXISTS `user_familiar_people` (
    `id`                bigint(20)     UNSIGNED NOT NULL AUTO_INCREMENT,
    `user_id`           bigint(20)     UNSIGNED,
    `type`              ENUM('GMAIL', 'YAHOO', 'FACEBOOK', 'TWITTER'),
    `net_id`            varchar(255),
    `name`              varchar(255),
    `email`             varchar(255),
    `invited`           tinyint(1) default 0,

    PRIMARY KEY (`id`)

) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci AUTO_INCREMENT=1 ;
