<?php

class News_Service_News extends Tea_Service_Abstract
{

    private static $_instance = null;
    private $_table = null;

    private function __construct()
    {
        $this->_table = new News_Model_DbTable_News();
    }

    public static function getInstance()
    {
        if (self::$_instance === null) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    public function getByPK($id, News_Model_News $news = null)
    {
        $rows = $this->_table->find($id);
        if (count($rows) == 0) {
            return null;
        }
        $row = $rows->current()->toArray();
        if (!$news instanceof News_Model_News) {
            $news = new News_Model_News();
        }
        $news->fill($row);
        $news->setNew(false);
        return $news;
    }

    public function getList($filter, $sort, $start, &$count, $limit = 10)
    {
        $select = $this->_table->getDefaultAdapter()->select();
        $cSelect = clone $select;

        $select->from(array('u' => 'news'), array('*'));
        $cSelect->from(array('u' => 'news'), array('COUNT(*) AS count'));

        if (is_array($sort)) {
            foreach ($sort as $key => $ord) {
                $key = "u.$key";
                $select->order($key . ' ' . strtoupper($ord));
            }
        }

        if (is_array($filter)) {
            foreach ($filter as $key => $value) {
                switch ($key) {
                    case 'title':
                        $select->where("CONCAT(u.title, u.lastName) LIKE '%$value%'");
                        break;

                    case 'updateDate':
                        $select->where("u.updateDate > '$value'");
                        $cSelect->where("u.updateDate > '$value'");
                        break;

                    case 'type':
                        if ($value != 0) {
                            $select->where("u.type LIKE '$value'");
                            $cSelect->where("u.type LIKE '$value'");
                        } else {
                            $select->where('type IN (0,1,2,4)');
                            $cSelect->where('type IN (0,1,2,4)');
                        }
                        break;

                    default:
                        $key = "u.$key";
                        $select->where("{$key} = ?", $value);
                        $cSelect->where("{$key} = ?", $value);
                }
            }
        }

        $rows = $this->_table->getAdapter()->fetchRow($cSelect);
        $count = (int) $rows['count'];

        $select->limit($limit, $start);

        $news = array();
        foreach ($this->_table->getAdapter()->fetchAll($select) as $row) {
            $item = new News_Model_News();
            $item->fill($row);
            $news[] = $item;
        }

        return $news;
    }

    public function save(News_Model_News $news)
    {
        if ($news->isNew()) {
            $data = $news->toArray(true);
            $pk = $this->_table->insert($data);
            if ($pk) {
                return $this->getByPK($pk, $news);
            }
        } else {
            $id = $news->getId();
            $data = $news->toArray(true);
            $where = $this->_table->getAdapter()->quoteInto('id = ?', $id);
            $this->_table->update($data, $where);
            return $this->getByPK($id, $news);
        }

        return false;
    }

    public function remove(News_Model_News $news)
    {
        $where = $this->_table->getAdapter()->quoteInto('id = ?', $news->getId());
        $this->_table->delete($where);
    }

    public function removeByConditions($conditions = array())
    {
        $where = '';
        foreach ($conditions as $property => $value) {
            if (strlen($where) > 0) {
                $where .= ' AND ';
            }
            $where .= $this->_table->getAdapter()->quoteInto($property . ' = ?', $value);
        }

        $this->_table->delete($where);
    }

    public function removeByQuery($property, $query)
    {
        $this->_table->delete("$property IN ($query)");
    }

    public function removeAll()
    {
        $this->_table->delete('');
    }

}
