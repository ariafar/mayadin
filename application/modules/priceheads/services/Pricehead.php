<?php

class Priceheads_Service_Pricehead extends Tea_Service_Abstract
{

    private static $_instance = null;
    private $_table = null;

    private function __construct()
    {
        $this->_table = new Priceheads_Model_DbTable_Priceheads();
    }

    public static function getInstance()
    {
        if (self::$_instance === null) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    public function getByPK($id, Priceheads_Model_Pricehead $pricehead = null)
    {

        $rows = $this->_table->find($id);
        if (count($rows) == 0) {
            return null;
        }
        $row = $rows->current()->toArray();
        if (!$pricehead instanceof Priceheads_Model_Pricehead) {
            $pricehead = new Priceheads_Model_Pricehead();
        }
        $pricehead->fill($row);
        $pricehead->setNew(false);
        return $pricehead;
    }

    public function getList($filter, $sort, $start, &$count, $limit = 10)
    {
        $select = $this->_table->getDefaultAdapter()->select();
        $cSelect = clone $select;

        $select->from(array('u' => 'priceheads'), array('*'));
        $cSelect->from(array('u' => 'priceheads'), array('COUNT(*) AS count'));

        if (is_array($sort)) {
            foreach ($sort as $key => $ord) {
                $key = "u.$key";
                $select->order($key . ' ' . strtoupper($ord));
            }
        }

        if (is_array($filter)) {
            foreach ($filter as $key => $value) {
                switch ($key) {
                    case 'updateDate':
                        $select->where("u.updateDate > '$value'");
                        $cSelect->where("u.updateDate > '$value'");
                        break;
                    default:
                        $key = "u.$key";
                        $select->where("{$key} = ?", $value);
                        $cSelect->where("{$key} = ?", $value);
                }
            }
        }

        $rows = $this->_table->getAdapter()->fetchRow($cSelect);
        $count = (int) $rows['count'];

        $select->limit($limit, $start);

        $priceheads = array();
        foreach ($this->_table->getAdapter()->fetchAll($select) as $row) {
            $item = new Priceheads_Model_Pricehead();
            $item->fill($row);
            $priceheads[] = $item;
        }

        return $priceheads;
    }

    public function save(Priceheads_Model_Pricehead $pricehead)
    {
        if ($pricehead->isNew()) {
            $data = $pricehead->toArray(true);
            $pk = $this->_table->insert($data);
            if ($pk) {
                return $this->getByPK($pk, $pricehead);
            }
        } else {
            $id = $pricehead->getId();
            $data = $pricehead->toArray(true);
            $where = $this->_table->getAdapter()->quoteInto('id = ?', $id);
            $this->_table->update($data, $where);
            return $this->getByPK($id, $pricehead);
        }

        return false;
    }

    public function remove(Priceheads_Model_Pricehead $pricehead)
    {
        $where = $this->_table->getAdapter()->quoteInto('id = ?', $pricehead->getId());
        $this->_table->delete($where);
    }

    public function removeByConditions($conditions = array())
    {
        $where = '';
        foreach ($conditions as $property => $value) {
            if (strlen($where) > 0) {
                $where .= ' AND ';
            }
            $where .= $this->_table->getAdapter()->quoteInto($property . ' = ?', $value);
        }

        $this->_table->delete($where);
    }

    public function removeByQuery($property, $query)
    {
        $this->_table->delete("$property IN ($query)");
    }

    public function removeAll()
    {
        $this->_table->delete('');
    }

}
