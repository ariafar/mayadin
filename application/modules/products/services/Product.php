<?php

class Products_Service_Product extends Tea_Service_Abstract
{

    private static $_instance = null;
    private $_table = null;

    private function __construct()
    {
        $this->_table = new Products_Model_DbTable_Products();
    }

    public static function getInstance()
    {
        if (self::$_instance === null) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    public function getByPK($id, Products_Model_Product $product = null)
    {

        $rows = $this->_table->find($id);
        if (count($rows) == 0) {
            return null;
        }
        $row = $rows->current()->toArray();
        if (!$product instanceof Products_Model_Product) {
            $product = new Products_Model_Product();
        }
        $product->fill($row);
        $product->setNew(false);
        return $product;
    }

    public function getList($filter, $sort, $start, &$count, $limit = 10)
    {
        $select = $this->_table->getDefaultAdapter()->select();
        $cSelect = clone $select;

        $select->from(array('u' => 'products'), array('*'));
        $cSelect->from(array('u' => 'products'), array('COUNT(*) AS count'));

        if (is_array($sort)) {
            foreach ($sort as $key => $ord) {
                $key = "u.$key";
                $select->order($key . ' ' . strtoupper($ord));
            }
        }

        if (is_array($filter)) {
            foreach ($filter as $key => $value) {
                switch ($key) {
                    case 'updateDate':
                        $select->where("u.updateDate > '$value'");
                        $cSelect->where("u.updateDate > '$value'");
                        break;
                    default:
                        $key = "u.$key";
                        $select->where("{$key} = ?", $value);
                        $cSelect->where("{$key} = ?", $value);
                }
            }
        }

        $rows = $this->_table->getAdapter()->fetchRow($cSelect);
        $count = (int) $rows['count'];

        $select->limit($limit, $start);

        $products = array();
        foreach ($this->_table->getAdapter()->fetchAll($select) as $row) {
            $item = new Products_Model_Product();
            $item->fill($row);
            $products[] = $item;
        }

        return $products;
    }

    public function save(Products_Model_Product $product)
    {
        if ($product->isNew()) {
            $data = $product->toArray(true);
            $pk = $this->_table->insert($data);
            if ($pk) {
                return $this->getByPK($pk, $product);
            }
        } else {
            $id = $product->getId();
            $data = $product->toArray(true);
            $where = $this->_table->getAdapter()->quoteInto('id = ?', $id);
            $this->_table->update($data, $where);
            return $this->getByPK($id, $product);
        }

        return false;
    }

    public function remove(Products_Model_Product $product)
    {
        $where = $this->_table->getAdapter()->quoteInto('id = ?', $product->getId());
        $this->_table->delete($where);
    }

    public function removeByConditions($conditions = array())
    {
        $where = '';
        foreach ($conditions as $property => $value) {
            if (strlen($where) > 0) {
                $where .= ' AND ';
            }
            $where .= $this->_table->getAdapter()->quoteInto($property . ' = ?', $value);
        }

        $this->_table->delete($where);
    }

    public function removeByQuery($property, $query)
    {
        $this->_table->delete("$property IN ($query)");
    }

    public function removeAll()
    {
        $this->_table->delete('');
    }

}
