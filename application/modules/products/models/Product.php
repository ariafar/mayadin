<?php

class Products_Model_Product extends Tea_Model_Entity
{

    protected $_properties = array(
        'productCode' => NULL,
        'productTitle' => NULL,
        'unitCode' => NULL,
        'groupId' => NULL,
        'departmentId' => NULL,
        'typeId' => NULL,
        'itemId' => NULL,
        'productSource' => NULL,
        'ptCode' => NULL,
        'price' => NULL,
        'grade1Price' => NULL,
        'grade2Price' => NULL,
        'premiumPrice' => NULL,
        'packingPrice' => NULL,
        'efficacyDate' => NULL,
        'updateDate' => NULL,
        'creationDate' => NULL,
        'deleted' => 0
    );

    public function __construct()
    {
        parent::__construct();

        $this->setCreationDate('now');
        $this->setUpdateDate('now');
    }

    public function fill($record)
    {
        foreach ($record as $key => $value) {
            switch ($key) {
                case 'productCode' :
                case 'productTitle' :
                case 'unitCode':
                case 'groupId' :
                case 'departmentId' :
                case 'typeId' :
                case 'itemId' :
                case 'productSource' :
                case 'ptCode':
                case 'price':
                case 'grade1Price' :
                case 'grade2Price' :
                case 'premiumPrice':
                case 'packingPrice' :
                case 'efficacyDate':
                case 'updateDate' :
                case 'creationDate' :
                case 'deleted' :
                    $this->_properties[$key] = $value;
                    break;
            }
        }
    }

}

