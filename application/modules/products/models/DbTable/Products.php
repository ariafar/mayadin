<?php

class Products_Model_DbTable_Products extends Zend_Db_Table_Abstract
{
    protected $_name    = 'products';
    protected $_primary = 'productCode';
}
