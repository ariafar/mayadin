<?php

class Products_ApiController extends Tea_Controller_Rest_Action
{

    public function init()
    {
        parent::init();
    }

    public function indexAction()
    {
        $limit = $this->_getParam('limit', 20);
        $start = $this->_getParam('start', 0);
        $updateDate = $this->getParam("updateDate");
        $type = $this->getParam("type");
        $category = $this->getParam("category");
        $parentId = $this->getParam("parentId");
        $deleted = $this->_getParam('deleted');

        $orderBy = $this->_getParam('order_by', 'updateDate');
        $orderDir = $this->_getParam('order_dir', 'desc');
        $sort = array($orderBy => $orderDir);

        $filter = array();

        isset($updateDate) && $filter["updateDate"] = $updateDate;
        isset($type) && $filter["type"] = $type;
        isset($category) && $filter["category"] = $category;
        isset($parentId) && $filter["parentId"] = $parentId;
        isset($deleted) && $filter["deleted"] = $deleted;

        $dt = new DateTime($str);
        $dt->setTimeZone(new DateTimeZone('Asia/Tehran'));

        $products_service = Products_Service_Product::getInstance();
        $products = $products_service->getList($filter, $sort, $start, $count, $limit);
        $list = array();
        foreach ($products as $item) {
            $item = $item->toArray();
            if (isset($item['fileId'])) {
                $r_service = Resources_Service_Resource::getInstance();
                $file_id = $item['fileId'];
                $file_id && $file = $r_service->getByPK($file_id);
                if ($file) {
                    $config = Zend_Registry::get('config');
                    $file && $item['file'] = $file->toArray();
                    $item["path"] = $config->app->url . '/r/' . $file->getId();
                };
            }
            $list[] = $item;
        }

        $response = array(
            'count' => $count,
            'list' => $list,
            'updateDate' => $dt->format('Y-m-d H:i:s')
        );
        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody(json_encode($response));
    }

    public function getAction()
    {
        $id = $this->_getParam('id');

        $service = Products_Service_Product::getInstance();
        $product = $service->getByPK($id);

        if ($product)
            $result = $product->toArray();
        else
            $result = array();

        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody(json_encode($result));
    }

    public function postAction()
    {
        $params = $this->getParams();
        $authService = Users_Service_Auth::getInstance();

        $user = $authService->getCurrentUser();

        $product = new Products_Model_Product();
        $product->setupdateDate('now');
        $product->fill($params);
        $result = Products_Service_Product::getInstance()
                ->save($product);

        if ($result) {
            $this->getResponse()
                    ->setHttpResponseCode(200)
                    ->appendBody(json_encode( $result->toArray()));
            return;
        }
    }

    public function putAction()
    {
        $service = Products_Service_Product::getInstance();
        $params = $this->getParams();

        $product = $service->getByPK($params['id']);
        //TO Do
        if (!$product instanceof Products_Model_Product) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody('Not Found (Product)');
            return;
        }

        $product->fill($params);
        $product->setUpdateDate('now');
        $product->setNew(false);
        $product = $service->save($product);

        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody(json_encode($product->toArray()));
    }

    public function deleteAction()
    {
        $service = Products_Service_Product::getInstance();
        $productId = $this->_getParam('id');
        $product = $service->getByPK($productId);

        if (!$product instanceof Products_Model_Product) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody("Not Found (Product)");
            return;
        }

        $service->remove($product);

        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody("Success");
    }

}

